import React, {Component} from 'react'


export default class FacilitiesItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = { fac: this.props.fac};
    }

    render() {

        return (
            <li>{this.state.fac}</li>
        );

    }

}
