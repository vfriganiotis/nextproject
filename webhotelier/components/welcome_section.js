import React, {Component} from 'react'
import Link from 'next/link'

export default class WelcomeSection extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            img : this.props.img,
            title : this.props.title,
            paragraph : this.props.paragraph
        }
    }

    render() {
        return (
           <div className={"row"}>
               <div class="col-md-12">
                   <h1>Hello i am</h1>
                   <h2>Hello i am 2</h2>
               </div>
               <div class="col-md-7">
                  <img src={this.state.img} />
               </div>
               <div class="col-md-5">
                   <h1>{this.state.title}</h1>
                   <h2>{this.state.paragraph}</h2>
                   <Link href="/accommodation" as="/rooms">
                       <a className="details btn">CHOOSE YOUR ACCOMMODATION</a>
                   </Link>
               </div>
           </div>
        );
    }
}


