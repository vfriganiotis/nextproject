import React, {Component} from 'react'

export default class BookingForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = { bookurl : this.props.bookurl}
    }

    render() {

        let newUrl = this.state.bookurl
        newUrl = newUrl + 'widgets/embed/'
        return (
            <div className={"booking_form"}>
                <iframe src={newUrl}>
                </iframe>
            </div>
        );

    }

}
