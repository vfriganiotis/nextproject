import React,{Component} from 'react'
import Observer from '@researchgate/react-intersection-observer';
import RoomSlider from '../components/room_full_slider'

export default class RoomGallery extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            galleries: this.props.gallery,
            count: this.props.count,
            total: this.props.gallery.length,
            show: false,
            showAll: 1,
            slug: this.props.slug,
            openGallery: false
        };

        this.showContainers = this.showContainers.bind(this);
        this.showMore = this.showMore.bind(this);
        this.galleriesShow = this.galleriesShow.bind(this);
    }

    componentWillReceiveProps(nextProps) {

        this.setState({
            galleries: nextProps.gallery,
            count: nextProps.count,
            total: nextProps.gallery.length,
            show: false,
            showAll: 1,
            slug: nextProps.slug,
            item: 0
        });

    }

    galleriesShow(i){

        this.setState({
            openGallery: !this.state.openGallery,
            item: i
        });
        console.log(this.state.item)
    }


    showContainers() {

        let count = this.state.count
        let showALL = this.state.showAll
        let slugs = this.state.slug
        let galleriesShow = this.galleriesShow
        return this.state.galleries.map(function(item,i) {

            let styles = {
                background: `url(${item})`,
            };

            if( count >= i) {
                return (

                    <ImageDiv click={ () => galleriesShow(i) } key={i}  styles={styles} count={ i + 1 } showAll={showALL} post_name={slugs}  />

                )
            }

        })

    }

    __buttonShowMore(){

        let totalLength = this.state.total;
        let count = this.state.count;

        if( totalLength > count  ){

            return(
                <div onClick={this.showMore} className="btn-c withArrowSm goTopArrow">{this.props.locale}</div>
            )

        }else{
            return(
                <div onClick={this.showMore} className="hide">{this.props.locale}</div>
            )
        }

    }

    showMore(){
        this.setState({ count: this.state.count + 10 , showAll : 0})
    }

    render() {

        return (
            <div>

                <div className="row no_m_pad bg_color_f2 transitions_cont">

                    <div className="col-md-3 _small_height">
                        <div className="start_c-abs">
                            <div className="cnt">Gallery</div>
                            <div className="btn-c withArrowSm">{this.props.localeCategory}</div>
                        </div>
                    </div>

                    { this.showContainers()}

                    <div className="show_more_items">
                        {this.__buttonShowMore()}
                    </div>

                </div>

                <RoomSlider images={this.state.galleries} open={this.state.openGallery} click={this.galleriesShow} item={this.state.item}/>

            </div>
        )
    }

}


class ImageDiv extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            showAll: this.props.showAll,
            count: this.props.count,
            newUrl: this.props.newUrl,
            pathname: this.props.pathname,
            post_name: this.props.post_name,
            styles: this.props.styles

        };
        this.handleIntersection = this.handleIntersection.bind(this);

    }

    componentWillReceiveProps(nextProps) {

        this.setState({
            // show: false,
            showAll: nextProps.showAll,
            count: nextProps.count,
            newUrl: nextProps.newUrl,
            pathname: nextProps.pathname,
            post_name: nextProps.post_name,
            // styles: nextProps.styles
        });

    }

    handleIntersection(event) {

        let self = this;
        if(event.isIntersecting){

            let newCount = 0;
            if( this.state.count > 3 ){
                newCount = (this.state.count - 4) * this.state.showAll;
            }else{
                newCount = this.state.count
            }

            setTimeout(function(){
                self.setState({ show: true})
            }, (newCount  * 0.25) * 1000)

        }

    }


    render() {

        const { show } = this.state;
        let options = {
            onChange: this.handleIntersection,
            root: '#scrolling-container',
            threshold: 0.25,
            disabled: show
        };

        return (

            <div  className={(show ? "col-md-3 revealed" : "col-md-3")} onClick={this.props.click}>
                <Observer {...options}>
                    <div>
                        <div className="room_single_acc">

                            <div className="sliderHome _small_height" style={this.state.styles}>

                            </div>

                            <div className="pos-abs _gall">

                            </div>

                            <div className="gal_overlay_bg">

                            </div>

                            <div className="search_icon">
                                <img src="/static/assets/searchIcon.png"/>
                            </div>

                            <div className="opac_black">

                            </div>

                        </div>
                    </div>
                </Observer>
            </div>

        );
    }
}




