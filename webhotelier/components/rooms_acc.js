import React, {Component} from 'react'
import Link from 'next/link'

export default class AccommodationRoomsCone extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rooms : this.props.rooms
        }
    }

    rooms(){

        const moreRooms = this.state.rooms.map(function(item,i){

            let codes = item.room_name.split(' ').join('-');
            let pathname = '/room/' + codes;
            let newUrl = '/room?id=' + codes;

            let styles = {
                background: `url(${item.featured_image})`,
            };

            return (<div key={i} className="col-md-6">
                <Link href={newUrl} as={pathname} >
                    <div className="room_single_acc">

                        <div className="sliderHome _small_height" style={styles}>

                        </div>

                        <div className="pos-abs">
                            <div className="cnt">{item.room_name}</div>
                            <div className="btn-c withArrowSm arrowRight ">READ MORE</div>
                        </div>

                        <div className="opac_black">

                        </div>

                    </div>
                </Link>
            </div>)

        })

        return moreRooms;
    }

    render() {
        return (
            <div className="row more_rooms">
                  {this.rooms()}
            </div>
        );
    }
}