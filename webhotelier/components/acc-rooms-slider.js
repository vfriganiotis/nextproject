import React, {Component} from 'react'
import Slider from "react-slick"
import Link from 'next/link'
import apiRoomsWP from "../static/WPdataRooms";

export default class AccommodationRooms extends React.Component {

    constructor(props) {
        super(props);

        this.prev = this.prev.bind(this);
        this.next = this.next.bind(this);

        let WProoms = JSON.parse(apiRoomsWP);

        let RoomsName = [];
        WProoms.map(function(item){
            RoomsName.push({ "name" :item.acf.room_name , "description" : item.acf.room_description , "feat_image" : item.acf.room_image.url })
        })

        this.state = { data: RoomsName};

    }

    next(){
        this.moreRoomsSlider.slickNext();
    }

    prev(){
        this.moreRoomsSlider.slickPrev();
    }



    rooms(){

        let moreRooms = this.state.data.map(function(item,i){

            let codes = item.name.split(' ').join('-');
            let pathname = '/room/' + codes;
            let newUrl = '/room?id=' + codes;
            let room = <div key={i}>
                <Link href={newUrl} as={pathname}>
                    <a className="details btn">DETAILS</a>
                </Link>
            </div>;

            return (<div key={i} className="pull-center">
                <div>
                    <img src={item.feat_image} />
                </div>
                <div className="primary_heading">{item.name}</div>
                <div className="description" dangerouslySetInnerHTML={{__html: item.description }}>
                </div>
                {room}
            </div>)

        })

         return moreRooms;
    }

    render() {

        const RoomsSettings = {
            dots: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            speed: 500,
            fade: true,
            accessibility:false,
            touchMove:false,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        return (
            <div className="container more_rooms">

                <Slider ref={c => (this.moreRoomsSlider = c)} {...RoomsSettings}>
                    {this.rooms()}
                </Slider>

                <div style={{ textAlign: "center" }}  id="story_body">

                    <button className="arrow_btn prev button" onClick={this.prev}>
                           <span dangerouslySetInnerHTML={{__html: "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"88.213px\" height=\"64.658px\" viewBox=\"0 0 88.213 64.658\" enable-background=\"new 0 0 88.213 64.658\" xml:space=\"preserve\">\n" +
                           "\t<path d=\"M88.213,64.658H0V0h88.213V64.658z M4,60.658h80.213V4H4V60.658z M32.516,40.912l-8.584-8.583l8.584-8.588\n" +
                           "\t\tv6.586h28.938v4H32.516V40.912z\"></path>\n" + " </svg>"}} />
                    </button>

                    <button className="arrow_btn next button" onClick={this.next}>
                            <span dangerouslySetInnerHTML={{__html: "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"88.213px\" height=\"64.658px\" viewBox=\"0 0 88.213 64.658\" enable-background=\"new 0 0 88.213 64.658\" xml:space=\"preserve\">\n" +
                            "\t<path d=\"M88.213,64.658H0V0h88.213V64.658z M4,60.658h80.213V4H4V60.658z M32.516,40.912l-8.584-8.583l8.584-8.588\n" +
                            "\t\tv6.586h28.938v4H32.516V40.912z\"></path>\n" +
                            "\n" +
                            "</svg>"}} />
                    </button>

                </div>

            </div>
        );
    }
}
