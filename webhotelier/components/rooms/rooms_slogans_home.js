import React, {Component} from 'react'
import Link from 'next/link'
// import ObservableDiv from 'intersection-observer'
import Observer from '@researchgate/react-intersection-observer';

export default class ShowRoomsAndSlogans extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rooms : this.props.rooms,
            layouts : this.props.rooms_and_layouts,
            language: this.props.language,
            ssrNo: null
        }
    }

    componentWillReceiveProps(nextProps){

        this.setState(prevState => ({
            rooms : nextProps.rooms,
            layouts : nextProps.rooms_and_layouts,
            language: nextProps.language
        }));

    }

    componentDidMount(){

        let Language = this.props.language

        const moreRooms = this.state.rooms.map(function(item,i){

            let codes = item.room_name.split(' ').join('-').toLowerCase();
            let pathname = '/room/' + codes;
            let newUrl = '/room?roomslug=' + codes + '&lng=en';


            if( Language !== undefined){

                switch(Language) {
                    case 'el':

                        pathname = '/el/room/' + codes;
                        newUrl = '/room?roomslug=' + codes + '&lng=el';

                        break;
                    default:

                    // code block
                }

            }

            let styles = {
                backgroundImage: `url(${item.featured_image})`,
            };

            return (

                <div  key={i} className="col-md-6">
                    <ImageDiv>
                        <Link href={newUrl} as={pathname} >
                            <div className="room_single_acc">

                                <div className="sliderHome _small_height lazy-bg" style={styles}>

                                </div>

                                <div className="pos-abs-rooms">
                                    <div className="cnt">{item.room_name}</div>
                                </div>

                                <div className="opac_black whp">

                                </div>

                            </div>
                        </Link>
                    </ImageDiv>
                </div>

            )

        })

        this.setState(prevState => ({
            ssrNo : moreRooms
        }));


    }



    slogan(){

        const slogans = this.state.layouts.map(function(item,i){

            let styles = {
                background: `${item.background_color}`,
            };
            let color = {
                color: `${item.color}`,
            };

            return (<div key={i} className="col-md-6">
                <div className="room_single_acc nohover">

                    <div className="sliderHome _small_height" style={styles}>

                    </div>

                    <div className="pos-abs-slogan primary_heading">
                        <div className="cnt" style={ color }>{item.text}</div>
                    </div>

                    <div className="opac_black">

                    </div>

                </div>
            </div>)

        })

        return slogans;
    }

    render() {
        return (
            <div className="row show-rooms-cont no-padding-col">
                {this.state.ssrNo}
                {this.slogan()}
            </div>
        );
    }
}



class ImageDiv extends React.Component {

    constructor(props) {
        super(props);
        this.state = { show: false  };
        this.handleIntersection = this.handleIntersection.bind(this);

    }

    handleIntersection(event) {

        if(event.isIntersecting){
            this.setState({ show: true})
        }

    }


    render() {


        const { show } = this.state;
        let options = {
            onChange: this.handleIntersection,
            root: '#scrolling-container',
            threshold: 0.25,
            disabled: show,
            rootMargin: '400px'
        };

        return (

            <div  className={(show ? "revealed" : "no-reveal")}>
                <Observer {...options}>
                    { this.props.children }
                </Observer>
            </div>

        );
    }
}


