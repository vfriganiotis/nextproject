import Link from 'next/link'
import Head from 'next/head'
import Menu from '../static/menu.json'
import OptionsContact from '../static/pages/contact.json'
import Options from '../static/options.json'
import Header from '../components/header'
import Footer from '../components/footer'
import getConfig from 'next/config'
import MenuEl from '../static/menu-el.json'
import FooterI from '../components/footers/footerI'
import CookieConsent from "react-cookie-consent"

//const localBusiness = import('../static/schemas/schema.json')

//GET NODE ENV VARIABLES
const {publicRuntimeConfig} = getConfig()
const {WP_URL,NEXT_URL} = publicRuntimeConfig

const options = Options;

const contactOptions = OptionsContact;

let skype,fax,tel,email,bookurl,facebook,instagram,twitter,WEBMASTER_TOOLS,GOOGLE_ANALYTICS;

if (options.bookNow) {
    bookurl = <a href={options.bookNow} target={"_blank"} className='bookNow' >BOOK NOW</a>;
}

if (options.webMasterTools) {
    WEBMASTER_TOOLS = options.webMasterTools;
}

if (options.googleAnalytics) {
    GOOGLE_ANALYTICS = options.googleAnalytics;
}

if (options.facebook) {
    facebook = <a href={options.facebook} target={"_blank"}><span className="fa fa-facebook color--brand"></span></a>;
}

if (options.instagram) {
    instagram = <a href={options.instagram} target={"_blank"}><span className="fa fa-instagram color--brand"></span></a>;
}

if (options.twitter) {
    twitter = <a href={options.twitter} target={"_blank"}><span className="fa fa-twitter color--brand"></span></a>;
}


if (contactOptions.fax_number !== "") {
    fax = <div className='fax'><span className="fa fa-fax color--brand"></span> {contactOptions.fax_number}</div>;
}

if (contactOptions.telephone) {
    contactOptions.telephone.map(function(item){
        let fixTel = "tel:" + item ;
        tel = <a href={fixTel} className='tel'><span className="fa fa-phone color--brand"></span> {item}</a>;
    })
}

if (contactOptions.mail) {
    let fixEmail = "mailto:" + contactOptions.mail;
    email = <a href={fixEmail} className='email'><span className="fa fa-envelope color--brand"></span>{contactOptions.mail}</a>;
}

function MenuChilds(MENU, lang , mobile  ){

    let submenu,submenuv,newMenu;


    if( mobile === undefined ){
        if( lang !== 'en' ){

            newMenu = MENU.map(function(item , i ){

                submenu = item.menu_item_parent;

                if( parseInt(submenu,10) > 0){

                    submenuv = parseInt(submenu,10)

                    let pathname = '/' + lang +'/room/' + item.title.split(' ').join('-').toLowerCase();
                    let newUrl = '/room?id=' + item.title.split(' ').join('-').toLowerCase() + "&lng=el&roomslug=" + item.title.split(' ').join('-').toLowerCase();
                    let room = <li key={i}>
                        <Link href={newUrl} as={pathname} >
                            <a>{item.title}</a>
                        </Link>
                    </li>;
                    return room;
                }

            })

        }else{
            newMenu = MENU.map(function(item , i ){

                submenu = item.menu_item_parent;

                if( parseInt(submenu,10) > 0){

                    submenuv = parseInt(submenu,10)
                    let pathname = '/room/' + item.title.split(' ').join('-').toLowerCase();
                    let newUrl = '/room?id=' + item.title.split(' ').join('-').toLowerCase() + "&lng=" + item.item_lang + "&roomslug=" + item.title.split(' ').join('-').toLowerCase();
                    let room = <li key={i}>
                        <Link href={newUrl} as={pathname} >
                            <a>{item.title}</a>
                        </Link>
                    </li>;
                    return room;
                }

            })
        }
    }else{

        if( lang !== 'en' ){

            newMenu = MENU.map(function(item , i ){

                submenu = item.menu_item_parent;

                if( parseInt(submenu,10) > 0){

                    submenuv = parseInt(submenu,10)

                    let pathname = '/' + lang +'/room/' + item.title.split(' ').join('-').toLowerCase();
                    let newUrl = '/room?id=' + item.title.split(' ').join('-').toLowerCase() + "&lng=el&roomslug=" + item.title.split(' ').join('-').toLowerCase();
                    let room = <li key={i}>
                        <Link href={pathname} as={pathname} >
                            <a>{item.title}</a>
                        </Link>
                    </li>;
                    return room;
                }

            })

        }else{
            newMenu = MENU.map(function(item , i ){

                submenu = item.menu_item_parent;

                if( parseInt(submenu,10) > 0){

                    submenuv = parseInt(submenu,10)
                    let pathname = '/room/' + item.title.split(' ').join('-').toLowerCase();
                    let newUrl = '/room?id=' + item.title.split(' ').join('-').toLowerCase() + "&lng=" + item.item_lang + "&roomslug=" + item.title.split(' ').join('-').toLowerCase();
                    let room = <li key={i}>
                        <Link href={pathname} as={pathname} >
                            <a>{item.title}</a>
                        </Link>
                    </li>;
                    return room;
                }

            })
        }


    }

    return newMenu

}

function langFixer( MENU , SHOWROOMS , lang ){
    let newMenu;
    if( lang !== undefined){

        newMenu = MENU.map(function(item , i ){

            let Mmenu = item.menu_item_parent;

            if( parseInt(Mmenu,10) === 0){

                if( item.slug === 'accommodation' ){
                    let pathname = "/" + lang +"/" + item.slug;
                    let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

                    let Toplvl=<li key={i} className="menu-item-has-children">
                        <Link href={newUrl}  as={pathname}>
                            <a>{item.title.toLowerCase()}</a>
                        </Link>
                        <ul className="sub_menu">
                            {SHOWROOMS}
                        </ul></li>;

                    return Toplvl;

                }else{
                    let pathname = "/" + lang +"/" + item.slug;
                    let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

                    let Toplvl = <li key={i}>
                        <Link href={newUrl} as={pathname}>
                            <a>{item.title.toLowerCase()}</a>
                        </Link>
                    </li>;

                    return Toplvl;
                }

            }

        })
    }else{

        newMenu = MENU.map(function(item , i ){

            let Mmenu = item.menu_item_parent;

            if( parseInt(Mmenu,10) === 0){

                if( item.slug === 'accommodation' ){
                    let pathname = "/" + item.slug;
                    let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

                    let Toplvl=<li key={i} className="menu-item-has-children">
                        <Link href={newUrl}  as={pathname}>
                            <a>{item.title.toLowerCase()}</a>
                        </Link>
                        <ul className="sub_menu">
                            {SHOWROOMS}
                        </ul></li>;

                    return Toplvl;

                }else{
                    let pathname = "/" + item.slug ;
                    let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

                    let Toplvl = <li key={i}>
                        <Link href={newUrl} as={pathname}>
                            <a>{item.title.toLowerCase()}</a>
                        </Link>
                    </li>;

                    return Toplvl;
                }

            }

        })
    }

    return newMenu
}

function langFixerMobile( MENU , SHOWROOMS , lang , overview ){
    let newMenu;
    if( lang !== undefined){

        newMenu = MENU.map(function(item , i ){

            let Mmenu = item.menu_item_parent;

            if( parseInt(Mmenu,10) === 0){

                if( item.slug === 'accommodation' ){

                    let pathname = "/" + lang +"/" + item.slug;
                    let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

                    let Toplvl=<li key={i} className="menu-item-has-children">
                        <Link href={'#/'}  as={'#/'}>
                            <a>{item.title.toLowerCase()}</a>
                        </Link>

                        <ul className="sub_menu">
                            <Link href={newUrl}  as={pathname}>
                                <a>{overview}</a>
                            </Link>
                            {SHOWROOMS}
                        </ul></li>;

                    return Toplvl;

                }else{
                    let pathname = "/" + lang +"/" + item.slug;
                    let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

                    let Toplvl = <li key={i}>
                        <Link href={newUrl} as={pathname}>
                            <a>{item.title.toLowerCase()}</a>
                        </Link>
                    </li>;

                    return Toplvl;
                }

            }

        })
    }else{

        newMenu = MENU.map(function(item , i ){

            let Mmenu = item.menu_item_parent;

            if( parseInt(Mmenu,10) === 0){

                if( item.slug === 'accommodation' ){
                    let pathname = "/" + item.slug;
                    let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

                    let Toplvl=<li key={i} className="menu-item-has-children">

                        <Link href={'#/'}  as={'#/'}>
                            <a>{item.title.toLowerCase()}</a>
                        </Link>
                        <ul className="sub_menu">
                            <Link href={newUrl}  as={pathname}>
                                <a>Overview</a>
                            </Link>
                            {SHOWROOMS}
                        </ul></li>;

                    return Toplvl;

                }else{
                    let pathname = "/" + item.slug ;
                    let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

                    let Toplvl = <li key={i}>
                        <Link href={newUrl} as={pathname}>
                            <a>{item.title.toLowerCase()}</a>
                        </Link>
                    </li>;

                    return Toplvl;
                }

            }

        })
    }

    return newMenu
}

const Show_rooms = MenuChilds(Menu,'en')
const Show_roomsEl = MenuChilds(Menu,'el')
const Show_roomsMobileEl = MenuChilds(Menu,'el','mobile')
const Show_roomsMobile = MenuChilds(Menu,'en','mobile')
const MenuVDesktop = langFixer(Menu,Show_rooms)
const MenuVDesktopEl = langFixer(MenuEl,Show_roomsEl,'el')
const MenuVMobileEl = langFixerMobile(MenuEl,Show_roomsMobileEl,'el','περιληψη')
const MenuVMobile = langFixerMobile(Menu,Show_roomsMobile)


export default class extends React.Component {

    constructor(props) {
        super(props);
        let goMemu,mobMenu,langBooking;

        goMemu = MenuVDesktop
        mobMenu = MenuVMobile
        langBooking = 'BOOK NOW'
        if( this.props.menu.firstLoad !== undefined){

             switch(this.props.menu.firstLoad) {
                 case 'el':
                     goMemu = MenuVDesktopEl
                     mobMenu = MenuVMobileEl
                     langBooking = 'ΚΡΑΤΗΣΗ'
                     break;
                 default:
                     goMemu = MenuVDesktop
                     mobMenu = MenuVMobile
                     langBooking = 'BOOK NOW'
                 // code block
             }

        }

        this.state = {
            menu : goMemu,
            mobileMenu: mobMenu,
            langBooking: langBooking
        };

    }

    componentDidMount () {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('/service-worker.js')
                .then(registration => {
                    console.log('service worker registration successful')
                })
                .catch(err => {
                    console.warn('service worker registration failed', err.message)
                })
        }
    }

    componentWillReceiveProps(nextProps){

        let newMemu,mobileMenu;

        switch(nextProps.menu.firstLoad) {

            case 'el':
                newMemu = MenuVDesktopEl
                mobileMenu = MenuVMobileEl
                break;
            default:
                newMemu = MenuVDesktop
                mobileMenu = MenuVMobile
            // code block
        }

        this.setState(prevState => ({
            menu : newMemu,
            mobileMenu: mobileMenu
        }));

    }

    render() {

        return (
            <div>

                <Head>
                    <title>{ this.props.title }</title>
                    <meta name="description" content={ this.props.description } />
                    <meta name="keywords" content={ this.props.keywords } />
                    <meta charSet='utf-8' />
                    <meta name='viewport' content='initial-scale=1.0, width=device-width' />

                    <meta name="google-site-verification" content={WEBMASTER_TOOLS} />

                    <script type="text/javascript" src="https://www.google-analytics.com/analytics.js" async />

                    <script type="text/javascript" src={`https://www.googletagmanager.com/gtag/js?id=${GOOGLE_ANALYTICS}`} async />

                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
                            window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
                            ga('create', '${GOOGLE_ANALYTICS}', 'auto');
                            ga('send', 'pageview');
                          `,
                        }}
                    />

                    <link rel="canonical" href={NEXT_URL} />

                    { this.props.localBusiness && <script type='application/ld+json' dangerouslySetInnerHTML={ { __html: `${JSON.stringify(this.props.localBusiness)}`}} />}

                    <link rel="preconnect dns-prefetch" href={NEXT_URL}/>
                    <link rel="preconnect dns-prefetch" href={"https://" + WP_URL} crossOrigin/>

                    <link rel="preconnect dns-prefetch" href="https://fonts.gstatic.com" crossOrigin/>
                    <link rel="preconnect dns-prefetch" href="http://netdna.bootstrapcdn.com" crossOrigin/>
                    <link rel="dns-prefetch" href="//cdnjs.cloudflare.com" crossOrigin/>
                    <link rel="dns-prefetch" href="//ajax.googleapis.com" crossOrigin/>
                    <link rel="dns-prefetch" href="//s.w.org" crossOrigin/>

                    <link rel="preload" href={  NEXT_URL + "/static/themeOne/style.min.css" } as="style"/>
                    <link rel="preload" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600" as="font" crossOrigin/>

                    <link rel="stylesheet" type="text/css" href={"/static/criticalhome.min.css"} />

                    <link rel="shortcut icon" href="/static/favicon.ico" />

                    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600&display=swap" rel="stylesheet" />

                    {/*<script src="https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver" />*/}
                    {/*<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />*/}
                    {/*<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.9.8" type="text/css" media="all" />*/}
                    {/*<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />*/}
                    {/*<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />*/}

                    {/*<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap-grid.css" rel="stylesheet" />*/}
                    {/*<link href="https://cdnjs.cloudflare.com/ajax/libs/react-datepicker/2.5.0/react-datepicker.css" rel="stylesheet" />*/}
                    {/*<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/1.1.3/hamburgers.min.css" />*/}

                    {/*<link rel="stylesheet" type="text/css" href={"/static/reset.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={"/static/fonts_colors.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={"/static/app.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={"/static/accommodation.css"} />*/}

                    {/*<link rel="stylesheet" type="text/css" href={"/static/themeOne/app.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={"/static/themeOne/btn.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={"/static/themeOne/header.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={"/static/themeOne/footer.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={"/static/themeOne/booking.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={"/static/themeOne/media.css"} />*/}

                    {/*<link rel="stylesheet" type="text/css" href={"/static/themeOne/style.min.css"} />*/}

                </Head>

                <div className="container-fluid">

                    <Header mobileLogo={ options.mobileLogo } logo={ options.logo } menus={this.state.menu} mobileMenu={this.state.mobileMenu} book={options.bookNow} langBooking={this.state.langBooking} />

                    <CookieConsent
                            location="bottom"
                            buttonText="Accept"
                            buttonClasses="accept_privacy"
                            cookieName="myAwesomeCookieName2"
                            style={{ background: "#2B373B" }}
                            buttonStyle={{ color: "#ffffff", fontSize: "16px"}}
                            expires={150}
                        >
                        This website uses cookies to ensure you get the best experience on our website
                        <Link href={'/privacy'} as={'/privacy'}>
                            <a className="btns privacy_button" style={{ color: "#fff" , paddingLeft : "20px" }}>Privacy Policy</a>
                        </Link>
                    </CookieConsent>

                    { this.props.children }

                    <FooterI logourl={options.logo} name={options.hotelName} twitter={twitter} facebook={facebook} instagram={instagram} tel={tel} email={email} />

                </div>

                {/*<script async src={"/static/intersection-observer.js"} />*/}
                <script defer src={"/static/webfonts.js"} />

            </div>
        )
    }

}


