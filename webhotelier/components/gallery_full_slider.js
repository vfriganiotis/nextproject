import React, {Component} from 'react'
import Slider from "react-slick"

export default class FullSlider extends React.Component {

    constructor(props) {
        super(props);

        this.prev = this.prev.bind(this);
        this.next = this.next.bind(this);
        this.state = { images: this.props.images , settings: this.props.settings , dimensions: {
            height: [],width: []
        } ,galleries : "",loaded: false};
        // this.onImgLoad = this.onImgLoad.bind(this);

    }

    componentWillReceiveProps(){
        this.images()
        this.HeroSlider.slickGoTo(0)
    }


    componentWillMount(){

        if (!this.state.settings) {
            this.setState({settings: {
                dots: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 4000,
                speed: 500,
                fade: true,
                accessibility:false,
                touchMove:false,
                slidesToShow: 1,
                slidesToScroll: 1
            }})
        }

    }

    next(){
        this.HeroSlider.slickNext();
    }

    prev(){
        this.HeroSlider.slickPrev();
    }

    componentDidMount(){

        this.images()

    }

    images(){

        return this.props.images.map(function(item,i){

            return (<div key={i} >
                <div className="sliderHome _full_height">
                    <Img src={item} />
                </div>
            </div>)

        })


    }

    render() {

        let IMAGES = this.images()

        return (
            <div>
                <Slider ref={c => (this.HeroSlider = c)} {...this.state.settings}>

                    {IMAGES}

                </Slider>

                <div style={{ textAlign: "center" }}  id="story_body">

                    <button className="arrow_btn prev button" onClick={this.prev}>
                        <img src={"/static/assets/sma-arrow-left.png"} />
                    </button>

                    <button className="arrow_btn next button" onClick={this.next}>
                        <img src={"/static/assets/sma-arrow-right.png"} />
                    </button>

                </div>
            </div>

        );
    }
}


class Img extends React.Component {
    state = { biggerWidth: false };

    onLoad = ({target:img}) => {

        if(img.offsetWidth >= img.offsetHeight){
            this.setState({ biggerWidth: true });
        }

    };

    render() {
        const { biggerWidth } = this.state;

        return (
            <img
                className={(biggerWidth ? "bgWidth" : "bgHeigth")}
                src={this.props.src}
                onLoad={this.onLoad}
            />
        );
    }
}
