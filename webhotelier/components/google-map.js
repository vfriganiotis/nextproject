import React, {Component} from 'react'
import GoogleMapReact from 'google-map-react';

export default class GoogleMapComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {lat: parseFloat(this.props.lat) , lon: parseFloat(this.props.lon) , icon: this.props.icon};
    }

    renderMarkers(map, maps) {

        let marker = new maps.Marker({
            position: {
                lat: this.state.lat,
                lng: this.state.lon
            },
            map
        });

    }

    render() {

        const mapCenterOpt = {
            center: {
                lat: this.state.lat,
                lng: this.state.lon
            },
            zoom: 12,
            // icon: this.state.data.logourl
        };

        return (
            <div style={{ height: '100vh', width: '100%' }}  className='_animation'>
                <GoogleMapReact options={{gestureHandling: 'greedy' ,scrollwheel: false}}
                                bootstrapURLKeys={{ key: 'AIzaSyAweofWPFAadH7psVjPjL44Vm7135bjgac' }}
                                defaultCenter={mapCenterOpt.center}
                                defaultZoom={mapCenterOpt.zoom}
                                defaultScrollwheel={mapCenterOpt.scrollwheel}
                                defaultIcon={mapCenterOpt.icon}
                                gestureHandling={'greedy'}
                                onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}
                >
                </GoogleMapReact>
            </div>
        );
    }
}
