import React,{Component} from 'react'

export default class ServicesComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    showContainers() {

        let count = 2;

        return this.props.services.map(function(item,i) {

            let styles = {
                background: `url(${item.image})`,
            };

            let pathname = '/photos/' + item.post_slug;
            let newUrl = '/photos?id=' + item.post_slug;

            i = i + 2

            if( i % 2 === 0 ){
                return (

                    <div key={i} className="row" >
                        <div className="col-md-6 serv_bg_img" style={styles}></div>

                        <div className="col-md-6 left-side-pull">
                           <div className="services-content">
                               <div className="primary_title font-openSans text-center">
                                    {item.post_name}
                                </div>
                               <div className="primary_text" dangerouslySetInnerHTML={{ __html: item.content }} />
                           </div>

                        </div>
                    </div>

                )
            }else{
                return (

                    <div key={i} className="row" >

                        <div className="col-md-6 right-side-pull">
                            <div className="services-content">
                                <div className="primary_title text-center">
                                    {item.post_name}
                                </div>
                                <div className="primary_text" dangerouslySetInnerHTML={{ __html: item.content }} />
                            </div>
                        </div>

                        <div className="col-md-6 serv_bg_img" style={styles}></div>
                    </div>

                )
            }

        })

    }

    render() {
        return (
            <div className={"services-cont"}>

               { this.showContainers() }

            </div>
        );
    }
}