import Link from 'next/link'
import Head from 'next/head'
import Menu from '../static/menu.json'
import OptionsContact from '../static/pages/contact.json'
import Options from '../static/options.json'
import Header from '../components/header'
import getConfig from 'next/config'
import MenuEl from '../static/menu-el.json'
import FooterI from '../components/footers/footerI'
import CookieConsent from "react-cookie-consent"

//GET NODE ENV VARIABLES
const {publicRuntimeConfig} = getConfig()
const {API_URL} = publicRuntimeConfig

const options = Options;

const contactOptions = OptionsContact;

let skype,fax,tel,email,bookurl,facebook,instagram,twitter;

if (options.bookNow) {
    bookurl = <a href={options.bookNow} target={"_blank"} className='bookNow' >BOOK NOW</a>;
}

if (options.facebook) {
    facebook = <a href={options.facebook} target={"_blank"}><span className="fa fa-facebook color--brand"></span></a>;
}

if (options.instagram) {
    instagram = <a href={options.instagram} target={"_blank"}><span className="fa fa-instagram color--brand"></span></a>;
}

if (options.twitter) {
    twitter = <a href={options.twitter} target={"_blank"}><span className="fa fa-twitter color--brand"></span></a>;
}


if (contactOptions.fax_number !== "") {
    fax = <div className='fax'><span className="fa fa-fax color--brand"></span> {contactOptions.fax_number}</div>;
}

if (contactOptions.telephone) {
    contactOptions.telephone.map(function(item){
        let fixTel = "tel:" + item ;
        tel = <a href={fixTel} className='tel'><span className="fa fa-phone color--brand"></span> {item}</a>;
    })
}

if (contactOptions.mail) {
    let fixEmail = "mailto:" + contactOptions.mail;
    email = <a href={fixEmail} className='email'><span className="fa fa-envelope color--brand"></span>{contactOptions.mail}</a>;
}

let submenu;
let submenuv;
const Show_rooms = Menu.map(function(item , i ){

    submenu = item.menu_item_parent;

    if( parseInt(submenu,10) > 0){

        submenuv = parseInt(submenu,10)
        let pathname = '/room/' + item.title.split(' ').join('-').toLowerCase();
        let newUrl = '/room?id=' + item.title.split(' ').join('-').toLowerCase() + "&lng=" + item.item_lang + "&roomslug=" + item.title.split(' ').join('-').toLowerCase();
        let room = <li key={i}>
            <Link href={newUrl} as={pathname} >
                <a>{item.title}</a>
            </Link>
        </li>;
        return room;
    }

})

let submenuel;
let submenuvel;
const Show_roomsEl = Menu.map(function(item , i ){

    submenuel = item.menu_item_parent;

    if( parseInt(submenuel,10) > 0){

        submenuvel = parseInt(submenuel,10)

        // let pathname = "/el/" + item.title.split(' ').join('-').toLowerCase() ;
        // let newUrl =  "/" + item.title.split(' ').join('-').toLowerCase() + "?lng=" + item.item_lang;

        let pathname = '/el/room/' + item.title.split(' ').join('-').toLowerCase();
        let newUrl = '/room?id=' + item.title.split(' ').join('-').toLowerCase() + "&lng=el&roomslug=" + item.title.split(' ').join('-').toLowerCase();
        let room = <li key={i}>
            <Link href={newUrl} as={pathname} >
                <a>{item.title}</a>
            </Link>
        </li>;
        return room;
    }

})

const MenuVDesktop = Menu.map(function(item , i ){

    let Mmenu = item.menu_item_parent;

    if( parseInt(Mmenu,10) === 0){

        if( submenuv === parseInt( item.ID ,10)){
            let pathname = "/" + item.slug;
            let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

            let Toplvl=<li key={i} className="menu-item-has-children">
                <Link href={newUrl}  as={pathname}>
                    <a>{item.title.toLowerCase()}</a>
                </Link>
                <ul className="sub_menu">
                    {Show_rooms}
                </ul></li>;

            return Toplvl;

        }else{
            let pathname = "/" + item.slug ;
            let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

            let Toplvl = <li key={i}>
                <Link href={newUrl} as={pathname}>
                    <a>{item.title.toLowerCase()}</a>
                </Link>
            </li>;

            return Toplvl;
        }

    }

})


const MenuVDesktopEl = MenuEl.map(function(item , i ){

    let Mmenu = item.menu_item_parent;

    if( parseInt(Mmenu,10) === 0){

        if( submenuv === parseInt( item.ID ,10)){
            let pathname = "/el/" + item.slug;
            let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

            let Toplvl=<li key={i} className="menu-item-has-children">
                <Link href={newUrl}  as={pathname}>
                    <a>{item.title.toLowerCase()}</a>
                </Link>
                <ul className="sub_menu">
                    {Show_roomsEl}
                </ul></li>;

            return Toplvl;

        }else{
            let pathname = "/el/" + item.slug;
            let newUrl =  "/" + item.slug + "?lng=" + item.item_lang;

            let Toplvl = <li key={i}>
                <Link href={newUrl} as={pathname}>
                    <a>{item.title.toLowerCase()}</a>
                </Link>
            </li>;

            return Toplvl;
        }

    }

})


export default class extends React.Component {

    constructor(props) {
        super(props);

        let goMemu;

        goMemu = MenuVDesktop

        if( this.props.menu.firstLoad !== undefined){

            switch(this.props.menu.firstLoad) {
                case 'el':
                    goMemu = MenuVDesktopEl
                    break;
                default:
                    goMemu = MenuVDesktop
                // code block
            }

        }

        this.state = {
            menu : goMemu
        };

    }

    componentDidMount () {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('/service-worker.js')
                .then(registration => {
                    console.log('service worker registration successful')
                })
                .catch(err => {
                    console.warn('service worker registration failed', err.message)
                })
        }

        this.setState({
            css : "/static/themeOne/style.min.css"
        })


    }

    componentWillReceiveProps(nextProps){

        let newMemu

        switch(nextProps.menu.firstLoad) {
            case 'el':
                newMemu = MenuVDesktopEl
                break;
            default:
                newMemu = MenuVDesktop
            // code block
        }

        this.setState(prevState => ({
            menu : newMemu
        }));

    }

    render() {

        return (
            <div>

                <Head>
                    <title>{ this.props.title }</title>
                    <meta name="description" content={ this.props.description } />
                    <meta name="keywords" content={ this.props.keywords } />
                    <meta charSet='utf-8' />
                    <meta name='viewport' content='initial-scale=1.0, width=device-width' />

                    <link rel="dns-prefetch" href="https://maps.googleapis.com"/>

                    {/*<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap-grid.css" rel="stylesheet" />*/}
                    {/*<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.9.8" type="text/css" media="all" />*/}
                    {/*<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />*/}
                    {/*<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />*/}
                    {/*<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/1.1.3/hamburgers.min.css" />*/}
                    {/*<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet" />*/}
                    {/*<link href="https://cdnjs.cloudflare.com/ajax/libs/react-datepicker/2.5.0/react-datepicker.css" rel="stylesheet" />*/}

                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/reset.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/fonts_colors.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/app.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/accommodation.css"} />*/}

                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/themeOne/app.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/themeOne/btn.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/themeOne/header.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/themeOne/footer.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/themeOne/booking.css"} />*/}
                    {/*<link rel="stylesheet" type="text/css" href={API_URL + "/static/themeOne/media.css"} />*/}

                    <script defer src={API_URL + "/static/intersection-observer.js"} />

                    <link rel="stylesheet" type="text/css" href={API_URL + "/static/criticalhome.min.css"} />

                </Head>

                <div className="container-fluid">

                    <Header  logo={ options.logo } menus={this.state.menu} book={options.bookNow} />

                    <CookieConsent
                        location="bottom"
                        buttonText="Accept"
                        buttonClasses="accept_privacy"
                        cookieName="myAwesomeCookieName2"
                        style={{ background: "#2B373B" }}
                        buttonStyle={{ color: "#ffffff", fontSize: "16px"}}
                        expires={150}
                    >
                        This website uses cookies to ensure you get the best experience on our website
                        <Link href={'/privacy'} as={'/privacy'}  style={{ color: "#fff" , paddingLeft : "20px" }}>
                            <a className="btns privacy_button">Privacy Policy</a>
                        </Link>
                    </CookieConsent>

                    { this.props.children }

                    <FooterI logourl={options.logo} name={options.hotelName} twitter={twitter} facebook={facebook} instagram={instagram} tel={tel} email={email} />

                </div>

                <link rel="stylesheet" type="text/css" href={API_URL + "/static/themeOne/style.min.css"} />

            </div>
        )
    }

}


