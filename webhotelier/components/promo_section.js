import React,{Component} from 'react'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'

export default class PromoSection extends React.Component {

    constructor(props) {
        super(props);

        let newString = this.props.text.replace(/(<([^>]+)>)/ig, "");

        this.state = {
            title: this.props.title,
            text: newString,
            button: this.props.button,
            link: this.props.link,
            img: this.props.img,
            lines: 5,
            buttonText: "Read More"
        };

        this.showText = this.showText.bind(this)

        this.myRef = React.createRef()

    }

    handleReflow = (rleState) => {
        const {
            clamped,
            text,
        } = rleState
        // do sth...

    }

    scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop - 200)

    showText(){

        console.log(this.showText)
        if( this.state.lines === 5){
            this.setState({
                lines : 500,
                buttonText: "Read Less"
            })
        }else{
            this.setState({
                lines : 5,
                buttonText: "Read More"
            })
        }
        this.scrollToMyRef()
        console.log(this.state.lines)


    }

    componentWillReceiveProps(nextProps){

        let newString =  nextProps.text.replace(/(<([^>]+)>)/ig, "");

        this.setState(prevState => ({
            title:  nextProps.title,
            text: newString,
            button:  nextProps.button,
            link:  nextProps.link,
            img:  nextProps.img,
            buttonText: "Read More"
        }));

    }

    render() {
        return (
            <div className="row" ref={this.myRef}>
                <div className="col-md-7">
                    <img src={this.state.img} />
                </div>
                <div className="col-md-5">
                    <div className="prm_section">
                        <h3 className="title">{this.state.title}</h3>
                        <LinesEllipsis
                            className="text"
                            text= {this.state.text}
                            maxLine={this.state.lines}
                            ellipsis='...'
                            trimRight
                            basedOn='letters'
                            onReflow={this.handleReflow}
                        />

                        <div  />

                        <div className="expandButton" onClick={this.showText}>
                            {this.state.buttonText}
                        </div>
                        <Link href={this.state.link} as={this.state.link}>
                            <a className="btns withArrowSm arrowRight">{this.state.button}</a>
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}