import React, {Component} from 'react'
import GalleryItem from '../components/gallery-item'
import Slider from "react-slick"


export default class Gallery extends React.Component {
    constructor(props) {
        super(props);

        this.state = {isToggleOn: true, url: this.props.url ,total : this.props.url.length ,count: this.props.count, slideCount: 1};
        this.onItemClick = this.onItemClick.bind(this);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.__buttonShowMore = this.__buttonShowMore.bind(this);
        this.showMore = this.showMore.bind(this);
    }

    next(index) {
        this.slider.slickNext();
    }

    previous(index) {
        this.slider.slickPrev();
    }

    showMore(){
        this.setState({ count: this.state.count + 10})
    }

    onItemClick(index) {

        this.slider.slickGoTo(index);
        let elems = document.querySelectorAll('.sliderPopup');
        Array.from( elems ).forEach(function(item) {
            if( !item.classList.contains('show')){
                item.classList.toggle('show')
            }else{
                item.classList.remove('show')
            }
        });

    }

    __fetchImages(){

        const click = this.onItemClick;
        const count = this.state.count;

        let itemsUrl = this.state.url.map(function(item,i){

            if(count >= i){
                return (
                    <li key={i} onClick={() => click(i)}><GalleryItem url={[item]}/></li>
                )
            }

        })

        return itemsUrl

    }

    __fetchGalleryImages(){
        let itemsUrl = this.state.url.map(function(item,i){
            return(
                <GalleryItem key={i} url={[item]} />
            )
        })

        return itemsUrl
    }

    __buttonShowMore(){

        let totalLength = this.state.total - 1;
        let count = this.state.count;

        if( totalLength > count  ){
            return(
                <div onClick={this.showMore} className="button_show">SHOW MORE</div>
            )

        }else{
            return(
                <div onClick={this.showMore} className="hide">SHOW MORE</div>
            )
        }

    }

    render() {

        const settings = {
            dots: false,
            infinite: true,
            speed: 500,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            beforeChange: (current, next) => this.setState({ slideCount: next + 1 })
        };

        return (
            <div>
                <ul className="gallery-col">
                    { this.__fetchImages() }
                </ul>

                {this.__buttonShowMore()}

                <div className="sliderPopup">

                    <div className="sliders_count">{this.state.slideCount} / {this.state.total}</div>

                    <div className="spinner">

                    </div>

                    <div style={{ textAlign: "center" }}  id="story_body">
                        <button className="arrow_btn prv" onClick={this.previous}>

                        <span dangerouslySetInnerHTML={{__html: "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"88.213px\" height=\"64.658px\" viewBox=\"0 0 88.213 64.658\" enable-background=\"new 0 0 88.213 64.658\" xml:space=\"preserve\">\n" +
                        "\t<path d=\"M88.213,64.658H0V0h88.213V64.658z M4,60.658h80.213V4H4V60.658z M32.516,40.912l-8.584-8.583l8.584-8.588\n" +
                        "\t\tv6.586h28.938v4H32.516V40.912z\"></path>\n" + " </svg>"}} />;

                        </button>
                        <button className="arrow_btn next" onClick={this.next}>

                            <span dangerouslySetInnerHTML={{__html: "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"88.213px\" height=\"64.658px\" viewBox=\"0 0 88.213 64.658\" enable-background=\"new 0 0 88.213 64.658\" xml:space=\"preserve\">\n" +
                            "\t<path d=\"M88.213,64.658H0V0h88.213V64.658z M4,60.658h80.213V4H4V60.658z M32.516,40.912l-8.584-8.583l8.584-8.588\n" +
                            "\t\tv6.586h28.938v4H32.516V40.912z\"></path>\n" +
                            "\n" +
                            "</svg>"}} />;

                        </button>
                        <button className="closeX" onClick={this.onItemClick}>
                            ×
                        </button>
                    </div>

                    <Slider className='sl_one' ref={c => (this.slider = c)} {...settings}>
                        { this.__fetchGalleryImages() }
                    </Slider>

                </div>

            </div>
        )
    }
}