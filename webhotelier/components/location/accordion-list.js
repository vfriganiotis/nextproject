import React, {Component} from 'react'

export default class AccordionList extends React.Component{

    constructor(props){
        super(props)

    }

    render(){
        return(
            <div className="accordion_container">
                <div className="accordion-div">
                    <div className="head">Hotel</div>
                </div>
                <div className={this.props.show === true ? 'accordion-div' : 'accordion-div showAcc'}>
                    <div className="head" onClick={this.props.resetMap}>Museams</div>
                    <div className="accordion-content">
                        <ul>
                            {this.props.markers.map(function(item,i){
                                return (
                                    <li key={i} onClick={this.props.showMarker}>{item.heading}</li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }

}
