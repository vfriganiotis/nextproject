import React, {Component} from 'react'

export default class AccordionComponent extends React.Component{

    constructor(props){
        super(props)

        this.state = {

            resetMap : props.resetMap,
            openAccordion: props.openAccordion,
            markers: props.markers
        }

    }

    render(){

        let openAccordion = this.state.openAccordion
        let markers = this.state.markers
        let ShowMarker = this.props.showMarker;
        let showDirections = this.props.showDirections

        return(
            <div>
                <div className="accordion_container">

                    <div className="accordion-div" onClick={ this.props.resetMap }>
                        <div className="head hotel-map-btn"><h1>HOTEL</h1></div>
                    </div>

                    {this.props.taxonomies.map(function(item,i){

                        let MARKS = markers.filter(function(mark){
                            return mark.taxonomies === item.post_slug
                        })

                        let sss = MARKS.map(function(mm,index){

                            return(
                                <li
                                    key={index}
                                   //onMouseEnter={() => ShowMarker(mm.post_id)}
                                    onClick={() => showDirections(mm.post_id)}
                                    className={mm.show !== true ? 'accordion-list' : 'accordion-list showlist'}  >
                                    {mm.post_name}
                                </li>
                            )
                        })

                        return(
                            <div key={i} className={item.show !== true ? 'accordion-div' : 'accordion-div showAcc'}>
                                <div className="head" onClick={() => openAccordion(item.post_slug)}>
                                    <h1>{item.post_name} <i className={item.show !== true ? 'fa fa-angle-right' : 'fa fa-angle-down'}></i></h1>
                                </div>
                                <div className="accordion-content">
                                    <ul>
                                        {sss}
                                    </ul>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }

}
