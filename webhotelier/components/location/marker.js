import React, {Component} from 'react'
import { Marker } from "react-google-maps"
import Link from 'next/link'
import {InfoBox} from "react-google-maps/lib/components/addons/InfoBox";

export default class MyMarker extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            position: {
                lat: props.positionLat,
                lng: props.positionLng
            },
            icon: props.icon,
            visible: props.visible,
            show: props.show,
            key: props.key,
            marker: props.marker,
            markers: props.markers,
            lang: props.lang
        };

        //this.click = this.click.bind(this);
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            markers : nextProps.markers
        })
    }



    render() {

        let closeMarker = this.props.closeMarker
        let showMarker = this.props.showMarker
        let distance = this.props.distance
        let duration = this.props.duration
        let mainLat = this.props.mainLat
        let mainLng = this.props.mainLng
        let Language = this.props.lang

        return (

            this.state.markers.map(function(marker,i){

                let regex = /(<([^>]+)>)/ig;
                let result = marker.content.replace(regex, '');

                let pathname = "/location/" +  marker.taxonomies  + "/" + marker.post_slug
                let itemHref = "/template-map" + "?sight=" + marker.taxonomies + "&slug=" + marker.post_slug

                let ReadMore = <Link href={ itemHref } as={ pathname } >
                    <a>READ MORE</a>
                </Link>

                if( Language !== undefined){

                    switch(Language) {
                        case 'el':

                            pathname = "/el/location/" +  marker.taxonomies  + "/" + marker.post_slug
                            itemHref = "/template-map" + "?sight=" + marker.taxonomies + "&slug=" + marker.post_slug + '&lng=el';

                            ReadMore = <Link href={ itemHref } as={ pathname } >
                                <a>ΠΕΡΙΣΣΟΤΕΡΑ</a>
                            </Link>

                            break;
                        default:

                        // code block
                    }

                }


                let Gpathname = "https://maps.google.com/?saddr=(" + parseFloat(marker.lat) + "," + parseFloat(marker.lng) + ")&daddr=(" + parseFloat(mainLat) + "," + parseFloat(mainLng) + ")"

                let LinkMore = <Link href={ Gpathname } as={ Gpathname }>
                    <a target="_blank">Get Google Info</a>
                </Link>

                return(
                    <Marker
                        key={i}
                        position={{ lat: parseFloat(marker.lat), lng: parseFloat(marker.lng) }}
                        onClick={() => showMarker(marker.post_id) }
                        defaultVisible={true}
                        visible={marker.show}
                        icon={"/static/assets/AMBITIONpng.png"}
                    >
                        { marker.showInfoBox === true && <InfoBox
                            // onCloseClick={ (marker) => marker.show = !marker.show }
                            options={{ closeBoxURL: ``, enableEventPropagation: true ,disableAutoPan: true  }}
                        >
                            <div style={{  opacity: 1 }}>
                                <div style={{ fontSize: `16px`, fontColor: `#08233B` , backgroundColor: `#fff`, padding: `10px` }}>

                                    <div className="closeInfo"  onClick={ () => closeMarker(marker.post_id) }>X</div>

                                    <div className={"info-name"}>
                                        {marker.post_name}
                                    </div>

                                    <div className={"info-image"}>
                                        <img src={marker.image} />
                                    </div>

                                    <div className="text-center">
                                        {distance}
                                        <br></br>
                                        {duration}
                                    </div>

                                    <div className="smallinfomap insite">
                                        {LinkMore}
                                    </div>

                                    <div className={"info-readMore btns"}>
                                        {ReadMore}
                                    </div>

                                </div>
                                <style jsx>{`

                                  .closeInfo{

                                    position: absolute;
                                    right: 20px;
                                  }

                                  .info-name {
                                        text-align: center;
                                        padding: 0 10px;
                                        padding-bottom: 10px;
                                        display: block;
                                        width: 100%;
                                        float: left;
                                    }

                                `}</style>
                            </div>
                        </InfoBox>}


                    </Marker>
                )
            })

        )
      }

}
