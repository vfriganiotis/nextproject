import React,{Component} from 'react'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'

export default class WhyChooseUs extends React.Component {

    constructor(props) {
        super(props);

        let regex = /(<([^>]+)>)/ig;
        let result = props.text.replace(regex, '');

        this.state = {
            title: this.props.title,
            text: result,
            whyChoose: this.props.whyChoose,
            lines: 5,
            whyChooseList: this.props.whyChooseList,
            buttonText: this.props.buttonText,
            faIcon: 'fa-plus'
        };

        this.showText = this.showText.bind(this)

        this.myRef = React.createRef()

    }

    componentWillReceiveProps(nextProps){

        let regex = /(<([^>]+)>)/ig;
        let result = nextProps.text.replace(regex, '');

        this.setState(prevState => ({
            title: nextProps.title,
            text: result,
            whyChoose: nextProps.whyChoose,
            whyChooseList: nextProps.whyChooseList,
            buttonText: nextProps.buttonText,
        }));

    }

    scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop - 200)

    showText(){

        console.log(this.showText)
        if( this.state.lines === 5){
            this.setState({
                lines : 500,
                buttonText: this.props.buttonLess,
                faIcon:  'fa-minus'
            })
        }else{
            this.setState({
                lines : 5,
                buttonText: this.props.buttonText,
                faIcon:  'fa-plus'
            })
        }
        this.scrollToMyRef()
        console.log(this.state.lines)


    }

    handleReflow = (rleState) => {
        const {
            clamped,
            text,
        } = rleState
        // do sth...
    }


    render() {
        return (
            <div className="row overview_choose_cont text-center row-no-padding background_color_white" ref={this.myRef}>
                <div className="col-md-6">
                    <div className="max-cont pad-cont primary_color_body">
                        <h1 className="primary_heading">
                            {this.state.title}
                        </h1>
                        <div className="primary_text primary_color_body">
                            <LinesEllipsis
                                className="text"
                                text= {this.state.text}
                                maxLine={this.state.lines}
                                ellipsis='...'
                                trimRight
                                basedOn='letters'
                                onReflow={this.handleReflow}
                            />
                        </div>

                        <div className="btn_read" onClick={this.showText} >
                            <div className="btn_read">
                                {this.state.buttonText}
                                <span className={"fa primary_color_link " + this.state.faIcon  } >

                                </span>
                            </div>

                        </div>

                    </div>
                </div>
                <div className="col-md-6">
                    <div className="why_choose_us pad-cont primary_color_body">

                        <div className="max-cont">

                            <h2 className="primary_title font-openSans"> {this.state.whyChoose} </h2>

                            <div className="primary_text font-openSans">

                                {this.state.whyChooseList.map(function(item,i){
                                    return(
                                        <div key={i}> {item} </div>
                                    )
                                })}

                            </div>

                            <a href={this.props.link} className="btn font-semibold btn_text small-margin-top">{this.props.buttonNow}</a>

                        </div>

                    </div>
                </div>
            </div>
        );
    }
}