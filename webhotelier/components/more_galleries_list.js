import React, {Component} from 'react'
import Link from 'next/link'

export default class MoreGalleriesList extends React.Component {

    constructor(props) {
        super(props);
        this.state = { showMenu: true, galleries : this.props.galleries };
        this.click = this.click.bind(this);
        this.showMoreGalleries = this.showMoreGalleries.bind(this);
    }

    click(){

        this.setState(prevState => ({
            showMenu: !prevState.showMenu
        }));

    }

    showMoreGalleries(){


        if( this.props.lang !== undefined){

            switch(this.props.lang) {
                case 'el':
                    return this.state.galleries.map(function(item,i){
                        let pathname = "/el/photos/" + item.slug;
                        let newUrl = "/photos?slug=" + item.slug + '&lng=el';
                        let photogallery = <li key={i}>
                            <Link href={newUrl} as={pathname}>
                                <a>{item.name}</a>
                            </Link>
                        </li>;
                        console.log(item)

                        return photogallery;
                    })

                    break;
                default:
                    return this.state.galleries.map(function(item,i){
                        let pathname = "/photos/" + item.slug;
                        let newUrl = "/photos?slug=" + item.slug;
                        let photogallery = <li key={i}>
                            <Link href={newUrl} as={pathname}>
                                <a>{item.name}</a>
                            </Link>
                        </li>;
                        console.log(item)

                        return photogallery;
                    })
                // code block
            }

        }else{
            return this.state.galleries.map(function(item,i){
                let pathname = "/photos/" + item.slug;
                let newUrl = "/photos?slug=" + item.slug;
                let photogallery = <li key={i}>
                    <Link href={newUrl} as={pathname}>
                        <a>{item.name}</a>
                    </Link>
                </li>;
                console.log(item)

                return photogallery;
            })
        }

    }

    render() {

        return (<div>

                <div className={ this.state.showMenu === true ? 'showGalleriesList' : '' } >
                    <h1>CHOOSE A GALLERY:</h1>
                    <div className={this.state.showMenu === true ? 'hamburger hamburger--slider js-hamburger photo_gal' : 'hamburger hamburger--slider js-hamburger photo_gal is-active ' }  onClick= { this.click } >
                        <div className="hamburger-box">
                            <div className="hamburger-inner"></div>
                        </div>
                    </div>

                    <ul className='sub_menu_photos' onClick= { this.click }>
                        {this.showMoreGalleries()}
                    </ul>
                    <div onClick= { this.click } className="close_galleries_menu btn">CLOSE MENU</div>
                </div>
                <style jsx>{`

                   .sub_menu_photos {
                        position: absolute;
                        background: #616161;
                        width: 100%;
                        left: 0;
                        /* top: 60px; */
                        z-index: 2;
                        padding: 0 0 0 15px;
                        /* max-height: 0; */
                        transition: max-height .55s ease-out;
                        color: #fff;
                        overflow: hidden;
                    }

                `}</style>
            </div>

        );
    }
}
