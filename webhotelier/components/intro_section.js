import React,{Component} from 'react'

export default class IntroSection extends React.Component {

    render() {

        return (
           <div>
               <h2 className="maintitle">{this.props.title}</h2>
               <h2 className="subtitle">{this.props.subtitle}</h2>
           </div>
        );
    }

}