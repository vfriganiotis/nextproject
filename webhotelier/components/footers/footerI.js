import React, {Component} from 'react'

export default class FooterI extends React.Component {

    render() {

        return (<footer className="footer-v2">
                <div className="footer_bottom">
                        <div className="row">
                            <div className="col-md-6">

                                <div className="copyright_site">
                                    <h1>{this.props.name}</h1>
                                </div>
                                <div>
                                    Web design &amp; Development by: <a href="http://www.marinet.gr/">Marinet</a>  | Hotel Booking Engine: <a href="https://www.webhotelier.net/" target="_blank">Webhotelier</a>
                                </div>


                            </div>
                            <div className="col-md-6">

                                <div className="social-footer primary_color_link">

                                    <span className="font-openSans primary_color_body font-bold">FOLLOW US</span>

                                    <ul>
                                        <li>{this.props.facebook}</li>
                                        <li>{this.props.twitter}</li>
                                        <li>{this.props.instagram}</li>
                                    </ul>

                                </div>

                            </div>
                        </div>
                    </div>

                {/*<div className="copyright text-center">*/}
                {/*<h1>Design and Development By <a href="http://www.marinet.gr/hotelmarketing/" target="_blank">Marinet Ltd</a></h1>*/}
                {/*</div>*/}
            </footer>
        );
    }
}
