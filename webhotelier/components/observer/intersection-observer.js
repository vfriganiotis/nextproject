import React,{Component} from 'react'
import Observer from '@researchgate/react-intersection-observer';

export default class ObservableDiv extends React.Component {

    constructor(props) {
        super(props);
        this.state = { show: false  };
        this.handleIntersection = this.handleIntersection.bind(this);
    }

    handleIntersection(event) {

        if(event.isIntersecting){

            this.setState({ show: true})

        }

    }

    render() {

        const { show } = this.state;
        let options = {
            onChange: this.handleIntersection,
            root: '#scrolling-container',
            threshold: 0.25,
            disabled: show
        };

        return (

            <div  className={(show ? "col-md-3 revealed" : "col-md-3 no-reveal")}>
                <Observer {...options}>

                    { this.props.children }

                </Observer>
            </div>

        );
    }
}