import React,{Component} from 'react'

export default class ThreeLayoutComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reviews: this.props.reviews
        };
    }

    showContainers() {

        return this.state.reviews.map(function(item,i) {

                return (

                    <div key={i} className="col-md-4 review_cont" >

                        <div className="review_inner">
                            <div className="review-heading">

                                <div className={item.name.split("")[0].toLowerCase()}></div>

                                <div className="name-rev">
                                    <div> {item.name} </div>
                                    <div>
                                        <img src={"/static/assets/star-active.png"} />
                                        <img src={"/static/assets/star-active.png"} />
                                        <img src={"/static/assets/star-active.png"} />
                                        <img src={"/static/assets/star-active.png"} />
                                        <img src={"/static/assets/star-active.png"} />
                                    </div>
                                </div>

                            </div>

                            <div className="reviews-content" dangerouslySetInnerHTML={{ __html: item.content }} />

                            <div className="rev-bottom">
                                <img src={"/static/assets/booking-256-1.png"} />
                            </div>

                        </div>
                        <style jsx>{`

                            .name-rev{
                                display: block;
                                width: calc(100% - 60px);
                                float: left;
                                height: 50px;
                                font-size: 24px;
                                text-align: left;
                                padding-left: 10px;
                                padding-top: 17px;
                                padding-bottom: 28px;
                                min-width: 100px;
                             }

                             .review-heading{
                                float: left;
                                margin-bottom: 10px;
                             }

                             .review-heading > div{
                                 float: left;
                             }
                             .review_cont{
                               padding: 20px;
                              }

                              .review_inner{
                                padding: 20px;
                                border: 1px solid #fff;
                                background: #fff;
                              }
                             .reviews-content {
                                margin: 0;
                                color: #777!important;
                                line-height: 24px!important;
                                font-size: 14px!important;
                                position: static!important;
                                height: 100px!important;
                                overflow-y: auto!important;
                                font-size: 14px!important;
                                text-align: left!important;
                                padding: 0 2px 0 0!important;
                                width: 100%;
                                margin-top: 85px;
                                margin-bottom: 20px;
                             }

                             .reviews-content::-webkit-scrollbar {
                                    width: 4px!important;
                                }


                           .reviews-content::-webkit-scrollbar-thumb{

                                -webkit-border-radius: 10px!important;
                                border-radius: 10px!important;
                                background: #ccc!important;
                                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5)!important;
                            }

                            .reviews-content::-webkit-scrollbar-track {
                                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3)!important;
                                -webkit-border-radius: 4px!important;
                                border-radius: 4px!important;
                            }

                            .a, .b, .c, .d, .e,
                            .f, .g, .h, .i, .j,
                            .k, .l, .m, .n, .o,
                            .p, .q, .r, .s, .t,
                            .u, .v, .w, .x, .y,
                            .z
                            { display: inline-block; background: url('/static/assets/spriteLetters.png') no-repeat; overflow: hidden; text-indent: -9999px; text-align: left; }

                            .a { background-position: -0px -0px; width: 60px; height: 60px; }
                            .b { background-position: -60px -0px; width: 60px; height: 60px; }
                            .c { background-position: -120px -0px; width: 60px; height: 60px; }
                            .d { background-position: -180px -0px; width: 60px; height: 60px; }
                            .e { background-position: -240px -0px; width: 60px; height: 60px; }
                            .f { background-position: -0px -60px; width: 60px; height: 60px; }
                            .g { background-position: -60px -60px; width: 60px; height: 60px; }
                            .h { background-position: -120px -60px; width: 60px; height: 60px; }
                            .i { background-position: -180px -60px; width: 60px; height: 60px; }
                            .j { background-position: -240px -60px; width: 60px; height: 60px; }
                            .k { background-position: -0px -120px; width: 60px; height: 60px; }
                            .l { background-position: -60px -120px; width: 60px; height: 60px; }
                            .m { background-position: -120px -120px; width: 60px; height: 60px; }
                            .n { background-position: -180px -120px; width: 60px; height: 60px; }
                            .o { background-position: -240px -120px; width: 60px; height: 60px; }
                            .p { background-position: -0px -180px; width: 60px; height: 60px; }
                            .q { background-position: -60px -180px; width: 60px; height: 60px; }
                            .r { background-position: -120px -180px; width: 60px; height: 60px; }
                            .s { background-position: -180px -180px; width: 60px; height: 60px; }
                            .t { background-position: -240px -180px; width: 60px; height: 60px; }
                            .u { background-position: -0px -240px; width: 60px; height: 60px; }
                            .v { background-position: -60px -240px; width: 60px; height: 60px; }
                            .w { background-position: -120px -240px; width: 60px; height: 60px; }
                            .x { background-position: -180px -240px; width: 60px; height: 60px; }
                            .y { background-position: -240px -240px; width: 60px; height: 60px; }
                            .z { background-position: -0px -300px; width: 60px; height: 60px; }

                        `}</style>
                    </div>

                )
            })

    }

    render() {

        return (
            <div className={"services-cont row"}>

                { this.showContainers() }

            </div>
        );
    }
}
