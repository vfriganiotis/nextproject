import React, {Component} from 'react'

export default class Parallax extends React.Component {


    render() {

        let parallaxStyle = {
            backgroundImage: `url(${this.props.img})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'noRepeat',
            height: 500,
            position: 'relative'
        };

        return (
            <div className="parallax_cont row" style={ parallaxStyle }>
                <div className="abs_pos_center">{this.props.content}</div>
            </div>
        );
    }
}
