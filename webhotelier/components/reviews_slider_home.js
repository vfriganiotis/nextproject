import React,{Component} from 'react'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'
import Slider from "react-slick"

export default class ReviewsSliderHome extends React.Component {

    constructor(props) {
        super(props);

        let regex = /(<([^>]+)>)/ig;
        let result = props.title.replace(regex, '');

        this.state = {
            title: result,
            settings: props.settings,
            slides: props.slides
        };

    }

    componentWillMount(){

        if (!this.state.settings) {
            this.setState({settings: {
                dots: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 4000,
                speed: 500,
                fade: true,
                accessibility:false,
                touchMove:false,
                slidesToShow: 1,
                slidesToScroll: 1
            }})
        }

    }

    componentWillReceiveProps(nextProps){

        let regex = /(<([^>]+)>)/ig;
        let result = nextProps.title.replace(regex, '');

        this.setState(prevState => ({
            title: nextProps.title,
        }));

    }

    reviews(){

        let moreRooms = this.props.slides.map(function(item,i){

            return (<div key={i} >
                <div className="primary_title primary_color_white">{item.heading}</div>
                <div className="primary_color_white small_text font-italic">{item.content}</div>

            </div>)

        })

        return moreRooms;
    }

    render() {
        let styles = {
            background: `${this.props.backgroundColor}`,
        };

        return (
            <div className="overview_choose_cont text-center row-no-padding reviews" ref={this.myRef} style={styles}>

                <div className="max-cont pad-cont ">
                     <span className="tripadvisor__svg  brand_fill">
                           <svg xmlns="http://www.w3.org/2000/svg" width="42.254" height="24.928" viewBox="0 0 42.254 24.928">
                               <circle cx="31.582" cy="14.257" r="1.729"></circle>
                               <path d="M31.582 9.07a5.192 5.192 0 0 0-5.186 5.188 5.193 5.193 0 0 0 5.186 5.186 5.194 5.194 0 0 0 5.188-5.186 5.193 5.193 0 0 0-5.188-5.188zm0 8.793a3.61 3.61 0 0 1-3.605-3.605 3.611 3.611 0 0 1 3.605-3.608 3.61 3.61 0 0 1 3.607 3.608 3.61 3.61 0 0 1-3.607 3.605z"></path><path d="M39.701 7.349l2.189-3.121h-6.697c-.172-.061-.342-.127-.518-.182l.023-.035c-.246-.16-6.123-3.93-13.813-4.01-6.588-.08-12.1 2.992-13.748 4.012l-6.774.215 2.188 3.123A10.622 10.622 0 0 0 0 14.258c0 5.883 4.787 10.67 10.67 10.67 3.217 0 6.1-1.438 8.057-3.697l2.482 3.4 2.34-3.375c1.957 2.244 4.83 3.672 8.033 3.672 5.885 0 10.672-4.787 10.672-10.67 0-2.635-.965-5.046-2.553-6.909zm-10.717-3.43c-3.953.994-7.041 4.189-7.857 8.209a10.703 10.703 0 0 0-8.166-8.29c2.195-.811 4.941-1.502 7.9-1.469 3.123.037 5.946.759 8.123 1.55zM10.67 22.557c-4.576 0-8.299-3.725-8.299-8.299 0-4.577 3.723-8.301 8.299-8.301s8.299 3.725 8.299 8.301c0 4.574-3.723 8.299-8.299 8.299zm20.912 0c-4.574 0-8.299-3.725-8.299-8.299 0-4.577 3.725-8.301 8.299-8.301 4.576 0 8.301 3.725 8.301 8.301 0 4.574-3.725 8.299-8.301 8.299z"></path><circle cx="10.67" cy="14.257" r="1.729"></circle><path d="M10.67 9.07a5.194 5.194 0 0 0-5.188 5.188 5.195 5.195 0 0 0 5.188 5.186 5.194 5.194 0 0 0 5.188-5.186A5.193 5.193 0 0 0 10.67 9.07zm0 8.793a3.61 3.61 0 0 1-3.607-3.605c0-1.99 1.619-3.608 3.607-3.608s3.607 1.618 3.607 3.608a3.61 3.61 0 0 1-3.607 3.605z"></path>
                           </svg>
                       </span>
                    <Slider ref={c => (this.HeroSlider = c)} {...this.state.settings}>

                        {this.reviews()}

                    </Slider>
                </div>

            </div>
        );
    }
}