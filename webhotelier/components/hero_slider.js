import React, {Component} from 'react'
import Slider from "react-slick"

export default class HeroSlider extends React.Component {

    constructor(props) {
        super(props);

        this.prev = this.prev.bind(this);
        this.next = this.next.bind(this);
        this.state = { images: props.images, book: props.book , settings: props.settings };

    }

    componentWillReceiveProps(){
        this.HeroSlider.slickGoTo(0)
    }

    componentWillMount(){

        if (!this.state.settings) {
            this.setState({settings: {
                    dots: true,
                    infinite: true,
                    autoplay: true,
                    autoplaySpeed: 4000,
                    speed: 500,
                    fade: true,
                    accessibility:false,
                    touchMove:false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }})
        }

    }

    next(){
        this.HeroSlider.slickNext();
    }

    prev(){
        this.HeroSlider.slickPrev();
    }

    rooms(){

        let moreRooms = this.props.images.map(function(item,i){

            let styles = {
                background: `url(${item})`,
            };

            return (<div key={i} >
                <div className="sliderHome _full_height" style={styles}>
                </div>
            </div>)

        })

        return moreRooms;
    }

    render() {

        return (

            <div className="col-md-12">
                <Slider ref={c => (this.HeroSlider = c)} {...this.state.settings}>
                    {this.rooms()}
                </Slider>

                <div style={{ textAlign: "center" }}  id="story_body">

                    <button className="arrow_btn prev button" onClick={this.prev}>
                        <img src={"/static/assets/sma-arrow-left.png"} />
                    </button>

                    <button className="arrow_btn next button" onClick={this.next}>
                        <img src={"/static/assets/sma-arrow-right.png"} />
                    </button>

                </div>
            </div>

        );
    }
}
