import React, {Component} from 'react'


export default class HeroZoomImage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {dataUrl: this.props.dataUrl, heading: this.props.heading};
    }


    render() {

        return (
            <div className="hero_cont">
                <div className="has_transition_zoom parallax">
                    <img src={this.state.dataUrl} />
                </div>
                <div className="hero_head">{this.state.heading}</div>
            </div>
        );

    }
}
