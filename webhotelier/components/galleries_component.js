import React,{Component} from 'react'
import galleriesARR from '../static/WPdataGalleries.json'
import Link from 'next/link'
import Observer from '@researchgate/react-intersection-observer';

export default class GalleriesComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            galleries: galleriesARR,
            count: this.props.count,
            total: galleriesARR.length,
            show: false,
            showAll: 1,
            showCont: null
        };

        this.showContainers = this.showContainers.bind(this);
        this.showMore = this.showMore.bind(this);

    }

    componentDidMount(){
        this.setState({
            showCont: this.showContainers(0)
        })
    }


    showContainers(time) {

        let count = this.state.count
        let showALL = this.state.showAll
        let Language = this.props.language

        return this.state.galleries.map(function(item,i) {

            let styles = {
                backgroundImage: `url(${item.gallery_image})`,
            };

            let pathname = '/photos/' + item.post_slug;
            let newUrl = '/photos?slug=' + item.post_slug + '&lng=en';

            if( Language !== undefined){

                switch(Language) {
                    case 'el':

                        pathname = '/el/photos/' + item.post_slug;
                        newUrl = '/photos?slug=' + item.post_slug + '&lng=el';

                        break;
                    default:
                }

            }

            let NewTime = i - time
            if( count >= i ) {
                return (

                    <ImageDiv key={i} count={ NewTime } showAll={showALL} newUrl={newUrl} pathname={pathname} post_name={item.post_name} styles={styles} />

                   )
            }

        })

    }

    __buttonShowMore(){

        if( this.state.total > this.state.count  ){

            return(
                <div onClick={this.showMore} className="btn-c withArrowSm goTopArrow">{this.props.locale}</div>
            )

        }else{
            return(
                <div onClick={this.showMore} className="hide">{this.props.locale}</div>
            )
        }

    }

    showMore(){
        this.setState({
            count:  6,
            showAll : 10
        })

        let self = this;
        setTimeout(function(){
            self.setState({
                showCont: self.showContainers(2.5)
            })
        }, 0)

    }

    render() {

        return (
            <div>

                <div className="row no_m_pad bg_color_f2 transitions_cont">

                    <div className="col-md-3 _small_height">
                        <div className="start_c-abs">
                            <div className="cnt">Gallery</div>
                            <div className="btn-c withArrowSm">{this.props.localeCategory}</div>
                        </div>
                    </div>

                    { this.state.showCont }

                    <div className="show_more_items">
                        {this.__buttonShowMore()}
                    </div>

                </div>

            </div>
        )
    }

}


class ImageDiv extends React.Component {

    constructor(props) {
        super(props);
        this.state = { show: false  };
        this.handleIntersection = this.handleIntersection.bind(this);

    }

    handleIntersection(event) {

        let self = this;
        if(event.isIntersecting){

            let newCount = 0;
            if( this.props.count > 12 ){
                newCount = (this.props.count - 4) * this.props.showAll;
            }else{
                newCount = this.props.count
            }

            setTimeout(function(){
                self.setState({ show: true})
            }, (newCount  * 0.25) * 1000)

        }

    }

    render() {

        const { show } = this.state;
        let options = {
            onChange: this.handleIntersection,
            root: '#scrolling-container',
            threshold: 0.25,
            disabled: show
        };

        return (

            <div  className={(show ? "col-md-3 revealed" : "col-md-3 no-reveal")}>
                <Observer {...options}>
                    <div>
                        <Link href={this.props.newUrl} as={this.props.pathname}>
                            <div className="room_single_acc">

                                <div className="sliderHome _small_height lazy-bg" style={this.props.styles}>

                                </div>

                                <div className="pos-abs _gall">
                                    <div className="cnt">{this.props.post_name}</div>
                                </div>

                                <div className="opac_black">

                                </div>

                            </div>
                        </Link>
                    </div>
                </Observer>
            </div>

        );
    }
}




