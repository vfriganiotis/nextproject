import React, {Component} from 'react'
import ReCAPTCHA from "react-google-recaptcha";
import axios from 'axios';
import getConfig from 'next/config'

//GET NODE ENV VARIABLES
const {publicRuntimeConfig} = getConfig()
const {RECAPTCHA_KEY,NEXT_URL} = publicRuntimeConfig

export default class Contact extends React.Component {

    constructor(props) {
        super(props);

        this.state={
            success : false,
            error: false
        }

        this.exampleRef = React.createRef()

    }

    handleSubmit(e){
        e.preventDefault();
        const name = document.getElementById('name').value;
        const email = document.getElementById('email').value;
        const message = document.getElementById('message').value;

        const recaptchaValue = this.exampleRef.current.getValue();
        // this.props.onSubmit(recaptchaValue);
        console.log(recaptchaValue)
        console.log(message)
        axios({
            method: "POST",
            url: NEXT_URL + "/sendMail",
            data: {
                name: name,
                email: email,
                message: message,
                recapthca: recaptchaValue
            }
        }).then((response)=>{

            if (response.data.msg === 'success'){
                this.setState({
                    success: true
                })
                this.resetForm()
            }else if(response.data.msg === 'fail'){
                this.setState({
                    error: true
                })
            }

        }).catch(error => {
            console.log(error.message);
            this.setState({
                error: true
            })
        })

    }

    resetForm(){
        document.getElementById('contact-form').reset();
    }

    closeForm(){
        this.setState({
            error: false,
            success: false
        })
    }


    render() {
        let key_captcha = RECAPTCHA_KEY
        return (
            <form id="contact-form" onSubmit={this.handleSubmit.bind(this)} method="POST" target="_blank">
                <div className="form-group">
                    <input
                        required
                        type="text"
                        className="form-control"
                        id="name"
                        placeholder="Name"
                    />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" id="phone" placeholder="Telephone"/>
                </div>
                <div className="form-group">
                    <input type="email" className="form-control" id="email" aria-describedby="emailHelp" required placeholder="Email"/>
                </div>
                <div className="form-group">
                    <textarea  required className="form-control" rows="5" id="message" placeholder="Message"></textarea>
                </div>
                <div className="recaptcha-cont">
                    <ReCAPTCHA
                        ref={this.exampleRef}
                        sitekey={key_captcha}
                    />
                </div>

                {this.state.success &&
                    <div className={"message-notifications"}>
                        <div onClick={this.closeForm.bind(this)} className={"close"}>X</div>
                        <div className={"content-message-notifications"}>
                            YOUR EMAIL HAS SENT!!!!
                        </div>
                    </div>
                }

                {this.state.error &&
                  <div className={"message-notifications"}>
                      <div onClick={this.closeForm.bind(this)} className={"close"}>X</div>
                      <div className={"content-message-notifications"}>
                         there is something wrong with the system, please try later
                      </div>
                  </div>
                }

                <button type="submit" className="btn btn-primary">Submit</button>
            </form>

        );
    }
}


