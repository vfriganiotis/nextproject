import React,{Component} from 'react'
import DatePicker from "react-datepicker";
import NoSSR from 'react-no-ssr';

export default class AvailabilityForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            startDate: new Date(),
            checkOutDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
        };
        this.handleChange = this.handleChange.bind(this);
        this.CheckOuthandleChange = this.CheckOuthandleChange.bind(this);
    }

    handleChange(date) {
        this.setState({
            startDate: date,
            checkOutDate: new Date(date.getTime() + 24 * 60 * 60 * 1000)
        });
    }

    CheckOuthandleChange(date) {
        this.setState({
            checkOutDate: date
        });
    }

    render() {

        return (
            <form id="availability_forms" action="https://hotelkallistosantorini.reserve-online.net/" method="POST" target="_blank">

                <div className="row">

                    <div className="col-md-4 small-pad ">
                        <div className="cosmores-txt">ONLINE BOOKING</div>
                    </div>

                    <NoSSR>
                        <div className="col-md-2 small-pad">
                            <DatePicker
                                dateFormat='dd-LL-YYYY'
                                name="checkin"
                                selected={ this.state.startDate }
                                onChange={ this.handleChange }
                            />
                        </div>
                        <div className="col-md-2 small-pad">
                            <DatePicker
                                dateFormat='dd-LL-YYYY'
                                name="checkout"
                                selected={ this.state.checkOutDate }
                                onChange={ this.CheckOuthandleChange }
                            />
                        </div>
                    </NoSSR>

                    <div className="col-md-1 adults_ small-pad">
                        <select id="wh-adults" name="adults" className="form-control">
                            <option value="1">1 adult</option>
                            <option value="2" defaultValue="">2 adults</option>
                            <option value="3">3 adults</option>
                            <option value="4">4 adults</option>
                            <option value="5">5 adults</option>
                            <option value="6">6 adults</option>
                            <option value="7">7 adults</option>
                        </select>
                    </div>

                    <div className="col-md-1 rooms_ small-pad">
                        <select id="wh-rooms" name="rooms" className="form-control">
                            <option value="1">1 room</option>
                            <option value="2">2 rooms</option>
                            <option value="3">3 rooms</option>
                            <option value="4">4 rooms</option>
                            <option value="5">5 rooms</option>
                        </select>
                    </div>

                    <div className="col-md-2 small-pad">
                        <button type="submit" className="btn btn-primary form_book_button">Submit</button>
                    </div>

                </div>

            </form>

        );
    }
}