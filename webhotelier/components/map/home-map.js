import React,{Component} from 'react'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import Observer from '@researchgate/react-intersection-observer';

const GOOGLEMAP = dynamic(() => import('google-map-react'));

// const GOOGLEMAP = import('google-map-react');
// import GoogleMapReact from 'google-map-react';

const exampleMapStyles = [

    {elementType: 'geometry', stylers: [{color: '#f2f2f2'}]},//first
    {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{color: '#000000'}]
    },
    {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{color: '#000000'}]
    },
    {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{color: '#ffffff'}]
    },
    {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{color: '#ffffff'}]
    },
    {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{color: '#C9C9C9'}]//this
    },
    {
        featureType: 'road',
        elementType: 'geometry.stroke',
        stylers: [{color: '#eed3d2'}]
    },
    {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{color: '#C9C9C9'}]//this
    },
    {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{color: '#bb4c49'}]//this
    },
    {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{color: '#1f2835'}]
    },
    {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{color: '#f3d19c'}]
    },
    {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{color: '#2f3948'}]
    },
    {
        featureType: 'transit.station',
        elementType: 'labels.text.fill',
        stylers: [{color: '#d59563'}]
    },
    {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{color: '#02b8e3'}]//water
    },
    {
        featureType: 'water',
        elementType: 'labels.text.fill',//water
        stylers: [{color: '#02b8e3'}]
    },
    {
        featureType: 'water',
        elementType: 'labels.text.stroke',
        stylers: [{color: '#17263c'}]
    }
]

const mapOptions = {
    styles: exampleMapStyles,
    gestureHandling: 'greedy',
    scrollwheel: false
};

export default class HomeMap extends React.Component {

    constructor(props) {
        super(props);

        let regex = /(<([^>]+)>)/ig;
        let result = this.props.text.replace(regex, '');

        let pathname = '/' + this.props.link;
        let newUrl = '/' + this.props.link + '?lng=en';

        if( this.props.language !== undefined){

            switch(this.props.language) {
                case 'el':

                    pathname = '/el/' + this.props.link;
                    newUrl = '/' + this.props.link + '?lng=el';

                    break;
                default:

            }

        }


        this.state = {
            title: this.props.title,
            text: result,
            button: this.props.button,
            pathname: pathname,
            link: newUrl,
            lat: parseFloat(this.props.lat),
            lon: parseFloat(this.props.lon) ,
            icon: this.props.icon,

        };
    }


    componentWillReceiveProps(nextProps){

        let pathname = '/' + nextProps.link;
        let newUrl = '/' + nextProps.link + '?lng=en';

        let regex = /(<([^>]+)>)/ig;
        let result = nextProps.text.replace(regex, '');


        if( nextProps.language !== undefined){

            switch(nextProps.language) {
                case 'el':

                    this.setState(prevState => ({
                        title: nextProps.title,
                        link:  '/' + nextProps.link + '?lng=el',
                        pathname: '/el/' + nextProps.link,
                        text: result,
                    }));

                    break;
                default:

                    this.setState(prevState => ({
                        title: nextProps.title,
                        link:  newUrl,
                        pathname: pathname,
                        text: result,
                    }));

            }

        }

    }


    renderMarkers(map, maps) {

        let marker = new maps.Marker({
            position: {
                lat: this.state.lat,
                lng: this.state.lon
            },
            icon:"../../static/assets/AMBITIONpng.png",
            map
        });

    }

    render() {

        const mapCenterOpt = {
            center: {
                lat: this.state.lat,
                lng: this.state.lon
            },
            zoom: 12,
            // icon: this.state.data.logourl
        };

        let RenderMarkers = ({map, maps}) => this.renderMarkers(map, maps)
        return (
            <div className="row no-padding-col">
                <div className="col-md-12">
                    <div style={{ height: '520px', width: '100%' }}  className='_animation no-padding-col'>

                        <ObservableMap
                              options={mapOptions}
                              bootstrapURLKeys={{ key: 'AIzaSyAweofWPFAadH7psVjPjL44Vm7135bjgac' }}
                              defaultCenter={mapCenterOpt.center}
                              defaultZoom={mapCenterOpt.zoom}
                              defaultScrollwheel={mapCenterOpt.scrollwheel}
                              defaultIcon={mapCenterOpt.icon}
                              onGoogleApiLoaded={RenderMarkers}
                        />

                        <div className="map_contact_details text-center primary_color_white">
                            <div className="primary_title font-openSans ">{this.state.title}</div>
                            <div className="contact-map-opt">
                                <div>{this.props.mail}</div>
                                <div>{this.props.tel}</div>
                                <div>{this.props.text}</div>
                            </div>
                            <Link href={this.state.link} as={this.state.pathname}>
                                <a className="btns">{this.state.button}</a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}



class ObservableMap extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            options: this.props.options,
            bootstrapURLKeys: this.props.bootstrapURLKeys,
            defaultCenter: this.props.defaultCenter,
            defaultZoom:this.props.defaultZoom,
            defaultScrollwheel: this.props.defaultScrollwheel,
            defaultIcon: this.props.defaultIcon,
            onGoogleApiLoaded: this.props.onGoogleApiLoaded

        };
        this.handleIntersection = this.handleIntersection.bind(this);

    }

    handleIntersection(event) {

        if(event.isIntersecting){

            this.setState({
                show: true,
                key: 'AIzaSyAweofWPFAadH7psVjPjL44Vm7135bjgac'
            })

        }

    }

    render() {

        const { show } = this.state;
        let options = {
            onChange: this.handleIntersection,
            root: '#scrolling-container',
            threshold: 0.25,
            disabled: show,
            rootMargin: '850px'
        };

        return (

            <div  className={(show ? "col-md-12 revealed" : "col-md-12 no-revealMaps")}>
                <Observer {...options} >

                    <div style={{ height: '520px', width: '100%' }}>
                        {show === true && <GOOGLEMAP
                            options={mapOptions}
                            bootstrapURLKeys={this.state.bootstrapURLKeys}
                            defaultCenter={this.state.defaultCenter}
                            defaultZoom={this.state.defaultZoom}
                            defaultScrollwheel={this.state.defaultScrollwheel}
                            defaultIcon={this.state.defaultIcon}
                            gestureHandling={'greedy'}
                            onGoogleApiLoaded={this.state.onGoogleApiLoaded}
                        />}
                    </div>

                </Observer>
            </div>

        );
    }
}