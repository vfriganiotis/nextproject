import React from "react"
import { compose, withProps} from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker , DirectionsRenderer  } from "react-google-maps"
import AccordionComponent from "../../components/location/accordion"
import { MarkerClusterer } from "react-google-maps/lib/components/addons/MarkerClusterer";
import MyMarker from  "../../components/location/marker"

const exampleMapStyles = [
    {elementType: 'geometry', stylers: [{color: '#f2f2f2'}]},//first
    {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{color: '#000000'}]
    },
    {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{color: '#000000'}]
    },
    {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{color: '#ffffff'}]
    },
    {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{color: '#ffffff'}]
    },
    {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{color: '#C9C9C9'}]//this
    },
    {
        featureType: 'road',
        elementType: 'geometry.stroke',
        stylers: [{color: '#eed3d2'}]
    },
    {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{color: '#C9C9C9'}]//this
    },
    {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{color: '#bb4c49'}]//this
    },
    {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{color: '#1f2835'}]
    },
    {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{color: '#f3d19c'}]
    },
    {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{color: '#2f3948'}]
    },
    {
        featureType: 'transit.station',
        elementType: 'labels.text.fill',
        stylers: [{color: '#d59563'}]
    },
    {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{color: '#02b8e3'}]//water
    },
    {
        featureType: 'water',
        elementType: 'labels.text.fill',//water
        stylers: [{color: '#02b8e3'}]
    },
    {
        featureType: 'water',
        elementType: 'labels.text.stroke',
        stylers: [{color: '#17263c'}]
    }
]


const MyMapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAweofWPFAadH7psVjPjL44Vm7135bjgac&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div className={"container-map"} />,
        mapElement: <div className={"container-map-inner"} style={{ height: `100%` }} />
    }),
    withScriptjs,
    withGoogleMap
)((props) =>
    <div className="map-container">
        <div className="map-filter">
            <AccordionComponent
                resetMap={props.resetMap}
                taxonomies={props.taxonomies}
                openAccordion={props.openAccordion}
                markers={props.markers}
                showMarker={props.showMarker}
                showDirections={props.showDirections}
            />
        </div>
        <GoogleMap
            ref={props.onMapMounted}
            zoom={props.zoom}
            closeMarker={props.closeMarker}
            defaultCenter={new google.maps.LatLng(props.lat, props.lng)}
            options={{
                styles: props.styles,
                gestureHandling: "greedy",
                scrollwheel: false
            }}
        >

            <Marker position={{ lat: parseFloat(props.lat), lng: parseFloat(props.lng) }}
                    onClick={props.onMarkerClick}
                    icon={"/static/assets/AMBITIONpng.png"}
            />

            <MarkerClusterer
               // onClick={props.onMarkerClustererClick}
                averageCenter
                enableRetinaIcons
                gridSize={60}
                ignoreHidden={true}
                maxZoom={10}
                defaultMaxZoom={10}
            >

                <MyMarker
                    icon={"/static/assets/pin-text.png"}
                    defaultVisible={true}
                    markers={props.markers}
                    closeMarker={props.closeMarker}
                    showMarker={props.showMarker}
                    duration={props.duration}
                    distance={props.distance}
                    mainLat={props.lat}
                    mainLng={props.lng}
                    lang={props.lang}
                />

            </MarkerClusterer>

            <DirectionsRenderer directions={props.directions} options={{ suppressMarkers: true }}  />
        </GoogleMap>

    </div>
)

export default class AccordionMapComponent extends React.PureComponent {

    state = {
        isMarkerShown: true,
        isInfoBoxShown: false,
        zoomLevel: 12,
        lat: this.props.center.lat,
        lng: this.props.center.lng,
        show: false,
        markers: this.props.markers,
        taxonomies: this.props.taxonomies,
        map: null,
        newPin: null,
        directions: null,
        distance: null,
        duration: null,
        counter: 1
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            markers: nextProps.markers,
            taxonomies: nextProps.taxonomies,
        })
        console.log("eea")
    }

    componentWillMount() {

        let newTaxs = this.state.taxonomies.map(function(item){

            item.show = false;
            return item
        })

        let newMarkers = this.state.markers.map(function(item){

            item.show = false;
            item.showInfoBox = false;

            return item
        })

        this.setState(
            {
                markers: newMarkers,
                taxonomies: newTaxs,
                zoomLevel: 12,
                lat: this.props.center.lat,
                lng: this.props.center.lng
            }
        )

    }

    openAccordion = (Term) => {

        if( this.state.counter === 1){
            const DirectionsService = new google.maps.DirectionsService();
            const directionsDisplay = new google.maps.DirectionsRenderer();

            this.setState(
                {
                    directionsService: DirectionsService,
                    directionsDisplay: directionsDisplay,
                }
            )
        }

        let newTaxs = this.state.taxonomies.map(function(item){
            item.post_slug === Term ? item.show = true : item.show = false
            return item
        })

        let newMarkers = this.state.markers.map(function(item){
            if( item.taxonomies === Term){
                item.show = true;
                item.showInfoBox = false;
            }else{
                item.show = false;
                item.showInfoBox = false;
            }
            return item
        })



        this.setState(
            {
                markers: newMarkers,
                taxonomies: newTaxs,
                zoomLevel: 13,
                directions: {routes: []},
                counter: 3
            }
        )


        //Testing ZoomOut
        let self = this
        setTimeout(function(){
            self.setState({
                zoomLevel: 12
            });
        },200)

    }

    resetMap = () => {

        let newTaxs = this.state.taxonomies.map(function(item){
            item.show = false;
            return item
        })

        let newMarkers = this.state.markers.map(function(item){
            item.show = false;
            item.showInfoBox = false;
            return item
        })

        let myLatlng = new google.maps.LatLng(parseFloat(this.props.center.lat),parseFloat(this.props.center.lng));
        this.state.map.panTo(myLatlng)

        this.setState(
            {
                markers: newMarkers,
                taxonomies: newTaxs,
                zoomLevel: 12,
                directions: {routes: []}
            }
        )

        //Testing ZoomOut
        this.setState({
            zoomLevel: 13
        });
        let self = this
        setTimeout(function(){
            self.setState({
                zoomLevel: 12
            });
        },200)

    }

    showDirections = (id) =>{

        let newMark = {
            lat: 0,
            lng: 0
        }

        this.state.markers.map(function(item){
            if( id === item.post_id ){
                item.show = true
                item.showInfoBox = true
                newMark.lat = item.lat
                newMark.lng = item.lng
            }else{
                item.showInfoBox = false
            }
            return item
        })

        this.state.directionsService.route({
            origin: new google.maps.LatLng(parseFloat(this.state.lat),parseFloat(this.state.lng)),
            destination: new google.maps.LatLng(parseFloat(newMark.lat),parseFloat(newMark.lng)),
            travelMode: google.maps.TravelMode.DRIVING,
        }, (result, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.setState({
                    directions: result,
                    distance: result.routes[0].legs[0].distance.text,
                    duration: result.routes[0].legs[0].duration.text
                });
                this.state.directionsDisplay.setMap(null)
                this.state.directionsDisplay.setMap(this.state.map);
                this.state.directionsDisplay.setDirections(result)
            } else {
                console.error(`error fetching directions ${result}`);
            }
        });

    }

    showMarker = (id) =>{

        let newMark = {
            lat: 0,
            lng: 0
        }

        let newMarkers =  this.state.markers.map(function(item){
                if( id === item.post_id ){
                    item.show = true
                    item.showInfoBox = true
                    newMark.lat = item.lat
                    newMark.lng = item.lng
                }else{
                    item.showInfoBox = false
                }
                return item
           })


        let myLatlng = new google.maps.LatLng(parseFloat(newMark.lat),parseFloat(newMark.lng));

        this.state.map.panTo(myLatlng)

        this.setState({
            markers: newMarkers,
            zoomLevel: 12,
            directions: {routes: []}
        })

    }

    closeMarker = (id) => {

        let closeMarker =  this.state.markers.map(function(item){
            if( id === item.post_id ){
                item.showInfoBox = false
            }
            return item
        })


        this.setState({
            markers: closeMarker
        })
    }


    onMapMounted = (ref)  => {

        this.setState({
            map: ref
        })

    }

    render() {

        return (
            <div className="row">

                <MyMapComponent
                    zoom={this.state.zoomLevel}
                    isMarkerShown={this.state.isMarkerShown}
                    closeMarker={this.closeMarker}
                    lat={this.state.lat}
                    lng={this.state.lng}
                    markers={this.state.markers}
                    changeZoom={this.changeZoom}
                    styles={exampleMapStyles}
                    resetMap={this.resetMap}
                    taxonomies={this.state.taxonomies}
                    openAccordion={this.openAccordion}
                    showMarker={this.showMarker}
                    onMapMounted={this.onMapMounted}
                    showDirections={this.showDirections}
                    directions={this.state.directions}
                    distance={this.state.distance}
                    duration={this.state.duration}
                    lang={this.props.lang}
                />

                <style jsx>{`

                  .map-filter{
                     background: #656565;
                  }

                `}</style>

            </div>
        )
    }
}





