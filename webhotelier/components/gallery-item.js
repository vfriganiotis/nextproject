import React, {Component} from 'react'

export default class GalleryItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {url: this.props.url};
    }

    render() {
        return (
            <img src={this.state.url} />
        );
    }
}
