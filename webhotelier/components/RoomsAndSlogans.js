import React, {Component} from 'react'
import Link from 'next/link'

export default class RoomsAndSlogans extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rooms : this.props.rooms,
            layouts : this.props.rooms_and_layouts,
            language: this.props.language
        }
    }

    componentWillReceiveProps(nextProps){

        this.setState(prevState => ({
            rooms : nextProps.rooms,
            layouts : nextProps.rooms_and_layouts,
            language: nextProps.language
        }));

    }

    rooms(){

        let Language = this.props.language

        const moreRooms = this.state.rooms.map(function(item,i){

            let codes = item.room_name.split(' ').join('-').toLowerCase();
            let pathname = '/room/' + codes;
            let newUrl = '/room?roomslug=' + codes + '&lng=en';


            if( Language !== undefined){

                switch(Language) {
                    case 'el':

                        pathname = '/el/room/' + codes;
                        newUrl = '/room?roomslug=' + codes + '&lng=el';

                        break;
                    default:

                    // code block
                }

            }

            let styles = {
                background: `url(${item.featured_image})`,
            };

            return (<div key={i} className="col-md-6">
                <Link href={newUrl} as={pathname} >
                    <div className="room_single_acc">

                        <div className="sliderHome _small_height" style={styles}>

                        </div>

                        <div className="pos-abs">
                            <div className="cnt">{item.room_name}</div>
                            <div className="btn-c withArrowSm arrowRight ">READ MORE</div>
                        </div>

                        <div className="opac_black">

                        </div>

                    </div>
                </Link>
            </div>)

        })

        return moreRooms;
    }

    slogan(){

        const slogans = this.state.layouts.map(function(item,i){

            let styles = {
                background: `${item.background_color}`,
            };
            let color = {
                color: `${item.color}`,
            };

            return (<div key={i} className="col-md-6">
                <div className="room_single_acc">

                    <div className="sliderHome _small_height" style={styles}>

                    </div>

                    <div className="pos-abs">
                        <div className="cnt" style={ color }>{item.text}</div>
                    </div>

                    <div className="opac_black">

                    </div>

                </div>
            </div>)

        })

        return slogans;
    }

    render() {
        return (
            <div className="row more_rooms">
                {this.rooms()}
                {this.slogan()}
            </div>
        );
    }
}