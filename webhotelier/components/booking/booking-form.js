import DatePicker from "react-datepicker";
import differenceInCalendarDays from 'date-fns/difference_in_calendar_days';

let text = 'Guest';
let infText, infants, total = 1;

export default class Form extends React.Component {

    state = {
        checkIn: null,
        checkOut: null,
        startDate: null,
        adults: 1,
        children: 0,
        infants: 0,
        highlight: false,
    }

    handleCheckIn = (date) => {

        this.setState({
            checkIn: date,
            checkOut: date,
            startDate: date,
        });
        if (!this.state.highlight) {
            this.setState({
                highlightWithRanges: false
            })
        }

        this.refs.calendar.setOpen(true)

    }

    handleCheckOut = (date) => {

        const start = new Date(this.state.checkIn)
        const nextDays = new Date(date)
        const end = new Date(date)
        const diffDays = differenceInCalendarDays(end, start)
        const daysHighlighted = new Array(diffDays).fill(start);

        this.setState({
            checkOut: date,
            endDate: nextDays.setDate(date.getDate() + 3),
            highlightWithRanges: [{
                "react-datepicker__day--highlighted-class": daysHighlighted.map((day, index) => {
                    if (index === 0) {
                        day.setDate(day.getDate() + index)
                        return new Date(day)
                    }
                    day.setDate(day.getDate() + 1)
                    return new Date(day)
                })
            }],
            highlight: true
        });
    }

    handleHighlight = () => {
        const start = new Date(this.state.checkIn)
        const end = new Date(this.state.checkOut)
        const diffDays = differenceInCalendarDays(end, start)
        let daysHighlighted = new Array(diffDays).fill(start);

        this.setState({
            highlightWithRanges: [{
                "react-datepicker__day--highlighted-class": daysHighlighted.map((day) => {
                    day.setDate(day.getDate() + 1)
                    return new Date(day)
                })
            }],
            highlight: false
        })
    }

    handleSelect = () => {
        this.refs.options.classList.toggle('active')
        document.querySelector('.select').classList.toggle('active')
    }

    handleStepUp = (e) => {
        e.preventDefault()
        e.target.parentElement.children[2].disabled = false
        e.target.parentElement.children[2].classList.remove('disable')
        const name = e.target.parentElement.children[1].getAttribute('name')
        let value = parseInt(e.target.parentElement.children[1].value)
        value = value + 1
        this.setState({[name]: value})
        if (value === 0 || value >= 4) {
            e.target.disabled = true
            e.target.classList.add('disable')
        }
        if (value >= 2 || (name === 'infants' && value >= 1)) {
            text = 'Guests'
            infText = 'infants'
        }

    }
    handleStepDown = (e) => {
        e.preventDefault()
        e.target.parentElement.children[0].disabled = false
        e.target.parentElement.children[0].classList.remove('disable')
        const name = e.target.parentElement.children[1].getAttribute('name')
        let value = parseInt(e.target.parentElement.children[1].value)
        if (value <= 0 || (name === 'adults' && value === 1)) {
            e.target.disabled = true
            e.target.classList.add('disable')
            return
        }
        value = value - 1
        this.setState({[name]: value})
    }

    reset=()=>{
        this.setState({
            checkIn:null,
            checkOut:null,
            highlightWithRanges:false
        })
    }

    componentWillUpdate(nextProps, nextState, nextContent) {
        const plus = document.querySelectorAll('.jsDisable')
        total = parseInt(nextState.adults) + parseInt(nextState.children)
        infants = nextState.infants
        if (total >= 4) {
            plus.forEach(item => {
                item.disabled = true
                item.classList.add('disable')

            })
        } else {
            plus.forEach(item => {
                item.disabled = false
                item.classList.remove('disable')
            })
            if (total <= 1) {
                text = 'Guest'
            }
        }
        if (infants > 1) {
            infText = 'infants'
        } else if (infants === 1) {
            infText = 'infant'
        } else {
            infants = ''
            infText = ''
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        document.querySelector('.react-datepicker__input-container input').addEventListener('click', this.handleHighlight)
    }

    render() {
        return (
            <form className={'row booking-formv1'}
                  action={this.props.bookingUrl}
                  method={'post'}>
                <div className="col-sm-12 col-md-3 small-pad2">
                    <div className="cosmores-txt">Check Availability:</div>
                </div>

                <div className={"col-sm-12 col-md-4 small-pad has-arrow "}>
                    <div className="header-book">Dates:</div>
                    <div className="datepicker_row">
                        <div className="datepick">
                            <DatePicker
                                className={""}
                                placeholderText={'Check-In'}
                                dateFormat="dd-MM-yyyy"
                                selected={this.state.checkIn}
                                onChange={(date) => this.handleCheckIn(date)}
                                name={"checkin"}
                                highlightDates={this.state.highlightWithRanges}>
                            </DatePicker>
                        </div>

                        <div className={'arrow'}/>

                        <div className="datepick date_checkOut">
                            <DatePicker
                                selectsStart
                                selectsEnd
                                placeholderText={'Check-Out'}
                                selected={(this.state.checkOut) ? this.state.checkOut : this.state.checkIn}
                                startDate={(this.state.checkOut) ? this.state.checkOut : this.state.startDate}
                                minDate={(this.state.checkOut) ? this.state.checkOut : this.state.startDate}
                                highlightDates={this.state.highlightWithRanges}
                                dateFormat="dd-MM-yyyy"
                                onChange={(date) => this.handleCheckOut(date)}
                                ref={'calendar'}
                                name={"checkout"}
                            >
                            </DatePicker>
                        </div>
                    </div>
                </div>

                <div className={"col-sm-12  col-md-4 no-arrow book-kids-adults"}>
                    <div className="header-book">Guests:</div>
                    <div className={'d-flex flex-column'}>
                        <div className={'select d-flex align-items-center'} onClick={() => this.handleSelect()}>
                            <span> {total} {text} {infants} {infText}</span>
                        </div>
                        <div className={'options'} ref={'options'}>
                            <div className={'d-flex flex-column'}>
                                <label htmlFor={'adults '} className={'justify-content-between align-items-center'}>
                                    <span>Adults</span>
                                    <div className={'d-flex justify-content-end'}>
                                        <button className={'plus jsDisable'} onClick={(e) => this.handleStepUp(e)}/>
                                        <input type="text" name={'adults'} readOnly value={this.state.adults}/>
                                        <button className={'minus'} onClick={(e) => this.handleStepDown(e)}/>
                                    </div>
                                </label>
                                <label htmlFor={'children'} className={'justify-content-between'}>
                                    <div className={'d-flex flex-column'}>
                                        <span>Kids</span>
                                    </div>
                                    <div className={'d-flex justify-content-end'}>
                                        <button className={'plus jsDisable'} onClick={(e) => this.handleStepUp(e)}/>
                                        <input type="text" name={'children'} readOnly value={this.state.children}/>
                                        <button className={'minus'} onClick={(e) => this.handleStepDown(e)}/>
                                    </div>
                                </label>
                                <label htmlFor={'infants'} className={'justify-content-between'}>
                                    <div className={'d-flex flex-column'}>
                                        <span>Infants</span>
                                    </div>
                                    <div className={'d-flex justify-content-end'}>
                                        <button className={'plus'} onClick={(e) => this.handleStepUp(e)}/>
                                        <input type="text" readOnly name={'infants'} value={this.state.infants}/>
                                        <button className={'minus'} onClick={(e) => this.handleStepDown(e)}/>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div className={'col-sm-12  col-md-1 small-pad'}>
                    <input type="submit" className={'submit'} placeholder={'Search'}/>
                </div>
            </form>
        )
    }


}