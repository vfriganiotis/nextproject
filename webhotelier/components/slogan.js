import React, {Component} from 'react'


export default class slogan extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
           layouts : this.props.rooms_and_layouts
        }
    }

    slogan(){

        const slogans = this.state.layouts.map(function(item,i){

            let styles = {
                background: `${item.background_color}`,
            };
            let color = {
                color: `${item.color}`,
            };

            return (<div key={i} className="col-md-6">
                <div className="room_single_acc">

                    <div className="sliderHome _small_height" style={styles}>

                    </div>

                    <div className="pos-abs">
                        <div className="cnt" style={ color }>{item.text}</div>
                    </div>

                    <div className="opac_black">

                    </div>

                </div>
            </div>)

        })

        return slogans;
    }

    render() {
        return (
            <div className="row more_rooms">

                {this.slogan()}

            </div>
        );
    }
}