import React, {Component} from 'react'
import Link from 'next/link'
import "react-datepicker/dist/react-datepicker.css";

export default class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showMenu: true,
            menus: this.props.menus ,
            mobileMenu: this.props.mobileMenu,
            mobileLogo: this.props.mobileLogo,
            logo: this.props.logo ,
            rooms : this.props.rooms ,
            book: this.props.book,
            photos: this.props.photos ,
            langBooking: this.props.langBooking,
            lang: "",
            MoreLang:"",
            defaultLang: "",
            showLangsAll: "noLangs"
        };
        this.click = this.click.bind(this);
        this.hoverOn = this.hoverOn.bind(this);
        this.hoverOut = this.hoverOut.bind(this);
    }

    click(){

        this.setState(prevState => ({
            showMenu: !prevState.showMenu
        }));

    }

    hoverOn(){
        this.setState(prevState => ({
            showLangsAll: "showAllLangs"
        }));
    }

    hoverOut(){
        this.setState(prevState => ({
            showLangsAll: "noLangs"
        }));
    }

    componentDidMount(){

        let lann = document.URL.split("/")

        let newPathname = ''
        let englishLanguage;

        for (let i = 0; i < document.URL.split("/").length; i++) {
            if( i !== 0){
                newPathname += "/";
            }
            newPathname += document.URL.split("/")[i];
        }

        englishLanguage = newPathname.replace(  "/" + document.URL.split("/")[3], "");

        switch(document.URL.split("/")[3]) {

            case 'el':

                newPathname = ''

                for (let i = 0; i < document.URL.split("/").length; i++) {
                    if( i !== 0){
                        newPathname += "/";
                    }

                    newPathname += document.URL.split("/")[i];
                }

                this.setState(prevState => ({
                    MoreLang: newPathname,
                    defaultLang : englishLanguage,
                    lang: lann[3],
                    ok: newPathname,
                    LogoLang: 'el'
                }));
                break;
            default:

                newPathname = ''

                for (let i = 0; i < document.URL.split("/").length; i++) {
                    if( i !== 0){
                        newPathname += "/";
                    }

                    if( i === 3 ){
                        newPathname += "el/";
                    }

                    newPathname += document.URL.split("/")[i];
                }

                this.setState(prevState => ({
                    MoreLang: newPathname,
                    defaultLang : document.URL,
                    lang: "en",
                    LogoLang: ''
                }));

        }

    }

    componentWillReceiveProps(nextProps){

        this.setState(prevState => ({
            menus: nextProps.menus,
            mobileMenu: nextProps.mobileMenu
        }));

        let lann = document.URL.split("/")

        let newPathname = ''
        let englishLanguage;

        for (let i = 0; i < document.URL.split("/").length; i++) {
            if( i !== 0){
                newPathname += "/";
            }
            newPathname += document.URL.split("/")[i];
        }

        englishLanguage = newPathname.replace(  "/" + document.URL.split("/")[3], "");

        console.log(document.URL.split("/")[3])
        switch(document.URL.split("/")[3]) {

            case 'el':

                newPathname = ''

                for (let i = 0; i < document.URL.split("/").length; i++) {
                    if( i !== 0){
                        newPathname += "/";
                    }

                    newPathname += document.URL.split("/")[i];
                }

                this.setState(prevState => ({
                    MoreLang: newPathname,
                    defaultLang : englishLanguage,
                    lang: lann[3],
                    home: 'en',
                    LogoLang: 'el',
                    langBooking: 'ΚΡΑΤΗΣΗ'
                }));
                break;
            default:

                newPathname = ''

                for (let i = 0; i < document.URL.split("/").length; i++) {
                    if( i !== 0){
                        newPathname += "/";
                    }

                    if( i === 3 ){
                        newPathname += "el/";
                    }

                    newPathname += document.URL.split("/")[i];
                }

                this.setState(prevState => ({
                    MoreLang: newPathname,
                    defaultLang : document.URL,
                    lang: "en",
                    home: 'el',
                    LogoLang: '',
                    langBooking: 'BOOK NOW'
                }));

        }

    }


    render() {

        return (<div className="head_container">

            <header className={ this.state.showMenu === true ? 'mobile-menu' : 'mobile-menu showMenu' } >

                <nav>
                    {this.state.mobileMenu}
                </nav>

                <div>
                    <div className="mobile-switcher">
                        <Link  href={ this.state.defaultLang + "/en"} as={ this.state.defaultLang}><a><div><img src={"/static/assets/gb.png"} /></div> </a></Link>
                        <Link  href={ this.state.defaultLang + "/el"} as={ this.state.MoreLang }><a><div><img src={"/static/assets/gr.png"} /></div> </a></Link>
                    </div>
                </div>

            </header>

            <header className={ this.state.showMenu === true ? 'header-v1 desktop-menu' : 'header-v1 desktop-menu showMenu' } >

                <Link href={this.props.book} as={this.props.book}><a className="btn btn-primary book_now">{this.state.langBooking}</a></Link>

                <nav>
                    {this.state.menus}
                </nav>

                <div onMouseEnter={this.hoverOn} onMouseLeave={this.hoverOut} className={this.state.lang + '  lang-switcher ' + this.state.showLangsAll}>
                    <div className="lang-container">
                        <Link  href={ this.state.defaultLang + "?lng=en"} as={ this.state.defaultLang}><a className={this.state.langs + ' langs en-lang'}><div>EN <img src={"/static/assets/gb.png"} /></div></a></Link>
                        <Link  href={ this.state.defaultLang + "?lng=el"} as={ this.state.MoreLang }><a className={this.state.langs + ' langs el-lang'}><div>EL <img src={"/static/assets/gr.png"} /></div> </a></Link>
                    </div>
                </div>

                <Link href={"/index?lng=" + this.state.lang} as={"/" + this.state.LogoLang }><a><img src={this.state.logo} /></a></Link>

            </header>

            <div className="mobile_container">
                <Link href={"/index?lng=" + this.state.lang} as={"/" + this.state.LogoLang }><a><img   className="logo_hotel mobile-menu" src={this.state.logo} /></a></Link>

                <a href={ this.state.book } className={"book_hotel mobile-menu"}> {this.state.langBooking} </a>

                <div className={this.state.showMenu === true ? 'mobile-menu hamburger hamburger--slider js-hamburger' : 'mobile-menu hamburger hamburger--slider js-hamburger is-active ' }  onClick= { this.click } >
                    <div className="hamburger-box">
                        <div className="hamburger-inner"></div>
                    </div>
                </div>
            </div>

        </div>
        );
    }
}
