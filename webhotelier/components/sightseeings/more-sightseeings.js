import React, {Component} from 'react'
import Slider from "react-slick"
import LinesEllipsis from 'react-lines-ellipsis'
import Link from 'next/link'

export default class SightsSlider extends React.Component {

    constructor(props) {
        super(props);

        this.prev = this.prev.bind(this);
        this.next = this.next.bind(this);
        this.state = {
            slides: this.props.slides,
            settings: this.props.settings,
            slugTerm: this.props.term ,
            lang: this.props.lang
        };

    }

    componentWillReceiveProps(nextProps){

        this.HeroSlider.slickGoTo(0)
        this.setState({
            slugTerm: nextProps.term,
            lang: nextProps.lang,
            slides: nextProps.slides
        })

    }

    componentWillMount(){

        if (!this.state.settings) {
            this.setState({settings: {
                dots: true,
                infinite: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 4000,
                speed: 500,
                fade: false,
                accessibility:false,
                touchMove:false,
                slidesToShow: 3,
                slidesToScroll: 1,
                useTransform: false
            }})
        }

    }

    componentDidMount(){

        if ( window.innerWidth < 768 ) {
            this.setState({settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }})
        }

    }

    next(){
        this.HeroSlider.slickNext();
    }

    prev(){
        this.HeroSlider.slickPrev();
    }

    render() {

        let slugTerm = this.state.slugTerm
        let lang = this.state.lang

        return (
            <div className="more-sights">

                <div className="more-sights-inner">
                    <Slider  ref={c => (this.HeroSlider = c)} {...this.state.settings}>
                        {this.props.slides.map(function(item,i){

                            if( slugTerm === item.taxonomies){
                                let styles = {
                                    background: `url(${item.image})`,
                                    height: '350px',
                                    backgroundSize: 'cover',
                                    backgroundPosition: "center"
                                };

                                let pathname = "/location/" +  item.taxonomies  + "/" + item.post_slug

                                let itemHref = "/template-map" + "?sight=" + item.taxonomies + "&slug=" + item.post_slug

                                let LINK = <Link href={ itemHref } as={ pathname } >
                                    <a>READ MORE</a>
                                </Link>


                                if( lang !== undefined){
                                    switch(lang) {
                                        case 'el':
                                            pathname = "/el/location/" +  item.taxonomies  + "/" + item.post_slug
                                            itemHref = "/template-map" + "?sight=" + item.taxonomies + "&slug=" + item.post_slug + "&lang=" + lang

                                            LINK = <Link href={itemHref } as={ pathname } >
                                                <a>{item.post_name}</a>
                                            </Link>
                                            break;
                                        default:
                                    }
                                }

                                let regex = /(<([^>]+)>)/ig;
                                let result = item.content.replace(regex, '');

                                return (<div key={i}>
                                    <div className="sight-slider">
                                        <div className="sight-slider-inner">
                                            <div className="sight-slider-image" style={styles}>

                                            </div>
                                            <div className="sight-content">
                                                <LinesEllipsis
                                                    className="text"
                                                    text= {result}
                                                    maxLine={5}
                                                    ellipsis='...'
                                                    trimRight
                                                    basedOn='letters'
                                                />
                                            </div>
                                            <div className="sight-read">
                                                {LINK}
                                            </div>
                                        </div>
                                    </div>
                                </div>)
                            }


                        })}
                    </Slider>
                </div>

                <div style={{ textAlign: "center" }}>

                    <button className="arrow_btn prev button" onClick={this.prev}>
                        <img src={"/static/assets/sma-arrow-left.png"} />
                    </button>

                    <button className="arrow_btn next button" onClick={this.next}>
                        <img src={"/static/assets/sma-arrow-right.png"} />
                    </button>

                </div>

                <style jsx>{`

                    .more-sights{
                       position: relative;
                    }

                    .sight-slider-inner{
                       margin: 20px;
                       box-shadow: 0px 0px 4px #000;
                    }

                    .sight-content{
                       padding: 20px;
                    }

                    .more-sights-inner{
                        padding-bottom: 50px;
                    }

                    .more-sights-inner .slick-slider{
                         padding-bottom: 50px;
                    }

                    .sight-read{
                       padding: 20px;
                    }

                  `}</style>


            </div>
        );
    }
}
