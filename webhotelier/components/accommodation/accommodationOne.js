import React, {Component} from 'react'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'

export default class RoomItems extends React.Component {

    constructor(props) {
        super(props);
    }

    __renderRooms(){

        let newRoomLang = this.props.lang
        const name = this.props.rooms.map(function(item,i){


            let codes = item.room_name.split(' ').join('-').toLowerCase();
            let pathname = '/room/' + codes
            let newUrl = '/room?roomslug=' + codes + "&lng=en";
            let room = <div key={i}>
                <Link href={newUrl} as={pathname}>
                    <a className="details btn">DETAILS</a>
                </Link>
            </div>;

            if( newRoomLang !== undefined){
                switch(newRoomLang) {
                    case 'el':
                        codes = item.room_name.split(' ').join('-').toLowerCase();
                        pathname = '/el/room/' + codes
                        newUrl = '/room?roomslug=' + codes + "&lng=el"
                        room = <div key={i}>
                            <Link href={newUrl} as={pathname}>
                                <a className="details btn">ΛΕΠΤΟΜΕΡΙΕΣ</a>
                            </Link>
                        </div>;
                        break;
                    default:
                }
            }


            const regex = /(<([^>]+)>)/ig;
            const result = item.overview.replace(regex, '')

            if( i % 2 == 0){

                return (<li key={i} className='_animation pull-center'>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="room_inner">
                                <h1 className="title_head">{item.room_name}</h1>
                                <LinesEllipsis
                                    className="text"
                                    text= {result}
                                    maxLine={"5"}
                                    ellipsis='...'
                                    trimRight
                                    basedOn='letters'
                                />
                                { room }
                            </div>
                        </div>
                        <div className="col-md-8">
                            <img src={item.featured_image} />
                        </div>
                    </div>
                </li>)
            }else{
                return (<li key={i} className='_animation pull-center'>
                    <div className="row">
                        <div className="col-md-8">
                            <img src={item.featured_image} />
                        </div>
                        <div className="col-md-4">
                            <div className="room_inner">
                                <h1 className="title_head">{item.room_name}</h1>
                                <LinesEllipsis
                                    className="text"
                                    text= {result}
                                    maxLine={"5"}
                                    ellipsis='...'
                                    trimRight
                                    basedOn='letters'
                                />
                                { room }
                            </div>
                        </div>
                    </div>
                </li>)
            }

        })

        return(
            <ul>
                {name}
            </ul>
        )

    }

    render() {
        let rooms = this.__renderRooms()
        return (
            <div>
                {rooms}
            </div>
        );

    }

}
