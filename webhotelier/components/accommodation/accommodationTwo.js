import React, {Component} from 'react'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'

export default class RoomItemsTwo extends React.Component {

    constructor(props) {
        super(props);
    }

    __renderRooms(){

        let newRoomLang = this.props.lang
        const name = this.props.rooms.map(function(item,i){


            let codes = item.room_name.split(' ').join('-').toLowerCase();
            let pathname = '/room/' + codes
            let newUrl = '/room?roomslug=' + codes + "&lng=en";
            let room = <li key={i}>
                <Link href={newUrl} as={pathname}>
                    <a className="details btn">DETAILS</a>
                </Link>
            </li>;

            if( newRoomLang !== undefined){
                switch(newRoomLang) {
                    case 'el':
                        codes = item.room_name.split(' ').join('-').toLowerCase();
                        pathname = '/el/room/' + codes
                        newUrl = '/room?roomslug=' + codes + "&lng=el"
                        room = <li key={i}>
                            <Link href={newUrl} as={pathname}>
                                <a className="details btn">ΛΕΠΤΟΜΕΡΙΕΣ</a>
                            </Link>
                        </li>;
                        break;
                    default:
                }
            }

            const regex = /(<([^>]+)>)/ig;
            const result = item.overview.replace(regex, '')

            const Excerpt = () => (
                <ul dangerouslySetInnerHTML={{ __html: item.excerpt }} />
            );

            let styles = {
                background: `url(${item.featured_image})`,
                backgroundSize: 'cover',
                backgroundPosition : 'center',
                minHeight: '400px'
            }

            return (<li key={i}>
                    <div className="room-abs">
                        <div className="pos-rel" style={styles}>
                            <div className="capacity_overlay primary_text">

                                {Excerpt()}
                                <ul>
                                 {room}
                                </ul>
                            </div>
                        </div>
                        <div className="room_name_">
                            <div className="room_name_item">{item.room_name}</div>
                            <span> Up to {item.up_to} Persons</span>
                        </div>
                    </div>
            </li>)


        })

        return(
            <ul className="text-center">
                {name}
            </ul>
        )

    }

    render() {
        let rooms = this.__renderRooms()
        return (
            <div className="acc-rooms-v1 col-md-12">
                {rooms}
            </div>
        );

    }

}
