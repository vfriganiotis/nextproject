import React, {Component} from 'react'

export default class Footer extends React.Component {

    render() {

        return (<footer className="footer-v1">
                <div className="footer_top container">
                    <div className="row pull-center text-upper">

                        <div className="col-md-12"><img src={this.props.logourl} /></div>
                        <div className="col-md-6 cd">
                            <h1>CONTACT INFORMATION</h1>
                            <h1>{this.props.name}</h1>
                            <ul>
                                <li>{this.props.skype}</li>
                                <li>{this.props.tel}</li>
                                <li>{this.props.email}</li>
                                <li>{this.props.bookurl}</li>
                            </ul>
                        </div>
                        <div className="col-md-6 cd">
                            <h1>STAY CONNECTED</h1>
                            <ul>
                                <li>{this.props.facebook}</li>
                                <li>{this.props.twitter}</li>
                                <li>{this.props.instagram}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                {/*<div className="copyright text-center">*/}
                    {/*<h1>Design and Development By <a href="http://www.marinet.gr/hotelmarketing/" target="_blank">Marinet Ltd</a></h1>*/}
                {/*</div>*/}
            </footer>
        );
    }
}
