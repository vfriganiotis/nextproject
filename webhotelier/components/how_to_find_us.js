import React,{Component} from 'react'
import Link from 'next/link'
import GoogleMapReact from 'google-map-react';
import LinesEllipsis from 'react-lines-ellipsis'

const exampleMapStyles = [

    {elementType: 'geometry', stylers: [{color: '#f2f2f2'}]},//first
    {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{color: '#000000'}]
    },
    {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{color: '#000000'}]
    },
    {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{color: '#ffffff'}]
    },
    {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{color: '#ffffff'}]
    },
    {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{color: '#C9C9C9'}]//this
    },
    {
        featureType: 'road',
        elementType: 'geometry.stroke',
        stylers: [{color: '#eed3d2'}]
    },
    {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{color: '#C9C9C9'}]//this
    },
    {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{color: '#bb4c49'}]//this
    },
    {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{color: '#1f2835'}]
    },
    {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{color: '#f3d19c'}]
    },
    {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{color: '#2f3948'}]
    },
    {
        featureType: 'transit.station',
        elementType: 'labels.text.fill',
        stylers: [{color: '#d59563'}]
    },
    {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{color: '#02b8e3'}]//water
    },
    {
        featureType: 'water',
        elementType: 'labels.text.fill',//water
        stylers: [{color: '#02b8e3'}]
    },
    {
        featureType: 'water',
        elementType: 'labels.text.stroke',
        stylers: [{color: '#17263c'}]
    }
]

const mapOptions = {
    styles: exampleMapStyles,
    gestureHandling: 'greedy',
    scrollwheel: false
};

export default class HowToFindUs extends React.Component {

    constructor(props) {
        super(props);

        let regex = /(<([^>]+)>)/ig;
        let result = this.props.text.replace(regex, '');

        let pathname = '/' + this.props.link;
        let newUrl = '/' + this.props.link + '?lng=en';

        if( this.props.language !== undefined){

            switch(this.props.language) {
                case 'el':

                    pathname = '/el/' + this.props.link;
                    newUrl = '/' + this.props.link + '?lng=el';

                    break;
                default:

            }

        }


        this.state = {
            title: this.props.title,
            text: result,
            button: this.props.button,
            pathname: pathname,
            link: newUrl,
            lat: parseFloat(this.props.lat),
            lon: parseFloat(this.props.lon) ,
            icon: this.props.icon,

        };
    }


    componentWillReceiveProps(nextProps){

        let pathname = '/' + nextProps.link;
        let newUrl = '/' + nextProps.link + '?lng=en';

        let regex = /(<([^>]+)>)/ig;
        let result = nextProps.text.replace(regex, '');


        if( nextProps.language !== undefined){

            switch(nextProps.language) {
                case 'el':

                    this.setState(prevState => ({
                        title: nextProps.title,
                        link:  '/' + nextProps.link + '?lng=el',
                        pathname: '/el/' + nextProps.link,
                        text: result,
                    }));

                    break;
                default:

                    this.setState(prevState => ({
                        title: nextProps.title,
                        link:  newUrl,
                        pathname: pathname,
                        text: result,
                    }));

            }

        }

    }


    renderMarkers(map, maps) {

        let marker = new maps.Marker({
            position: {
                lat: this.state.lat,
                lng: this.state.lon
            },
            icon:"../static/assets/AMBITIONpng.png",
            map
        });

    }

    render() {

        const mapCenterOpt = {
            center: {
                lat: this.state.lat,
                lng: this.state.lon
            },
            zoom: 12,
            // icon: this.state.data.logourl
        };

        return (
            <div className="row">
                <div className="col-md-6">
                    <div className="fm_map">

                        <h3 className="title">{this.state.title}</h3>

                        <LinesEllipsis
                            className="text"
                            text= {this.state.text}
                            maxLine={5}
                            ellipsis='...'
                            trimRight
                            basedOn='letters'
                        />

                        <Link href={this.state.link} as={this.state.pathname}>
                            <a className="btns">{this.state.button}</a>
                        </Link>
                    </div>
                </div>
                <div className="col-md-6">
                    <div style={{ height: '466px', width: '100%' }}  className='_animation'>
                        <GoogleMapReact
                            options={mapOptions}
                            bootstrapURLKeys={{ key: 'AIzaSyAweofWPFAadH7psVjPjL44Vm7135bjgac' }}
                            // defaultOptions={{
                            //     styles: {exampleMapStyles}
                            // }}

                            defaultCenter={mapCenterOpt.center}
                            defaultZoom={mapCenterOpt.zoom}
                            defaultScrollwheel={mapCenterOpt.scrollwheel}
                            defaultIcon={mapCenterOpt.icon}
                            gestureHandling={'greedy'}
                            onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}
                        >
                        </GoogleMapReact>
                    </div>
                </div>
            </div>
        );
    }
}