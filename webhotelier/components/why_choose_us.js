import React,{Component} from 'react'
import Link from 'next/link'

export default class WhyChooseUs extends React.Component {

    constructor(props) {
        super(props);

        let pathname = '/' + this.props.chooseOneLink;
        let newUrl = '/' + this.props.chooseOneLink + '?lng=en';
        let pathnameTwo = '/' + this.props.chooseTwoLink;
        let newUrlTwo = '/' + this.props.chooseTwoLink + '?lng=en';

        if( this.props.language !== undefined){

            switch(this.props.language) {
                case 'el':

                    pathname = '/el/' + this.props.chooseOneLink;
                    newUrl = '/' + this.props.chooseOneLink + '?lng=el';
                    pathnameTwo = '/el/' + this.props.chooseTwoLink;
                    newUrlTwo = '/' +this.props.chooseTwoLink + '?lng=el';

                    break;
                default:

            }

        }

        this.state = {
            title: this.props.title,
            chooseOneLink: newUrl,
            chooseOnePathname: pathname,
            chooseTwoLink: newUrlTwo,
            chooseTwoPathname: pathnameTwo,
            chooseOne: this.props.chooseOne,
            chooseTwo: this.props.chooseTwo,
            language: this.props.language
        };

    }

    componentWillReceiveProps(nextProps){

        let pathname = '/' + nextProps.chooseOneLink;
        let newUrl = '/' + nextProps.chooseOneLink + '?lng=en';
        let pathnameTwo = '/' + nextProps.chooseTwoLink;
        let newUrlTwo = '/' + nextProps.chooseTwoLink + '?lng=en';

        if( nextProps.language !== undefined){

            switch(nextProps.language) {
                case 'el':

                    this.setState(prevState => ({
                        title: nextProps.title,
                        chooseOneLink:  '/' + nextProps.chooseOneLink + '?lng=el',
                        chooseOnePathname: '/el/' + nextProps.chooseOneLink,
                        chooseTwoLink: '/' + nextProps.chooseTwoLink + '?lng=el',
                        chooseTwoPathname: '/el/' + nextProps.chooseTwoLink,
                        chooseOne: nextProps.chooseOne,
                        chooseTwo: nextProps.chooseTwo,
                        language: nextProps.language
                    }));

                    break;
                default:

                    this.setState(prevState => ({
                        title: nextProps.title,
                        chooseOneLink: newUrl,
                        chooseOnePathname: pathname,
                        chooseTwoLink: newUrlTwo,
                        chooseTwoPathname: pathnameTwo,
                        chooseOne: nextProps.chooseOne,
                        chooseTwo: nextProps.chooseTwo,
                        language: nextProps.language
                    }));

            }

        }

    }


    render() {
        return (
            <div className="main-slogan-banner pad-cont">

                <h2 className="mtitle">{this.state.title}</h2>

                <div className="stitle">

                    <Link href={this.state.chooseOneLink} as={this.state.chooseOnePathname}>
                        <a>{this.state.chooseOne}</a>
                    </Link>
                    <span className="s_divider">|</span>
                    <Link href={this.state.chooseTwoLink} as={this.state.chooseTwoPathname}>
                        <a>{this.state.chooseTwo}</a>
                    </Link>

                </div>

            </div>
        );
    }
}