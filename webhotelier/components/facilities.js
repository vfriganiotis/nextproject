import React, {Component} from 'react'
import FacilitiesItem from '../components/facilities-item'

export default class Facilities extends React.Component {

    constructor(props) {
        super(props);
        this.state = {facilities: this.props.facilities};
        this.facilities = this.facilities.bind(this);
    }

    facilities() {

        if( this.props.facilities ){
            let FacilitiesItems = this.props.facilities.map(function(item,i){

                return <FacilitiesItem key={i} fac={item} ></FacilitiesItem>

            })

            return FacilitiesItems
        }

    }

    render() {

        return (
            <ul className={"facilities"}>
                {this.facilities()}
            </ul>
        );

    }
}
