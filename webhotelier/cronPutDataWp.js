const fs =require('fs');
let request = require('request');

//WP
let WPusername = "bill",
    WPpassword = "123",
    WPurl = "https://webhotelier.sitesdemo.com/api/wp-json/wp/v2/rooms",
    WPpages = "https://webhotelier.sitesdemo.com/api/wp-json/wp/v2/pages/",
    WPurlGallery = "https://webhotelier.sitesdemo.com/api/wp-json/wp/v2/galleries",
    WPFieldsGallery = "https://webhotelier.sitesdemo.com/api/wp-json/acf/v3/galleries/",
    WPFieldsUrl = "https://webhotelier.sitesdemo.com/api/wp-json/acf/v3/rooms/",
    WPImages = "https://webhotelier.sitesdemo.com/api/wp-json/wp/v2/media/",
    WPauth = "Basic " + new Buffer(WPusername + ":" + WPpassword).toString("base64");

const DataPagesRaw = require('./static/data');
const DataPages = JSON.parse(DataPagesRaw);

let HotelFacilities = []

DataPages.data.facilities.map(function(item){
    HotelFacilities.push({"facilitie": item})
})

let pages = [
    {
        "name": "HOMEPAGE",
        "data": [
            {
                "hero_image": ""
            },
            {
                "hero_text": DataPages.data.name
            },
            {
                "facilities": HotelFacilities
            },
            {
                "description" : ""
            }


        ]
    },
    {
        "name": "FACILITIES",
        "data": []
    },
    {
        "name": "ACCOMMODATION",
        "data": []
    },
    {
        "name": "GALLERY",
        "data": []
    },
    {
        "name": "LOCATION",
        "data": []
    },
    {
        "name": "CONTACT",
        "data": []
    }
];

let WP_PAGES = [

]

const hotelImgs = './static/large/';
let hotelIMAGES = [];
let WPHotelImagesId = [];

let files = fs.readdirSync(hotelImgs);

files.forEach(function(file) {
    hotelIMAGES.push( hotelImgs + file )
})

async function putDataToPages() {

    for ( const Litem of hotelIMAGES){

        let formData = {
            file: [fs.createReadStream(Litem)],
        };

        let WPMediaPost = {
            url : WPImages,
            headers : {
                "Authorization" : WPauth,
                "Content-Type" : "application/x-www-form-urlencoded",
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json",
                "Content-Disposition" : "attachment;"
            },
            method: "POST",
            formData: formData
        };

        const html = await REQUESTpages(WPMediaPost);
    }


    const WPCreatePost = {
        url : WPpages,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET",
        // body: JsonBody,
        //json: true
    }

    const v = await REQUESTpages(WPCreatePost)
    console.log('SHOULD WORK: READ Post');

    for( const ImItem of WP_PAGES ){
        for( const item of pages){
            if( ImItem.name === item.name) {
                let WPpagesSingle = WPpages + ImItem.id
                console.log(WPpagesSingle)

                let UpdateFields = {
                    "fields": {
                        "hero_image": WPHotelImagesId[1],
                        "hero_text": DataPages.data.name,
                        "room_description": "",
                        "facilities_heading":"FACILITIES",
                        "facilities": HotelFacilities,
                        "description": DataPages.data.description,
                        "parallax_top_image": WPHotelImagesId[0],
                        "parallax_top_text" : DataPages.data.name,
                        "parallax_bottom_image": WPHotelImagesId[1],
                        "parallax_bottom_text" : DataPages.data.name,
                        "gallery" : WPHotelImagesId,
                        "lat": DataPages.data.location.lat,
                        "long": DataPages.data.location.lon,
                        "directions": DataPages.data.directions
                    }
                }

                const WPUpdatePost = {
                    url : WPpagesSingle,
                    headers : {
                        "Authorization" : WPauth,
                        "Content-Type" : "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Accept": "application/json"
                    },
                    method: "POST",
                    body: UpdateFields,
                    json: true
                }

                const v = await REQUESTpages(WPUpdatePost)
            }
        }
    }

}
putDataToPages()

function REQUESTpages(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Create
            if (!error && response.statusCode === 201) {

                //Gallery Media
                if( typeof body !== 'object'){

                    let info = JSON.parse(body)
                    let uroom =  url.formData.file[0].path.split('/')
                    WPHotelImagesId.push(info.id)

                }else{
                    //CREATE ROOM
                    console.log("kanamme create page")

                }

            }else{
                console.log(error)
            }

            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){
                    let info = JSON.parse(body)
                    console.log("Eidame ta pages")

                    info.map(function(item){
                        WP_PAGES.push({"name":item.title.rendered ,"id":item.id})
                    })
                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

//CREATE POST

const roomsData = require('./static/roomsData.json')
let info = JSON.parse(roomsData)

const RoomImgs = './static/rooms/';
let roomsIMAGES = [];

let filesRooms = fs.readdirSync(RoomImgs);

filesRooms.forEach(function(file) {

    let RoomsName =  fs.readdirSync(RoomImgs + file)
    roomsIMAGES.push({name: file , images : []})
    RoomsName.forEach(function(fileImg) {

        fileImg = RoomImgs + file + '/'+ fileImg
        roomsIMAGES.map(function(item,i){

            if( item.name === file ){
                roomsIMAGES[i].images.push( fileImg )
            }
        })

    })

})

info.data.rooms.map(function(item,i){
    filesRooms.map(function(res){
        if( item.name === res){
            roomsIMAGES[i].code = item.code
            roomsIMAGES[i].book = DataPages.data.bookurl + "?nights=1&room=" + item.code
        }
    })

})

function REQUEST(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){

            //Create
            if (!error && response.statusCode === 201) {

                //Gallery Media
                if( typeof body !== 'object'){

                    let info = JSON.parse(body)
                    let uroom =  url.formData.file[0].path.split('/')

                    for(let i = 0; i < roomsIMAGES.length; i++){
                        if( roomsIMAGES[i].name === uroom[uroom.length - 2]){

                            for(let x =0; x < roomsIMAGES[i].images.length; x++){
                                if( roomsIMAGES[i].images[x] === url.formData.file[0].path){

                                    roomsIMAGES[i].images[x] = info.id

                                }
                            }

                        }
                    }

                }else{
                    //CREATE ROOM
                    for(let i = 0; i < roomsIMAGES.length; i++){
                        if( roomsIMAGES[i].name === body.title.raw){
                            roomsIMAGES[i].roomName = body.id
                        }
                    }

                }

            }else{
                console.log(error)
            }

            //Update
            if (!error && response.statusCode === 200) {
                console.log(2)
            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

async function myBackEndLogic() {

    try {
        // FOR EACH LOOP ASYNC/AWAIT
        for( let i = 0; i < roomsIMAGES.length; i++){

            for ( const Litem of roomsIMAGES[i].images){

                let formData = {
                    file: [fs.createReadStream(Litem)],
                };

                let WPMediaPost = {
                    url : WPImages,
                    headers : {
                        "Authorization" : WPauth,
                        "Content-Type" : "application/x-www-form-urlencoded",
                        "Access-Control-Allow-Origin": "*",
                        "Accept": "application/json",
                        "Content-Disposition" : "attachment;"
                    },
                    method: "POST",
                    formData: formData
                };

                const html = await REQUEST(WPMediaPost);
            }
        }

        for (const item of info.data.rooms) {

            const JsonBody = {
                "title" : "Jedi Rooms",
                "status" : "publish"
            }

            JsonBody.title = item.name

            const WPCreatePost = {
                url : WPurl,
                headers : {
                    "Authorization" : WPauth,
                    "Content-Type" : "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                method: "POST",
                body: JsonBody,
                json: true
            }

            const v = await REQUEST(WPCreatePost)
            console.log('SHOULD WORK: 3 kaname CREATE Post');


        }

        for( let x = 0; x < roomsIMAGES.length; x++){

            let WPFieldsUrl = "https://webhotelier.sitesdemo.com/api/wp-json/acf/v3/rooms/" + roomsIMAGES[x].roomName;

            let UpdateFields = {
                "fields": {
                    "room_gallerie":[
                        //222
                    ],
                    "room_name": roomsIMAGES[x].name,
                    "room_description": "",
                    "facilities_heading":"",
                    "facilities": [],
                    "more_rooms": [],
                    "book" : roomsIMAGES[x].book
                }
            }

            roomsIMAGES[x].images.map(function(item){
                UpdateFields.fields.room_gallerie.push(item)
            })

            UpdateFields.fields.room_image = roomsIMAGES[x].images[0]

            info.data.rooms.map(function(item){
                if( item.name === roomsIMAGES[x].name){
                    UpdateFields.fields.room_description = item.description;
                    UpdateFields.fields.facilities_heading = "FACILITIES";
                    item.amenities.map(function(amenitie){
                        UpdateFields.fields.facilities.push({"facilitie":amenitie})
                    })
                }
            })

            roomsIMAGES.map(function(item){
                if( !(item.name === roomsIMAGES[x].name)){
                    UpdateFields.fields.more_rooms.push({"select_room":item.roomName})
                }
            })

            let WPUpdatePost = {
                url : WPFieldsUrl,
                headers : {
                    "Authorization" : WPauth,
                    "Content-Type" : "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                method: "POST",
                body: UpdateFields,
                json: true
            }

            const cc = await REQUEST(WPUpdatePost)
            console.log('SHOULD WORK: 4 kaname update Post');
        }

        for (const item of info.data.rooms) {

            const JsonBody = {
                "title" : "Jedi Rooms",
                "status" : "publish"
            }

            JsonBody.title = item.name

            const WPCreatePost = {
                url : WPurlGallery,
                headers : {
                    "Authorization" : WPauth,
                    "Content-Type" : "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                method: "POST",
                body: JsonBody,
                json: true
            }

            const v = await REQUEST(WPCreatePost)
            console.log('SHOULD WORK: 3 kaname CREATE Gallery');

        }

        for( let x = 0; x < roomsIMAGES.length; x++){

            let WPFieldsUrl = "https://webhotelier.sitesdemo.com/api/wp-json/acf/v3/galleries/" + roomsIMAGES[x].roomName;

            let UpdateFields = {
                "fields": {
                    "gallery":[
                        //222
                    ],
                    //"gallery_image" : ""
                }
            }

            roomsIMAGES[x].images.map(function(item){
                UpdateFields.fields.gallery.push(item)
            })

            UpdateFields.fields.gallery_image =  roomsIMAGES[x].images[0]


            let WPUpdatePost = {
                url :WPFieldsUrl,
                headers : {
                    "Authorization" : WPauth,
                    "Content-Type" : "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                method: "POST",
                body: UpdateFields,
                json: true
            }

            const cc = await REQUEST(WPUpdatePost)
            console.log('SHOULD WORK: 4 kaname update Post');
        }

    } catch (error) {

        console.error('ERROR:');
        console.error(error);

    }

}

//run async function
myBackEndLogic();
console.log(roomsIMAGES)




