import React, {Component} from 'react'
import Layout from '../components/layout'
import Link from 'next/link'
import HeroSlider from '../components/hero_slider'
import RoomGallery from '../components/room-gallery'
import DATA from '../static/WPdataRooms.json'
import DATAEL from '../static/WPdataRooms-el.json'
import GALLERY from '../static/WPdataGalleries.json'
import OPTIONS from '../static/options.json'
import Form from '../components/booking/booking-form'

const RoomPage = (props) =>

    <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.postId.Menu } >

        <section className={"hero_slider __home rel  "}>
            <div className={"row no-padding-col"}>
                <HeroSlider
                    images={props.postId.data.page.main_slider_images}
                    settings={props.postId.sliderSettings }
                />
                <div className={"room_gallery_slider"}>{props.postId.data.page.room_name}</div>
            </div>
        </section>

        <section style={ {minHeight: "170px"} } className={"hidden-sm-down"}>
            <div className="row  pad-cont no_grey no-padding-col">
                <div className={"col-md-12"}>
                    <div className="container">
                        <Form bookingUrl={props.postId.options.bookNow} />
                    </div>
                </div>
            </div>
        </section>

        <section className={"intro"}>
            <div className={"row no-padding-col text-center rooms_inner"}>

                <div className={"col-md-12"}>
                    <div className="primary_color_body title_room">{props.postId.data.page.room_name}</div>
                    <div className="small_italic"> Elegant designer’s den </div>
                </div>

                <div className="col-md-12 room_highlights">
                    <div className="room_highlights_inner room_overview font-semibold">
                        {props.postId.data.page.hightligts.map(function(item , i){
                            return (
                                <div key={i}>
                                    <img src={item.icon} />
                                    <span>{item.text}</span>
                                </div>
                            )
                        })}
                    </div>
                </div>

                <div className="room_overview" dangerouslySetInnerHTML={{__html:"<div className='_animation'>" + props.postId.data.page.overview  + "</div>"}}>
                </div>

                <div className="col-md-12 facilities_rooms rooms_text_color">
                    <div className="top_remove_border"></div>
                    <div className="bottom_remove_border"></div>

                    <div className="fac-heading room_overview"> {props.postId.data.page.facilities_heading} </div>

                    <div className="col-md-12 facilities-in font-openSans primary_color_white">
                        <div  dangerouslySetInnerHTML={{__html: props.postId.data.page.facilities }}>
                        </div>
                    </div>

                    <Link href={props.postId.options.bookNow + "?room=" + props.postId.data.page.book_now + "&adults=2"}  as={props.postId.options.bookNow + "?room=" + props.postId.data.page.book_now + "&adults=2"}>
                        <a className="btn font-light btn_text">{props.locale.bookNow}</a>
                    </Link>

                </div>

            </div>
        </section>

        <section>
            <RoomGallery
                gallery={props.postId.data.gallery.gallery}
                slug={props.postId.data.gallery.post_slug}
                count={10}
                locale={props.locale.loadGalleries}
                localeCategory={props.locale.gallery}
            />
        </section>

    </Layout>


RoomPage.getInitialProps = async ({ query}) => {

    let url = { query }

    const PAGE = {};

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng,
        slug: url.query.roomslug,
        room: url.query.roomName
    }

    PAGE.sliderSettings = {
        dots: false,
        fade: true,
        infinite: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 500,
        accessibility:false,
        touchMove:false,
        slidesToShow: 1,
        slidesToScroll: 1
    }

    PAGE.data = {}

    PAGE.options = OPTIONS

    DATA.map(function(item){
        if( item.room_name.split(" ").join("-").toLowerCase() === url.query.roomslug){
            PAGE.data.page = item;
            PAGE.SEO = {
                title: item.SEO.title,
                description: item.SEO.description,
                keywords: item.SEO.keywords
            }
        }
    })

    GALLERY.map(function(item){
        if( item.post_name.split(" ").join("-").toLowerCase() === url.query.roomslug){
            PAGE.data.gallery = item;
        }
    })


    PAGE.LOCALE = {
        services: "SERVICES & AMENITIES",
        bookNow: "BOOK NOW",
        gallery: "Choose Our Category",
        loadGalleries: "LOAD MORE GALLERIES"
    }

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                DATAEL.map(function(item){
                    if( item.room_name.split(" ").join("-").toLowerCase() === url.query.roomslug){
                        PAGE.data.page = item;
                        PAGE.SEO = {
                            title: item.SEO.title,
                            description: item.SEO.description,
                            keywords: item.SEO.keywords
                        }
                    }
                })

                GALLERY.map(function(item){
                    if( item.post_name.split(" ").join("-").toLowerCase() === url.query.roomslug){
                        PAGE.data.gallery = item;
                    }
                })



                PAGE.LOCALE = {
                    services: "SERVICES & AMENITIES",
                    bookNow: "ΚΡΑΤΗΣΗ",
                    gallery: "Διαλέξτε μια κατηγορία",
                    loadGalleries: "Φωρτοστέ περισσότερα"
                }
                break;
            default:
        }
    }

    return {postId: PAGE , seo: PAGE.SEO , locale: PAGE.LOCALE}

}


export default RoomPage

