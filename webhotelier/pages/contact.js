import React, {Component} from 'react'
import Layout from '../components/layout'
import DATA  from '../static/pages/contact.json'
import Contact from '../components/contact'
import DATAEL from "../static/pages/contact-el.json";


const ContactPage = (props) =>
    <div>
        <Layout  title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.postId.Menu } >

            <div className="row">
                <div className="col-md-12 Contact-details secondary_bg_color_body hero_start pull-center small_hero_heading">

                    <h1 className="primary_heading">{props.postId.TEXTTOP}</h1>

                    <div>
                        <span className="fa fa-map-marker color--brand"></span>
                        <h2>{props.postId.data.location}</h2>
                    </div>

                    <div>
                        <span className="fa fa-phone color--brand"></span>
                        <h2><a href={"tel:" + props.postId.data.telephone[0]}>{props.postId.data.telephone[0]}</a></h2>
                    </div>

                    { props.postId.data.fax_number.length > 0 && <div>
                            <span className="fa fa-fax color--brand"></span>
                            <h2><a href={"tel:" + props.postId.data.fax_number}>{props.postId.data.fax_number}</a></h2>
                        </div>
                    }

                    <div>
                        <span className="fa fa-envelope color--brand"></span>
                        <h2><a href={"mailto:" + props.postId.mail} className='email'>{props.postId.data.mail}</a></h2>
                    </div>

                </div>

                <div className="contact_form container-fluid pull-center  primary_bg_color_body">

                    <div className="v_small_hero_heading primary_heading primary_color_white">{props.postId.TEXTBOTTOM}</div>

                    <div className="row">
                        <Contact />
                    </div>

                </div>
            </div>

        </Layout>
    </div>

ContactPage.getInitialProps = async ({ query }) => {

    const url = { query }

    const PAGE = {};

    PAGE.sliderSettings = {
        dots: false,
        fade: true,
        infinite: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 500,
        accessibility:false,
        touchMove:false,
        slidesToShow: 1,
        slidesToScroll: 1
    }

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }

    PAGE.data = DATA
    PAGE.TEXTTOP = 'CONTACT DETAILS'
    PAGE.TEXTBOTTOM = 'CONTACT US'

    PAGE.SEO = {
        title: DATA.SEO.title,
        description: DATA.SEO.description,
        keywords: DATA.SEO.keywords,
    }

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                PAGE.data = DATAEL;
                PAGE.TEXTTOP = 'ΣΤΟΙΧΕΙΑ ΕΠΙΚΟΙΝΩΝΙΑΣ'
                PAGE.TEXTBOTTOM = 'ΦΟΡΜΑ ΕΠΙΚΟΙΝΩΝΙΑΣ'
                PAGE.SEO = {
                    title: DATAEL.SEO.title,
                    description: DATAEL.SEO.description,
                    keywords: DATAEL.SEO.keywords,
                }
                break;
            default:
        }
    }

    return {postId: PAGE , seo: PAGE.SEO }

}

export default ContactPage
