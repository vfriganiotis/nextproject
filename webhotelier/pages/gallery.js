import React, {Component} from 'react'
import Layout from '../components/layout'
import Gallery from '../components/gallery'
import Parallax from '../components/parallax'
import WPPages from '../static/WPdataPages.json'

const PageGallery = (props) =>
    <div>
        <Layout>

            <Parallax img={props.postId.homepage.acf.parallax_top_image.url} content={props.postId.homepage.acf.parallax_top_text}/>

            <div class="hero_start pull-center">
                <h1 className="primary_heading">PHOTO GALLERY</h1>
            </div>

            <Gallery url={props.postId.homepage.acf.newGallery} count={100}></Gallery>

        </Layout>
    </div>

PageGallery.getInitialProps = async ({ req }) => {

    let dataJson = {}
    let WPPAGES = JSON.parse(WPPages)

    WPPAGES.map(function(item){
        if( item.title.rendered === 'GALLERY'){
            return  dataJson.homepage = item
        }
    })

    //create Gallery Images array
    dataJson.homepage.acf.newGallery = [];
    dataJson.homepage.acf.gallery.map(function(item){
        dataJson.homepage.acf.newGallery.push(item.url)
    })

    return {postId: dataJson}
}

export default PageGallery

