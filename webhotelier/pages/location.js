import React, {Component} from 'react'
import Layout from '../components/layout'
import WPPages from '../static/pages/location.json'
import WPPagesEL from '../static/pages/location-el.json'
import AccordionMapComponent from '../components/map/map-block'
import WPMaps from '../static/WPdataMaps.json'
import WPMapsEl from '../static/WPdataMaps-el.json'
import WPTaxs from '../static/WPdataTaxs.json'
import WPTaxsEl from '../static/WPdataTaxs-el.json'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'
import WhyChooseHome from '../components/home_choose_us'

const LocationPage = (props) =>

    <section className={"secondary_bg_color_body"}>
        <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={props.menu} >


            <section className="full_map_cont">
                <AccordionMapComponent markers={props.markers} center={props.center}  taxonomies={props.taxonomies} lang={props.lang}/>
            </section>

            <section>
                <WhyChooseHome
                    title={props.postId.loverview}
                    text={props.postId.loverview_content[0]}
                    whyChoose={props.postId.why_choose_us}
                    whyChooseList={props.postId.why_choose_us_content}
                    link={props.postId.book_now}
                    buttonText={props.locale.readMore}
                    buttonLess={props.locale.readLess}
                    buttonNow={props.locale.bookNow}
                />
            </section>

            <section>
                <div className="row rooms_inner no-padd-bottom secondary_bg_color_body">
                    <div className="col-md-12 text-center">

                        <h1 className="primary_heading"> {props.hightLightsText} </h1>

                        <div className="terms primary_color_body">
                            {props.taxes.map(function(item,i){

                                let pathname = "/location/" + item.post_slug
                                let itemHref = "/taxonomies"  + "?slug=" + item.post_slug

                                let LINK =  <Link href={ itemHref } as={ pathname } >
                                    <a>{item.post_name}</a>
                                </Link>

                                if( props.lang !== undefined){
                                    switch(props.lang) {
                                        case 'el':
                                            pathname = "/el/location/" + item.post_slug
                                            itemHref = "/taxonomies"  + "?slug=" + item.post_slug + "&lang=" + props.lang

                                            LINK = <Link href={itemHref } as={ pathname } >
                                                <a>{item.post_name}</a>
                                            </Link>
                                            break;
                                        default:
                                    }
                                }

                                return (
                                    <div key={i}>
                                        {LINK}
                                        <span className="divider">/</span>
                                    </div>
                                )

                            })}
                        </div>

                    </div>
                </div>
            </section>

            <section>
                <div className="row  rooms_inner">

                    {props.sightsId.map(function(item,i){

                        let pathname = "/location/" +  item.taxonomies  + "/" + item.post_slug

                        let itemHref = "/template-map" + "?sight=" + item.taxonomies + "&slug=" + item.post_slug

                        let LINK = <Link href={ itemHref } as={ pathname } >
                            <a>READ MORE</a>
                        </Link>

                        if( props.lang !== undefined){
                            switch(props.lang) {
                                case 'el':

                                    pathname = "/el/location/" +  item.taxonomies  + "/" + item.post_slug
                                    itemHref = "/template-map" + "?sight=" + item.taxonomies + "&slug=" + item.post_slug + "&lang=" + props.lang

                                    LINK = <Link href={itemHref } as={ pathname } >
                                        <a>ΠΕΡΙΣΣΟΤΕΡΑ</a>
                                    </Link>
                                    break;
                                default:
                            }
                        }

                        let styles = {
                            background: `url(${item.image})`,
                            height: '450px',
                            backgroundSize: 'cover',
                            backgroundPosition: "center"
                        };


                        let regex = /(<([^>]+)>)/ig;
                        let result = item.content.replace(regex, '');

                        return (
                            <div key={i} className="col-md-6">
                                <div className="single-map-container">
                                    <div className="image-map" style={styles}></div>
                                    <div className="name-map title_head ">{item.post_name}</div>
                                    <div className="content-map">
                                        <LinesEllipsis
                                            className="text"
                                            text= {result}
                                            maxLine={5}
                                            ellipsis='...'
                                            trimRight
                                            basedOn='letters'
                                        />
                                    </div>
                                    <div className="read-map btns">{LINK}</div>
                                </div>
                            </div>
                        )
                    })}

                </div>
            </section>


            <style jsx>{`

                .single-map-container{
                   margin: 20px 0;
                   box-shadow: 0px 0px 20px #000;
                   padding: 20px;
                }

                .location-container{
                   width: 100%;
                }

                .read-map{
                    margin-top: 20px;
                 }

                 .content-map{
                   min-height: 120px;
                 }

                .read-map a{
                    color: #fff;
                    text-decoration: none;
                }

                .title_head {
                   margin: 20px 0;
                   min-height: 60px;
                }

                .terms{
                    padding: 30px;
                    background: #fff;
                    font-size: 20px;
                }

                .divider{
                    padding-left: 10px;
                }

                .terms > div:nth-last-child(1) .divider {
                    display: none;
                }

                .terms a{
                    display: inline-block;
                    color: #000;
                    text-decoration: none;
                    padding-left: 10px;
                }

                .terms > div{
                   display: inline-block;
                }

              `}</style>


        </Layout>
    </section>
LocationPage.getInitialProps = async ({ query }) => {

    const url = { query }

    const PAGE = {};

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }

    let WPPAGES = WPPages
    let WPMAPS = WPMaps
    let TAXS = WPTaxs
    let highlightText = 'More Highlights'

    let CENTER = {
        lat: parseFloat(WPPAGES.google_map_lat),
        lng: parseFloat(WPPAGES.google_map_lng)
    }

    PAGE.SEO = {
        title: WPPAGES.SEO.title,
        description: WPPAGES.SEO.description,
        keywords: WPPAGES.SEO.keywords,
    }

    PAGE.LOCALE = {
        readMore: "READ MORE",
        readLess: "READ LESS",
        bookNow: "BOOK NOW"
    }

    WPPAGES.markers = WPMaps;

    let anotherOne =  WPPAGES.select_sightseeings;
    let filteredArray = WPMAPS.filter(itemX => anotherOne.includes(itemX.post_id));

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':

                WPPAGES = WPPagesEL
                WPMAPS = WPMapsEl
                TAXS = WPTaxsEl
                WPPAGES.markers = WPMapsEl;
                highlightText = 'Περισσότερα Αξιοθέατα'

                anotherOne =  WPPAGES.select_sightseeings;
                filteredArray = WPMAPS.filter(itemX => anotherOne.includes(itemX.post_id));

                PAGE.SEO = {
                    title: WPPAGES.SEO.title,
                    description: WPPAGES.SEO.description,
                    keywords: WPPAGES.SEO.keywords,
                }

                PAGE.LOCALE = {
                    readMore: "ΠΕΡΙΣΣΟΤΕΡΑ",
                    readLess: "ΛΙΓΟΤΕΡΑ",
                    bookNow: "ΚΡΑΤΗΣΗ",
                }

                break;
            default:
        }
    }


    return {locale: PAGE.LOCALE, seo: PAGE.SEO, lang: PAGE.Menu.firstLoad ,postId: WPPAGES, maps: WPMAPS , menu: PAGE.Menu , markers : WPMAPS , center: CENTER , taxonomies: TAXS , taxes: TAXS , sightsId : filteredArray , hightLightsText: highlightText}

}


export default LocationPage
