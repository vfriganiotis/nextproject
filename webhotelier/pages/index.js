import React, {Component} from 'react'
import Layout from '../components/layout'
//import dynamic from 'next/dynamic'
import HeroSlider from '../components/hero_slider'
//const HeroSlider = dynamic(() => import('../components/hero_slider'));
import ReviewsSliderHome from '../components/reviews_slider_home'
//const ReviewsSliderHome = dynamic(() => import('../components/reviews_slider_home'));
import WhyChooseUs from '../components/why_choose_us'
//const WhyChooseUs = dynamic(() => import('../components/why_choose_us'));
import WhyChooseHome from '../components/home_choose_us'
//const WhyChooseHome = dynamic(() => import('../components/home_choose_us'));
import ShowRoomsAndSlogans from '../components/rooms/rooms_slogans_home'
//const ShowRoomsAndSlogans = dynamic(() => import('../components/rooms/rooms_slogans_home'));
import GalleriesComponent from '../components/galleries_component'
//const GalleriesComponent = dynamic(() => import('../components/galleries_component'));
import Form from '../components/booking/booking-form'
//const Form = dynamic(() => import('../components/booking/booking-form'));
import HomeMap from '../components/map/home-map'
//const HomeMap  = dynamic(() => import('../components/map/home-map'));

//const Observer = dynamic(() => import('@researchgate/react-intersection-observer'));

import DATA from '../static/pages/homepage.json'
import DATAEL from '../static/pages/homepage-el.json'
import localBusiness from "../static/schemas/schema.json"

import Observer from "@researchgate/react-intersection-observer";

const IndexPage = (props) =>

        <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.postId.Menu } localBusiness={props.seo.localBusiness}>

                <section className="hero_slider __home rel ">
                    <div className="row no-padding-col">
                        <HeroSlider images={props.postId.data.home.main_slider_images} settings={props.postId.sliderSettings }/>
                        <div className="main_slider_heading font-openSans primary_color_white">
                            <div className="slider_subheading"> {props.postId.data.home.main_slider_heading} </div>
                            <div className="slider_heading"> {props.postId.data.home.main_slider_content} </div>
                        </div>
                    </div>
                </section>

                <section style={ {minHeight: "170px"} } className={"hidden-sm-down"}>
                    <div className="row  pad-cont no_grey no-padding-col">
                        <div className={"col-md-12"}>
                            <div className="container">
                                <Form bookingUrl={props.postId.data.home.book_now} />
                            </div>
                        </div>
                    </div>
                </section>

                <section style={ {minHeight: "395px"} }>
                    <ObservableHome>
                        <WhyChooseHome
                            title={props.postId.data.home.main_slider_content}
                            text={props.postId.data.home.overview_content[0]}
                            whyChoose={props.postId.data.home.why_choose_us}
                            whyChooseList={props.postId.data.home.why_choose_us_content}
                            link={props.postId.data.home.book_now}
                            buttonText={props.locale.readMore}
                            buttonLess={props.locale.readLess}
                            buttonNow={props.locale.bookNow}
                        />
                    </ObservableHome>
                </section>

                <section style={ {minHeight: "1260px"}}>
                    <ObservableHome>
                        <ShowRoomsAndSlogans
                            rooms={props.postId.data.rooms}
                            language={props.postId.Menu.firstLoad}
                            rooms_and_layouts={props.postId.data.home.rooms_and_layouts}
                        />
                    </ObservableHome>
                </section>

                <section style={ {minHeight: "260px"} }>
                    <ObservableHome>
                        <div className="row  no-padding-col">
                            <div className={"col-md-12"}>
                                <ReviewsSliderHome
                                    title="hee"
                                    backgroundColor={props.postId.data.home.reviews_bg_color}
                                    slides={props.postId.data.home.reviews}
                                />
                            </div>
                        </div>
                    </ObservableHome>
                </section>

                <section style={ {minHeight: "520px"} }>
                    <ObservableHome>
                        <HomeMap
                            lat={props.postId.data.lat}
                            lon={props.postId.data.lng}
                            title={props.locale.contact}
                            text={props.postId.data.contactLocation}
                            tel={props.postId.data.contactTelephone}
                            mail={props.postId.data.contactMail}
                            button={props.locale.location}
                            link={"location"}
                            language={props.postId.Menu.firstLoad}
                        />
                    </ObservableHome>
                </section>

                <section style={ {minHeight: "205px"} }>
                    <ObservableHome>
                        <div className="row no-padding-col">
                            <div className="col-md-12 pull-center">
                                <WhyChooseUs
                                    title={props.postId.data.home.main_slider_content}
                                    chooseOne={props.locale.facilities}
                                    chooseOneLink={"services"}
                                    chooseTwo={props.locale.location}
                                    chooseTwoLink={"location"}
                                    language={props.postId.Menu.firstLoad}
                                />
                            </div>
                        </div>
                    </ObservableHome>

                </section>

                <section style={ {minHeight: "485px"} }>

                    <ObservableHome>
                        <GalleriesComponent
                            language={props.postId.Menu.firstLoad}
                            count={2}
                            locale={props.locale.loadGalleries}
                            localeCategory={props.locale.gallery}
                        />
                    </ObservableHome>

                </section>

        </Layout>


IndexPage.getInitialProps = async ({ query }) => {

    const url = { query }

    const PAGE = {};

    PAGE.sliderSettings = {
        dots: false,
        fade: true,
        infinite: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 500,
        accessibility:false,
        touchMove:false,
        slidesToShow: 1,
        slidesToScroll: 1
    }

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }


    PAGE.data = DATA

    PAGE.SEO = {
        title:  DATA.home.SEO.title,
        description:  DATA.home.SEO.description,
        keywords: DATA.home.SEO.keywords,
        localBusiness: localBusiness
    }

    PAGE.LOCALE = {
        readMore: "READ MORE",
        readLess: "READ LESS",
        bookNow: "BOOK NOW",
        location: "LOCATION",
        contact: "CONTACT US",
        facilities: "FACILITIES",
        gallery: "Choose Our Category",
        loadGalleries: "LOAD MORE GALLERIES"
    }

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                PAGE.data = DATAEL;
                PAGE.SEO = {
                    title: DATAEL.home.SEO.title,
                    description: DATAEL.home.SEO.description,
                    keywords: DATAEL.home.SEO.keywords,
                }

                PAGE.LOCALE = {
                    contact: "ΕΠΙΚΟΙΝΩΝΙΑ",
                    readMore: "ΠΕΡΙΣΣΟΤΕΡΑ",
                    readLess: "ΛΙΓΟΤΕΡΑ",
                    bookNow: "ΚΡΑΤΗΣΗ",
                    location: "ΤΟΠΟΘΕΣΙΑ",
                    facilities: "ΥΠΗΡΕΣΙΕΣ",
                    gallery: "Διαλέξτε μια κατηγορία",
                    loadGalleries: "Φωρτοστέ περισσότερα"
                }

                break;
            default:
        }
    }

    return {postId: PAGE , seo: PAGE.SEO , locale: PAGE.LOCALE}

}


export default IndexPage

class ObservableHome extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            options: this.props.options,
            bootstrapURLKeys: this.props.bootstrapURLKeys,
            defaultCenter: this.props.defaultCenter,
            defaultZoom:this.props.defaultZoom,
            defaultScrollwheel: this.props.defaultScrollwheel,
            defaultIcon: this.props.defaultIcon,
            onGoogleApiLoaded: this.props.onGoogleApiLoaded

        };
        this.handleIntersection = this.handleIntersection.bind(this);

    }

    handleIntersection(event) {

        if(event.isIntersecting){

            this.setState({
                show: true
            })

        }

    }

    render() {

        const { show } = this.state;
        let options = {
            onChange: this.handleIntersection,
            root: '#scrolling-container',
            threshold: 0.25,
            disabled: show,
            rootMargin: '550px'
        };

        return (

            <div  className={(show ? "revealed" : "no-revealMaps")}>
                <Observer {...options} >
                    <div>
                        {show === true && this.props.children}
                    </div>
                </Observer>
            </div>

        );
    }
}