import React, {Component} from 'react'
import Layout from '../components/layout'
import ThreeLayoutComponent from '../components/reviews/three-layout'
import DATAEL from "../static/WPdataReviews";
import DATA from "../static/WPdataReviews";
import WPSEO from '../static/pages/reviews.json'
import WPSEOEL from '../static/pages/reviews-el.json'
import localBusiness from "../static/schemas/reviews.json"
import { CircularProgressbar, buildStyles , CircularProgressbarWithChildren  } from 'react-circular-progressbar';
import "react-circular-progressbar/dist/styles.css";

const percentage = 66;

const PageReviews = (props) =>

    <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={props.postId.Menu} localBusiness={props.seo.localBusiness} >

        <div className={"row"}>

            <div className="container-fluid secondary_bg_color_body">

                <div className={"container pull-center hero_start"}>

                    <h1 className="primary_heading">{props.locale.title}</h1>

                    <div className={"circles_bar_container row"}>
                        <div className={"col-md-12"}>

                            <div className={"circles_bar"}>
                                <CircularProgressbarWithChildren value={84} styles={buildStyles({
                                    textColor: "red",
                                    pathColor: "#FFEB3B",
                                    trailColor: "#DFDEDE"
                                })}>
                                    {/* Put any JSX content in here that you'd like. It'll be vertically and horizonally centered. */}
                                    <img style={{ width: 40, marginTop: -5 }} src="/static/assets/trip-review.png" alt="tripAdvisor" />
                                    <div style={{ fontSize: 12, marginTop: 0 }}>
                                        <strong style={{ display: "block" }}>84%</strong>
                                        {props.postId.REVIEWS.tripadvisor.people} Persons
                                    </div>
                                </CircularProgressbarWithChildren>
                            </div>


                            <div className={"circles_bar"}>
                                <CircularProgressbarWithChildren value={88} styles={buildStyles({
                                    textColor: "red",
                                    pathColor: "#003382",
                                    trailColor: "#DFDEDE"
                                })}>
                                    {/* Put any JSX content in here that you'd like. It'll be vertically and horizonally centered. */}
                                    <img style={{ width: 40, marginTop: -5 }} src="/static/assets/booking.png" alt="tripAdvisor" />
                                    <div style={{ fontSize: 12, marginTop: 0 }}>
                                        <strong style={{ display: "block" }}>{88}%</strong>
                                        {props.postId.REVIEWS.booking.people} Persons
                                    </div>
                                </CircularProgressbarWithChildren>
                            </div>
                            <div className={"circles_bar"}>
                                <CircularProgressbarWithChildren value={84} styles={buildStyles({
                                    textColor: "red",
                                    pathColor: "#3A5BA0",
                                    trailColor: "#DFDEDE"
                                })}>
                                    {/* Put any JSX content in here that you'd like. It'll be vertically and horizonally centered. */}
                                    <img style={{ width: 40, marginTop: -5 }} src="/static/assets/facebook.png" alt="doge" />
                                    <div style={{ fontSize: 12, marginTop: 0 }}>
                                        <strong style={{ display: "block" }}>{84}%</strong>
                                        {props.postId.REVIEWS.facebook.people} Persons
                                    </div>
                                </CircularProgressbarWithChildren>
                            </div>
                            <div className={"circles_bar"}>
                                <CircularProgressbarWithChildren value={80} styles={buildStyles({
                                    textColor: "red",
                                    pathColor: "#D32E2E",
                                    trailColor: "#DFDEDE"
                                })}>
                                    {/* Put any JSX content in here that you'd like. It'll be vertically and horizonally centered. */}
                                    <img style={{ width: 40, marginTop: -5 }} src="/static/assets/hotels.png" alt="doge" />
                                    <div style={{ fontSize: 12, marginTop: 0 }}>
                                        <strong style={{ display: "block" }}>{80}%</strong>
                                        {props.postId.REVIEWS.hotels.people} Persons
                                    </div>
                                </CircularProgressbarWithChildren>
                            </div>
                        </div>
                    </div>

                    <ThreeLayoutComponent  reviews={props.postId.data}/>

                </div>

            </div>

        </div>

    </Layout>


PageReviews.getInitialProps = async ({  query  }) => {

    const url = { query }

    const PAGE = {};

    PAGE.sliderSettings = {
        dots: false,
        fade: true,
        infinite: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 500,
        accessibility:false,
        touchMove:false,
        slidesToShow: 1,
        slidesToScroll: 1
    }

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }

    PAGE.data = DATA

    PAGE.SEO = {
        title: WPSEO.SEO.title,
        description: WPSEO.SEO.description,
        keywords: WPSEO.SEO.keywords,
        localBusiness: localBusiness
    }

    PAGE.LOCALE = {
        title: "REVIEWS"
    }

    PAGE.REVIEWS = WPSEO

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                PAGE.data = DATAEL;
                PAGE.SEO = {
                    title: WPSEOEL.SEO.title,
                    description: WPSEOEL.SEO.description,
                    keywords: WPSEOEL.SEO.keywords,
                }
                PAGE.LOCALE = {
                    title: "ΚΡΙΤΙΚΕΣ"
                }
                PAGE.REVIEWS = WPSEO
                break;
            default:
        }
    }

    return { postId: PAGE , seo: PAGE.SEO , locale: PAGE.LOCALE }

}

export default PageReviews

