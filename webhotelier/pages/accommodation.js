import React, {Component} from 'react'
import Layout from '../components/layout'
import Parallax from '../components/parallax'
import IntroSection from '../components/intro_section'
import RoomItemsTwo from '../components/accommodation/accommodationTwo'
import ROOMS from '../static/WPdataRooms.json'
import ROOMSEL from '../static/WPdataRooms-el.json'
import DATA from '../static/pages/accommodation.json'
import DATAEL from '../static/pages/accommodation-el.json'

class Accommodation extends React.Component {

   static async getInitialProps({ query }) {

       let url = { query }

       const PAGE = {};

       PAGE.Menu = {
           firstLoad : url.query.lang || url.query.lng,
           lang : url.query.lng
       }

       PAGE.data = {
           page : DATA,
           rooms : ROOMS,
           text : "ACCOMMODATION"
       }

       PAGE.SEO = {
           title: DATA.SEO.title,
           description: DATA.SEO.description,
           keywords: DATA.SEO.keywords,
       }

       PAGE.LOCALE = {
           accommodation: "Accommodation",
           bestprice: "Best Price Guarantee",
       }

       if( PAGE.Menu.firstLoad !== undefined){
           switch(PAGE.Menu.firstLoad) {
               case 'el':
                   PAGE.data.page = DATAEL;
                   PAGE.data.rooms = ROOMSEL;
                   PAGE.data.text = "ΔΙΑΜΟΝΗ";
                   PAGE.SEO = {
                       title: DATAEL.SEO.title,
                       description: DATAEL.SEO.description,
                       keywords: DATAEL.SEO.keywords,
                   }

                   PAGE.LOCALE = {
                       accommodation: "Διαμονή",
                       bestprice: "Εγγύηση Καλύτερης Τιμής",
                   }

                   break;
               default:
           }
       }

       return { postId: PAGE , seo: PAGE.SEO , locale: PAGE.LOCALE }

    }


    render(){

        return (
            <Layout  title={this.props.seo.title} description={this.props.seo.description} keywords={this.props.seo.keywords} menu={ this.props.postId.Menu } >

               <Parallax img={this.props.postId.data.page.background_Hero}/>


                <div className="row ">
                    <div className="acc-top col-md-12">
                        <IntroSection title={this.props.locale.accommodation} subtitle={this.props.locale.bestprice} />
                    </div>
                </div>

                <div className="row">

                    <RoomItemsTwo rooms={ this.props.postId.data.rooms }  lang={ this.props.postId.Menu.firstLoad } />

                </div>

            </Layout>
        )

    }
}

export default Accommodation



