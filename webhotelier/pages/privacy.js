import React, {Component} from 'react'
import Layout from '../components/layout'
import DATA  from '../static/pages/contact.json'
import Contact from '../components/contact'
import DATAEL from "../static/pages/contact-el.json";

const PrivacyPage = (props) =>
    <div>
        <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.postId.Menu } >

            <div className="row">
                <div className="col-md-12 privacy-details secondary_bg_color_body hero_start pull-center small_hero_heading">
                    <p>
                        Data
                    privacy protection
                    In our company, personal data security was a priority even before GDPR.
                    Below you will be informed about the changes we have made to the terms of use, the privacy policy we apply and the cookies policy on our site.1. Data collection
                    We collect only those data that are necessary for the overall management of your order.
                    We collect your email only if we have your consent to inform you about promotions or products and services. The collection and storage of data is done with confidentiality and security.
                    Our company does not sell, disclose, or in any other way provide personal data submitted by visitors to our site, to any third parties unless it is compelled by law. As such data, we mean the name, address, telephone number and fax, as and your e-mail address. Our store can use anonymized data for statistic purposes (browser type, geographic location) to optimize the site as part of the ongoing internal review process and, generally, to get to know our consumers better.
                    </p>
                    <p>2. Purpose of collecting
                        We collect data for your quicker service (ordering) and for your personal use as to see the orders you have placed.
                        This information will not be used for unintended communication unless we have previously informed you about this possible use and you give us your permission. If we have your permission, our company may use this information to:
                        • Inform you for offers
                        • Send you forms or other email
                        • Send you newsletters or other announcements electronically
                        • Deliver you products or awards
                    </p>
                    <p>3. Data storage time
                        We keep your data in our online store until you ask us (through a simple and easy procedure) to delete them. In other cases, the data remain in our system for statistical purposes, but also for easier service on your next purchases. For any information regarding your data you can contact us.
                    </p>
                    <p>4. Rights
                        Our company recognizes the client’s rights, including the right to update your data if you have an online account, the right to delete your personal data, the right to request not to receive email, the right for the portability of your data, as well as the right to object to the consent and data processing. We recommend that children and young people under the age of 18 get permission from their parents before submitting their personal data to the website.
                    </p>
                    <p>Our website works in a secure SSL environment.
                        For more information, please contact us.
                    </p>
                    <p>ΙΔΙΩΤΙΚΟΤΗΤΑ
                        Προστασία προσωπικών δεδομένων
                        Στην εταιρεία μας η ασφάλεια των προσωπικών δεδομένων ήταν προτεραιότητα για μας και πριν τον GDPR.
                        Παρακάτω απλά ενημερωθείτε για τις αλλαγές που έχουμε κάνει στους όρους χρήσης, στην πολιτική προστασίας προσωπικών δεδομένων που εφαρμόζουμε καθώς και στην πολιτική των cookies στην ιστοσελίδα μας.
                    </p>
                    <p>1. Συλλογή στοιχείων
                        Συλλέγουμε μόνο εκείνα τα στοιχεία σας τα οποία είναι απαραίτητα για να σας εξυπηρετήσουμε αποστέλλοντας σας τις παραγγελίες σας.
                        Συλλέγουμε το email σας μόνο εφόσον έχουμε τη συγκατάθεσή σας για να σας ενημερώσουμε για νέα προϊόντα και άλλα νέα μας. Η συλλογή και αποθήκευση των στοιχείων γίνεται με προσοχή και ασφάλεια. Η εταιρεία μας δεν πουλά, ενοικιάζει, ή καθ’ οποιονδήποτε άλλον τρόπο παραχωρεί προσωπικά στοιχεία που υποβάλλονται από επισκέπτες του site μας προς οποιοδήποτε τρίτο μέρος, εκτός αν το επιβάλει ο νόμος. Ως τέτοια στοιχεία εννοούμε το όνομα, την διεύθυνση, τον αριθμό τηλεφώνου και fax, καθώς και την ηλεκτρονική σας διεύθυνση (e-mail). Το κατάστημά μας μπορεί να κάνει χρήση απρόσωπων στατιστικών δεδομένων (τύπο browser, γεωγραφική τοποθεσία) με σκοπό την βελτιστοποίηση του site, ως μέρος της διαρκούς διαδικασίας εσωτερικής αναθεώρησης και, γενικότερα, με σκοπό να γνωρίσουμε καλύτερα τους καταναλωτές των προϊόντων μας.
                    </p>
                    <p>2. Σκοπός συλλογής
                        Συλλέγουμε στοιχεία για την άμεση εξυπηρέτησή σας (αποστολή παραγγελιών) και για την επικοινωνία μας (ενημέρωση για την παραγγελία σας κτλ). Οι πληροφορίες αυτές δεν πρόκειται να χρησιμοποιηθούν για ανεπιθύμητη συνέχεια της επικοινωνίας, εκτός εάν σας έχουμε προηγουμένως ενημερώσει για την πιθανή αυτή χρήση και εσείς μας δώσετε την άδειά σας. Εάν έχουμε την άδειά σας, η εταιρεία μας μπορεί να χρησιμοποιήσει αυτές τις πληροφορίες για:
                        • Να σας ενημερώνει για προσφορές
                        • Να σας αποστέλλει έντυπα ή άλλη αλληλογραφία
                        • Να σας αποστέλλει ηλεκτρονικώς δελτία τύπου ή άλλες ανακοινώσεις
                        • Να σας παραδίδει προϊόντα ή βραβεία
                    </p>
                    <p>3. Χρόνος αποθήκευσης στοιχείων
                        Αποθηκεύουμε τα στοιχεία σας στο ηλεκτρονικό μας κατάστημα έως ότου μας ζητήσετε (με απλή και εύκολη διαδικασία) να τα διαγράψουμε. Σε άλλη περίπτωση τα στοιχεία παραμένουν στο σύστημά μας για λόγους στατιστικής, αλλά και για την ευκολότερη εξυπηρέτησή σας στις επόμενες αγορές σας. Για οποιαδήποτε πληροφορία σχετικά με τα στοιχεία σας μπορείτε να επικοινωνήσετε μαζί μας.
                    </p>
                    <p>4. Δικαιώματα
                        Η εταιρεία μας αναγνωρίζει τα δικαιώματα του πελάτη, τα οποία περιλαμβάνουν μεταξύ άλλων τη δυνατότητα να αλλάξετε τα στοιχεία σας εφόσον έχετε online λογαριασμό, τη δυνατότητα να διαγράψετε τα προσωπικά σας στοιχεία, τη δυνατότητα αίτησης ώστε να μην λαμβάνετε ενημερωτικά email, τη δυνατότητα για φορητότητα των στοιχείων σας, καθώς και τη δυνατότητα να αντιταχθείτε στη συγκατάθεση και στην επεξεργασία των δεδομένων σας. Συνιστούμε σε παιδιά και νέους ηλικίας κάτω των 18 να λαμβάνουν την άδεια των γονέων τους πριν υποβάλλουν προσωπικά τους στοιχεία στο website.
                    </p>
                    <p>Το website μας λειτουργεί σε ασφαλή περιβάλλον SSL.
                        Για περισσότερες πληροφορίες μη διστάσετε να επικοινωνήσετε μαζί μας.
                    </p>

                </div>
            </div>

        </Layout>
    </div>

PrivacyPage.getInitialProps = async ({ query }) => {

    const url = { query }

    const PAGE = {};

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }

    PAGE.SEO = {
        title: "titlos",
        description: "description",
        keywords: "keywords"
    }


    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                PAGE.data = DATAEL;
                PAGE.TEXTTOP = 'ΣΤΟΙΧΕΙΑ ΕΠΙΚΟΙΝΩΝΙΑΣ'
                PAGE.TEXTBOTTOM = 'ΦΟΡΜΑ ΕΠΙΚΟΙΝΩΝΙΑΣ'
                PAGE.SEO = {
                    title: "titlos EL",
                    description: "description EL",
                    keywords: "keywords EL"
                }
                break;
            default:
        }
    }

    return {postId: PAGE , seo: PAGE.SEO }

}

export default PrivacyPage

