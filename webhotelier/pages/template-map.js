import React, {Component} from 'react'
import Layout from '../components/layout'
import DATA from '../static/WPdataMaps.json'
import DATAEl from '../static/WPdataMaps-el.json'
import SightsSlider from '../components/sightseeings/more-sightseeings'
import WPTaxsEl from '../static/WPdataTaxs-el.json'
import WPTaxs from '../static/WPdataTaxs.json'
import Link from 'next/link'

const MapPage = (props) =>

    <section className={"secondary_bg_color_body"}>
        <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.menu } >

                <section className={"intro-map text-center "}>

                    <div className={"row no-padding-col"}>

                        {props.term.map(function(item,i){


                            let pathname = "/location/" + item.post_slug
                            let itemHref = "/taxonomies"  + "?slug=" + item.post_slug

                            let LINK = <Link href={itemHref } as={ pathname } >
                                <a>{item.post_name}</a>
                            </Link>

                            if( props.lang !== undefined){
                                switch(props.lang) {
                                    case 'el':
                                        pathname = "/el/location/" + item.post_slug
                                        itemHref = "/taxonomies"  + "?slug=" + item.post_slug + "&lang=" + props.lang

                                        LINK = <Link href={itemHref } as={ pathname } >
                                            <a>{item.post_name}</a>
                                        </Link>
                                        break;
                                    default:
                                }
                            }

                            let styles = {
                                background: `url(${item.image})`,
                                height: '350px',
                                backgroundSize: 'cover',
                                backgroundPosition: "center"
                            };

                            let regex = /(<([^>]+)>)/ig;
                            let result = item.description.replace(regex, '');

                            return (
                                <div key={i} className="col-md-12 text-center">
                                    <div className="" style={styles}>

                                    </div>
                                    <div className="primary_text primary_color_body">
                                        <p> {result} </p>
                                    </div>
                                </div>
                            )
                        })}

                    </div>

                </section>

                 <section className={"intro-map text-center "}>
                    <div className={"row no-padding-col "}>

                        <div className="col-md-12 text-center terms primary_color_body no-padding-col">

                            {props.taxTerms.map(function(item,i){

                                let pathname = "/location/" + item.post_slug
                                let itemHref = "/taxonomies"  + "?slug=" + item.post_slug

                                let LINK = <Link href={itemHref } as={ pathname } >
                                    <a>{item.post_name}</a>
                                </Link>

                                if( props.lang !== undefined){
                                    switch(props.lang) {
                                        case 'el':
                                            pathname = "/el/location/" + item.post_slug
                                            itemHref = "/taxonomies"  + "?slug=" + item.post_slug + "&lang=" + props.lang

                                            LINK = <Link href={itemHref } as={ pathname } >
                                                <a>{item.post_name}</a>
                                            </Link>
                                            break;
                                        default:
                                    }
                                }

                                if( item.showTaxonomy === true){
                                    return (
                                        <div key={i} className="this-tax">
                                            {LINK}
                                            <span className="divider">/</span>
                                        </div>
                                    )
                                }else{
                                    return (
                                        <div key={i} className="">
                                            {LINK}
                                            <span className="divider">/</span>
                                        </div>
                                    )
                                }

                            })}
                        </div>

                    </div>

                 </section>

                <section className={"intro-map text-center"}>

                        <div className={"row  no-padding-col rooms_inner"}>
                            <div className={"col-md-12"}>
                                <h2 className={"primary_heading text-center"}>{props.postId.data.page.post_name}</h2>
                                <img src={props.postId.data.page.image} />
                                <div className="description" dangerouslySetInnerHTML={{__html:"<div className='_animation'>" + props.postId.data.page.content  + "</div>"}}>
                                </div>
                                <SightsSlider slides={props.sights} term={props.termSlug} lang={props.lang}/>
                            </div>
                        </div>

                </section>

                <style jsx>{`

                .intro-map{
                    background-color: #eff0f0;
                }
                .terms > div{
                  display: inline-block;
                }

                 .terms{
                    padding: 30px;
                    background: #fff;
                    font-size: 20px;
                }

                .divider{
                    padding-left: 10px;
                }

                .terms > div:nth-last-child(1) .divider {
                    display: none;
                }

                .terms a{
                    display: inline-block;
                    color: #000;
                    text-decoration: none;
                    padding-left: 10px;
                }

                .terms > div{
                   display: inline-block;
                }

                .this-tax a {
                  color: #02b8e3;
                }

              `}</style>

        </Layout>
    </section>

MapPage.getInitialProps = async ({ query}) => {

    const url = { query }

    const PAGE = {};

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }

    PAGE.data = {}

    let GETTERMS = WPTaxs

    let TaxTerms = GETTERMS.map(function(item,i){
        if( item.post_slug === url.query.sight){
            item.showTaxonomy = true
        }else{
            item.showTaxonomy = false
        }
        return item
    })


    let termTaxonomy = "";

    let sightData = DATA

    DATA.map(function(item,i){
        if( item.post_slug === url.query.slug){
            PAGE.data.page = item;
            PAGE.data.tax = WPTaxs.filter(function(tax){
                if( item.taxonomies === tax.post_slug ){
                    termTaxonomy = tax.post_slug
                }
                return  item.taxonomies === tax.post_slug
            })
            PAGE.SEO = {
                title: item.SEO.title,
                description: item.SEO.description,
                keywords: item.SEO.keywords
            }
        }
    })



    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':

                sightData = DATAEl

                GETTERMS = WPTaxsEl
                TaxTerms = GETTERMS.map(function(item,i){
                    if( item.post_slug === url.query.sight){
                        item.showTaxonomy = true
                    }else{
                        item.showTaxonomy = false
                    }
                    return item
                })

                DATAEl.map(function(item,i){
                    if( item.post_slug === url.query.slug){
                        PAGE.data.page = item;
                        PAGE.data.tax = WPTaxsEl.filter(function(tax){
                            if( item.taxonomies === tax.post_slug ){
                                termTaxonomy = tax.post_slug
                                console.log(tax)
                            }
                            return  item.taxonomies === tax.post_slug
                        })
                        PAGE.SEO = {
                            title: item.SEO.title,
                            description: item.SEO.description,
                            keywords: item.SEO.keywords
                        }
                    }
                })


                break;
            default:
        }
    }

    return {postId: PAGE , menu: PAGE.Menu , sights: sightData , term: PAGE.data.tax , termSlug : termTaxonomy , taxTerms: TaxTerms , lang: PAGE.Menu.firstLoad , seo: PAGE.SEO }

}


export default MapPage

