import React, {Component} from 'react'
import Layout from '../components/layout'
import DATA from '../static/WPdataMaps.json'
import DATAEL from '../static/WPdataMaps-el.json'
import WPTaxs from '../static/WPdataTaxs.json'
import WPTaxsEL from '../static/WPdataTaxs-el.json'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'

const TaxPage = (props) =>
    <section className={"secondary_bg_color_body"}>
        <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.menu } >

            <section className={""}>
                <div className={"row"}>
                    <div className="col-md-12">
                        <div className={"row no-padding-col"}>

                            {props.postId.map(function(item,i){

                                let styles = {
                                    background: `url(${item.image})`,
                                    height: '350px',
                                    backgroundSize: 'cover',
                                    backgroundPosition: "center"
                                };

                                let regex = /(<([^>]+)>)/ig;
                                let result = item.description.replace(regex, '');

                                if( item.showTaxonomy === true){
                                    return (
                                        <div key={i} className="col-md-12 text-center">
                                            <div className="" style={styles}>

                                            </div>

                                            <div className="primary_text primary_color_body">
                                                <p> {result} </p>
                                            </div>

                                        </div>
                                    )
                                }

                            })}

                        </div>
                    </div>
                </div>

                <div className={"row "}>
                    <div className="container">

                        <div className={"row "}>

                            <div className="col-md-12 text-center terms primary_color_body no-padding-col">

                                {props.postId.map(function(item,i){

                                    let pathname = "/location/" + item.post_slug
                                    let itemHref = "/taxonomies"  + "?slug=" + item.post_slug

                                    let LINK = <Link href={itemHref } as={ pathname } >
                                        <a>{item.post_name}</a>
                                    </Link>

                                    if( props.lang !== undefined){
                                        switch(props.lang) {
                                            case 'el':

                                                pathname = "/el/location/" + item.post_slug
                                                itemHref = "/el/taxonomies"  + "?slug=" + item.post_slug + "&lang=" + props.lang

                                                LINK = <Link href={itemHref } as={ pathname } >
                                                    <a>{item.post_name}</a>
                                                </Link>
                                                break;
                                            default:
                                        }
                                    }

                                    if( item.showTaxonomy === true){
                                        return (
                                            <div key={i} className="this-tax">
                                                {LINK}
                                                <span className="divider">/</span>
                                            </div>
                                        )
                                    }else{
                                        return (
                                            <div key={i} className="">
                                                {LINK}
                                                <span className="divider">/</span>
                                            </div>
                                        )
                                    }

                                })}
                            </div>

                        </div>
                    </div>
                </div>

                <div className={"container"}>
                    <div className="row">

                        {props.sights.map(function(item,i){

                            let pathname = "/location/" +  item.taxonomies  + "/" + item.post_slug

                            let itemHref = "/template-map" + "?sight=" + item.taxonomies + "&slug=" + item.post_slug

                            let LINK = <Link href={ itemHref } as={ pathname } >
                                <a>READ MORE</a>
                            </Link>

                            if( props.lang !== undefined){
                                switch(props.lang) {
                                    case 'el':

                                        pathname =  "/el/location/" +  item.taxonomies  + "/" + item.post_slug
                                        itemHref = "/el/template-map" + "?sight=" + item.taxonomies + "&slug=" + item.post_slug + "&lang=" + props.lang

                                        LINK = <Link href={itemHref } as={ pathname } >
                                            <a>ΠΕΡΙΣΣΟΤΕΡΑ</a>
                                        </Link>
                                        break;
                                    default:
                                }
                            }

                            let styles = {
                                background: `url(${item.image})`,
                                height: '350px',
                                backgroundSize: 'cover',
                                backgroundPosition: "center"
                            };

                            let regex = /(<([^>]+)>)/ig;
                            let result = item.content.replace(regex, '');

                            return (
                                <div key={i} className="col-md-6">
                                    <div className="single-map-container">
                                        <div className="image-map" style={styles}></div>
                                        <div className="name-map title_head ">{item.post_name}</div>
                                        <div className="content-map">
                                            <LinesEllipsis
                                                className="text"
                                                text= {result}
                                                maxLine={5}
                                                ellipsis='...'
                                                trimRight
                                                basedOn='letters'
                                            />
                                        </div>
                                        <div className="read-map btns">{LINK}</div>
                                    </div>
                                </div>
                            )
                        })}

                    </div>

                </div>

            </section>


            <style jsx>{`

            .intro-map{
                background-color: #eff0f0;
            }

            .single-map-container{
               margin: 20px 0;
               box-shadow: 0px 0px 20px #000;
               padding: 20px;
            }

            .read-map{
                margin-top: 20px;
            }

            .read-map a{
                color: #fff;
                text-decoration: none;
            }

            .content-map{
               min-height: 120px;
             }

            .read-map a{
                color: #fff;
                text-decoration: none;
            }

            .title_head {
               margin: 20px 0;
               min-height: 60px;
            }


            .terms > div{
              display: inline-block;
            }

             .terms{
                padding: 30px;
                background: #fff;
                font-size: 20px;
            }

            .divider{
                padding-left: 10px;
            }

            .terms > div:nth-last-child(1) .divider {
                display: none;
            }

            .terms a{
                display: inline-block;
                color: #000;
                text-decoration: none;
                padding-left: 10px;
            }

            .terms > div{
               display: inline-block;
            }

            .this-tax a {
              color: #02b8e3;
            }

          `}</style>


        </Layout>
    </section>

TaxPage.getInitialProps = async ({ query}) => {

    const url = { query }

    const PAGE = {};

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }

    PAGE.data = {}

    let GETDATA = DATA
    let GETTERMS = WPTaxs

    let TaxMaps = GETDATA.filter(function(item,i){
        if( item.taxonomies === url.query.slug){

            return item
        }
    })

    let TaxTerms = GETTERMS.map(function(item,i){
        if( item.post_slug === url.query.slug){
            item.showTaxonomy = true
            PAGE.SEO = {
                title: item.post_name,
                description: item.description,
                keywords: item.post_name
            }
        }else{
            item.showTaxonomy = false
        }
        return item
    })

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                TaxMaps = DATAEL.filter(function(item,i){
                    if( item.taxonomies === url.query.slug){

                        return item
                    }
                })

                TaxTerms = WPTaxsEL.map(function(item,i){
                    if( item.post_slug === url.query.slug){
                        item.showTaxonomy = true
                        PAGE.SEO = {
                            title: item.post_name,
                            description: item.description,
                            keywords: item.post_name
                        }
                    }else{
                        item.showTaxonomy = false
                    }

                    return item
                })
                break;
            default:
        }
    }

    return { postId: TaxTerms , menu: PAGE.Menu , sights: TaxMaps , lang: PAGE.Menu.firstLoad , seo: PAGE.SEO }

}


export default TaxPage

