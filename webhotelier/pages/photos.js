import React, {Component} from 'react'
import Layout from '../components/layout'
import FullSlider from '../components/gallery_full_slider'
import Link from 'next/link'
import MoreGalleriesList from '../components/more_galleries_list'
import DATA from '../static/WPdataGalleries.json'
import DATAEL from '../static/WPdataGalleries.json'

const PhotosPage = (props) =>

    <div>
        <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.postId.Menu } >

            <section className={"more_gallery_imgs"}>

                <div className={"row no-padding-col"}>

                    <div className={"col-md-12"}>

                        <div className="name_gallery">{props.postId.data.page.post_name}</div>

                        <FullSlider images={props.postId.data.page.gallery} />

                        <div className="select_more_galleries">
                            <MoreGalleriesList galleries={props.postId.Show_photos} lang={props.lang}/>
                        </div>

                        <div className="gotoPhotos">
                            <Link href={props.postId.LINKS.XnewUrl} as={props.postId.LINKS.Xpathname}>
                                <a>X</a>
                            </Link>
                       </div>

                    </div>
                </div>

            </section>

        </Layout>
    </div>

PhotosPage.getInitialProps = async ({ query }) => {

    let url = { query }

    const PAGE = {};

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng,
        slug: url.query.slug,
        room: url.query.roomName
    }

    PAGE.sliderSettings = {
        dots: false,
        fade: true,
        infinite: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 500,
        accessibility:false,
        touchMove:false,
        slidesToShow: 1,
        slidesToScroll: 1
    }

    PAGE.data = {}
    PAGE.Show_photos = []
    PAGE.LINKS = {}

    PAGE.LINKS.Xpathname = '/galleries'
    PAGE.LINKS.XnewUrl = '/galleries&lng=en';


    DATA.map(function(item){
        if( item.post_slug === url.query.slug ){
            PAGE.data.page = item;
            PAGE.SEO = {
                title: item.SEO.title,
                description: item.SEO.description,
                keywords: item.SEO.keywords
            }
        }
        PAGE.Show_photos.push({slug:item.post_slug, name: item.post_name})
    })

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                PAGE.Show_photos = [];
                DATAEL.map(function(item){
                    if( item.post_slug === url.query.slug ){
                        PAGE.data.page = item;
                        PAGE.SEO = {
                            title: item.SEO.title,
                            description: item.SEO.description,
                            keywords: item.SEO.keywords
                        }
                    }
                    PAGE.Show_photos.push({slug:item.post_slug, name: item.post_name})
                })

                PAGE.LINKS.Xpathname = '/el/galleries'
                PAGE.LINKS.XnewUrl = '/galleries&lng=el';

                break;
            default:
        }
    }

    return {postId: PAGE , seo: PAGE.SEO , lang: PAGE.Menu.firstLoad}

}

export default PhotosPage

