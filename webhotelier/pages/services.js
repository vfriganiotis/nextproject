import React, {Component} from 'react'
import Layout from '../components/layout'
import ServicesComponent from '../components/services/services_component'
import DATA from '../static/WPdataServices.json'
import DATAEL from '../static/WPdataServices-el'
import PAGEDATA from '../static/pages/services.json'
import PAGEDATAEL from '../static/pages/services-el.json'

const Services = (props) =>

    <div>

        <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.postId.Menu } >

            <div className="primary-bg-color row no-padding">

                <div className="col-md-12">
                    <ServicesComponent  services={props.postId.data.page}/>
                </div>
                <div className="col-md-12">
                    <div className="bg-hotel" style={{ background: props.postId.data.services.overview_background_color }}>
                        <h1 className="primary_heading">{props.postId.data.services.overview_text}</h1>
                    </div>
                </div>

            </div>

            <style jsx>{`

                  .bg-hotel{
                     color: ${props.postId.data.services.overview_color};
                     padding: 150px;
                     text-align: center;
                  }

            `}</style>

        </Layout>

    </div>

Services.getInitialProps = async ({  query  }) => {


    let url = { query }

    const PAGE = {};

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }

    PAGE.data = {
        page : DATA,
        services: PAGEDATA
    }

    PAGE.SEO = {
        title: PAGEDATA.SEO.title,
        description: PAGEDATA.SEO.description,
        keywords: PAGEDATA.SEO.keywords
    }

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                PAGE.data.page = DATAEL;
                PAGE.data.serices = PAGEDATAEL;
                PAGE.SEO = {
                    title: PAGEDATAEL.SEO.title,
                    description: PAGEDATAEL.SEO.description,
                    keywords: PAGEDATAEL.SEO.keywords
                }
                break;
            default:
        }
    }

    return {postId: PAGE, seo: PAGE.SEO}

}

export default Services

