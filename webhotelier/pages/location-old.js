import React, {Component} from 'react'
import Layout from '../components/layout'
import GoogleMapReact from 'google-map-react'
import WPPages from '../static/pages/location.json'
import WPMaps from '../static/WPdataMaps.json'
import WPTaxs from '../static/WPdataTaxs.json'
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'

export default class extends Component {

    static async getInitialProps({ query }) {

        const url = { query }

        const PAGE = {};

        PAGE.Menu = {
            firstLoad : url.query.lang || url.query.lng,
            lang : url.query.lng
        }

        let WPPAGES = WPPages
        let WPMAPS = WPMaps
        let TAXS = WPTaxs

        if( PAGE.Menu.firstLoad !== undefined){
            switch(PAGE.Menu.firstLoad) {
                case 'el':
                    WPPAGES = WPPages
                    WPMAPS = WPMaps
                    break;
                default:
            }
        }

        return {postId: WPPAGES, maps: WPMAPS , menu: PAGE.Menu , taxes: TAXS }
    }

    renderMarkers(map, maps) {

        let marker = new maps.Marker({
            position: {
                lat: parseFloat(this.props.postId.google_map_lat),
                lng: parseFloat(this.props.postId.google_map_lng)
            },
            map
        });

    }

    render(){

        const mapCenterOpt = {
            center: {
                lat: parseFloat(this.props.postId.google_map_lat),
                lng: parseFloat(this.props.postId.google_map_lng)
            },
            zoom: 12
        };

        return (
            <Layout title={"location"} description={"location"} keywords={"key,words"}  menu={ this.props.menu } >
                <div style={{ height: '100vh', width: '100%' }}  className='_animation'>
                    <GoogleMapReact
                        options={{gestureHandling: 'greedy' ,scrollwheel: false}}
                        bootstrapURLKeys={{ key: 'AIzaSyAweofWPFAadH7psVjPjL44Vm7135bjgac' }}
                        defaultCenter={mapCenterOpt.center}
                        defaultZoom={mapCenterOpt.zoom}
                        defaultScrollwheel={mapCenterOpt.scrollwheel}
                        // defaultIcon={mapCenterOpt.icon}
                        gestureHandling={'greedy'}
                        onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}
                    >
                    </GoogleMapReact>
                </div>

                <div className="container">
                    <div className="row">
                        <div className="col-md-12 terms">
                            {this.props.taxes.map(function(item,i){

                                let pathname = "/location/" + item.post_slug
                                let itemHref = "/taxonomies"  + "?slug=" + item.post_slug

                                let link =  <Link href={itemHref } as={ pathname } >
                                    <a>{item.post_name}</a>
                                </Link>

                                return (
                                    <div>
                                        {link}
                                    </div>
                                )

                            })}
                        </div>
                    </div>
                    <div className="row">

                        {this.props.maps.map(function(item,i){

                            let pathname = "/location/" +  item.taxonomies  + "/" + item.post_name.split(" ").join('-').toLowerCase()

                            let itemHref = "/template-map" + "?sight=" + item.taxonomies + "&slug=" + item.post_name.split(" ").join('-').toLowerCase()

                            //let link = <a href={itemHref}>READ MORE</a>

                            let link = <Link href={ itemHref } as={ pathname } >
                                    <a>READ MORE</a>
                            </Link>

                            let styles = {
                                background: `url(${item.image})`,
                                height: '350px',
                                backgroundSize: 'cover',
                                backgroundPosition: "center"
                            };


                            let regex = /(<([^>]+)>)/ig;
                            let result = item.content.replace(regex, '');

                            return (
                                <div key={i} className="col-md-6">
                                    <div className="single-map-container">
                                        <div className="image-map" style={styles}></div>
                                        <div className="name-map title_head ">{item.post_name}</div>
                                        <div className="content-map">
                                            <LinesEllipsis
                                                className="text"
                                                text= {result}
                                                maxLine={5}
                                                ellipsis='...'
                                                trimRight
                                                basedOn='letters'
                                            />
                                        </div>
                                        <div className="read-map btns">{link}</div>
                                    </div>
                                </div>
                            )
                        })}

                    </div>

                    <style jsx>{`

                    .single-map-container{
                       margin: 20px 0;
                       box-shadow: 0px 0px 20px #000;
                       padding: 20px;
                    }

                    .read-map{
                        margin-top: 20px;
                     }

                    .read-map a{
                        color: #fff;
                        text-decoration: none;
                    }

                    .title_head {
                       margin: 20px 0;
                    }

                    .terms{
                        color: #000;
                        text-align: center;
                        border: 1px solid;
                        margin-top: 40px;
                        margin-bottom: 20px;
                        background: #f2f2f2;
                    }

                    .terms a{
                        display: inline-block;
                        color: #000;
                        text-decoration: none;
                        padding: 20px;
                    }

                    .terms > div{
                       display: inline-block;
                    }

                  `}</style>

                </div>

            </Layout>
        )
    }
}
