import React, {Component} from 'react'
import Layout from '../components/layout'
import GalleriesComponent from '../components/galleries_component'
import DATAEL from "../static/WPdataGalleries.json";
import DATA from "../static/WPdataGalleries.json";
import PAGESEOEL from "../static/pages/gallery-el.json";
import PAGESEO from "../static/pages/gallery.json";

const PhotoGallery = (props) =>
    <div>
        <Layout title={props.seo.title} description={props.seo.description} keywords={props.seo.keywords} menu={ props.postId.Menu } >

            <section>
                <div className="photo_galeries_cont">
                    <GalleriesComponent
                        galleries={props.postId.data.page}
                        language={props.postId.Menu.firstLoad}
                        count={10}
                        locale={props.locale.loadGalleries}
                        localeCategory={props.locale.gallery}
                    />
                </div>
            </section>

        </Layout>
    </div>

PhotoGallery.getInitialProps = async ({ query }) => {

    let url = { query }

    const PAGE = {};

    PAGE.Menu = {
        firstLoad : url.query.lang || url.query.lng,
        lang : url.query.lng
    }

    PAGE.data = {
        page : DATA
    }


    PAGE.SEO = {
        title: PAGESEO.SEO.title,
        description: PAGESEO.SEO.description,
        keywords: PAGESEO.SEO.keywords,
    }

    PAGE.LOCALE = {
        gallery: "Choose Our Category",
        loadGalleries: "LOAD MORE GALLERIES"
    }

    if( PAGE.Menu.firstLoad !== undefined){
        switch(PAGE.Menu.firstLoad) {
            case 'el':
                PAGE.data.page = DATAEL;
                PAGE.SEO = {
                    title: PAGESEOEL.SEO.title,
                    description: PAGESEOEL.SEO.description,
                    keywords: PAGESEOEL.SEO.keywords,
                }

                PAGE.LOCALE = {
                    gallery: "Διαλέξτε μια κατηγορία",
                    loadGalleries: "Φωρτοστέ περισσότερα"
                }
                break;
            default:
            // code block
        }
    }

    return {postId: PAGE , seo: PAGE.SEO , locale: PAGE.LOCALE}
}

export default PhotoGallery

