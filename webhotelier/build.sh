#!/usr/bin/env bash

if [[ $1 == "dev" ]];
	then
	    mkdir static
	    mkdir static/large
	    mkdir static/rooms
	    mkdir static/logo
	    cd static && touch data.json
	    echo "file json created"
	    cd ../

        node cronjob.js

	echo "$1 created run yarn dev"

elif [[ $1 == "prod" ]];
	then
	rm -rf static/large
	rm -rf static/rooms
	rm -rf static/data.json
	rm -rf static/roomsData.json

echo "$1 environment created"

else
echo "Parameter not right."
fi

