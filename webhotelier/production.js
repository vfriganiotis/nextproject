const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const Recaptcha = require('recaptcha-v2').Recaptcha;
const cors = require('cors')

require('dotenv').config()

const port = process.env.PORT
const MAIL_USER  = process.env.MAIL_USERNAME

//express
const server = express()

//mailer
const transporter = require('./mailer')

//Cors
server.use( cors() );

//Compression
server.use(compression())

//Body Parser
server.use( bodyParser.json() );
server.use(bodyParser.urlencoded({
    extended: true
}));

//OLD SITE REDIRECTS
const redirects = [
    { from: '/contact.php', to: '/contact' },
    { from: '/gr/contact.php', to: '/el/contact' },
]

//Redirects
redirects.forEach(({ from, to, type = 301, method = 'get' }) => {
    server[method](from, (req, res) => {
        res.redirect(type, to)
    })
})

//Redirects
server.get('/gr', function(req, res) {

    res.redirect('/el');
});

server.get('/gr/:page/', function(req, res) {

    let newUrl = '/el/' +  req.params.page
    res.redirect(newUrl);
});

server.get('/gr/:page/:subpage', function(req, res) {

    let newUrl = '/el/' +  req.params.page + '/' + req.params.subpage
    res.redirect(newUrl);
});

server.get('/gr/:page/:subpage/:postpage', function(req, res) {

    let newUrl = '/el/' +  req.params.page + '/' + req.params.subpage + '/' + req.params.postpage
    res.redirect(newUrl);
});

//Send Mail
server.post('/sendMail', (req, res, next) => {

    let name = req.body.name
    let email = req.body.email
    let message = req.body.message

    let recapth = req.body.recapthca

    console.log("ewewewewew")
    console.log(message)

    let content = `name: ${name} \n email: ${email} \n message: ${message} `

    let mail = {
        from: name,
        to: MAIL_USER,
        subject: 'New Message from Contact Form',
        text: content
    }

    const recaptchaData = {
        remoteip: req.connection.remoteAddress,
        response: recapth,
        secret: process.env.RECAPTCHA_SECRET,
    };

    const recaptcha = new Recaptcha(process.env.RECAPTCHA, process.env.RECAPTCHA_SECRET, recaptchaData);

    recaptcha.verify((success) => {

        if (success) {

            transporter.sendMail(mail, (err, data) => {

                if (err) {
                    res.json({
                        msg: 'fail'
                    })
                } else {
                    res.json({
                        msg: 'success'
                    })
                }
            })

        }else{

            res.json({
                msg:  'fail'
            })

        }
    })

})

//Refresh Cache
server.get('/wp-api', (req, res) => {

    console.log("meta to reset")
    const shell = require('./child_helper');

    const commandList = [
        "npm run refresh"
    ]

    shell.series(commandList , (err)=>{
        if(err){
            console.log(err)
        }
        console.log('cron run')
    });

});

server.use(
    express.static(__dirname + '/out', {
        maxage: '365d'
    })
);

server.listen(port, (err) => {
    console.log( process.env.SECRET_KEY )
    console.log(`> Ready on 1 http://localhost:${port}`)
})


