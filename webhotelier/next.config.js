const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const withCSS = require('@zeit/next-css')
require('dotenv').config()

module.exports = withCSS({
    exportPathMap: function (defaultPathMap) {

        const postList = require('./static/WPdataRooms')
        const postList1 = require('./static/WPdataRooms-el')
        const gallList = require('./static/WPdataGalleries')
        const mapsList = require('./static/WPdataMaps')
        const mapsListEL = require('./static/WPdataMaps-el')
        const taxsList = require('./static/WPdataTaxs')
        const taxsListEL = require('./static/WPdataTaxs-el')

        const taxs = taxsList.reduce((pages, post) =>
            Object.assign({}, pages, {
                [`/location/${post.post_slug}`]: {
                    page: '/taxonomies',
                    query: {slug: post.post_slug, lang:' '}
                }
            }), {})


        const maps = mapsList.reduce((pages, post) =>
            Object.assign({}, pages, {
                [`/location/${post.taxonomies}/${post.post_slug}`]: {
                    page: '/template-map',
                    query: {slug: post.post_slug,lang:' '}
                }
            }), {})

        const rooms = postList.reduce((pages, post) =>
            Object.assign({}, pages, {
                [`/room/${post.room_name.split(' ').join('-').toLowerCase()}`]: {
                    page: '/room',
                    query: {roomslug: post.room_name.split(' ').join('-').toLowerCase(),lang:''}
                }
            }), {})


        const galleries = gallList.reduce((pages, post) =>
            Object.assign({}, pages, {
                [`/photos/${post.post_slug}`]: {
                    page: '/photos',
                    query: {slug:post.post_slug,lang:' '}
                }
            }), {})

        const elgalleries = gallList.reduce((pages, post) =>
            Object.assign({}, pages, {
                [`/el/photos/${post.post_slug}`]: {
                    page: '/photos',
                    query: {slug:post.post_slug,lang:'el'}
                }
            }), {})


        const eltaxs = taxsListEL.reduce((pages, post) =>
            Object.assign({}, pages, {
                [`/el/location/${post.post_slug}`]: {
                    page: '/taxonomies',
                    query: {slug: post.post_slug, lang:'el'}
                }
            }), {})

        const elrooms  = postList1.reduce((pages, post) =>
            Object.assign({}, pages, {
                [`/el/room/${post.room_name.split(' ').join('-').toLowerCase()}`]: {
                    page: '/room',
                    query: {roomslug: post.room_name.split(' ').join('-').toLowerCase(),lang:'el'}
                }
            }), {})

        const elmaps = mapsListEL.reduce((pages, post) =>
            Object.assign({}, pages, {
                [`/el/location/${post.taxonomies}/${post.post_slug}`]: {
                    page: '/template-map',
                    query: {slug: post.post_slug,lang:'el'}
                }
            }), {})



        let newArr = {};

        const loop=(pagesArr=>{
            pagesArr.map(obj=>{
                for(let item in obj){
                    newArr[item] = obj[item]
                }
            })
        })

        // loop([rooms,galleries,maps,taxs])
        loop([rooms,elrooms,galleries,elgalleries,maps,elmaps,taxs,eltaxs])
        console.log(newArr)
        // combine the map of post pages with the home
        return Object.assign({},newArr, {
            '/': {page: '/'},
            '/accommodation': {page: '/accommodation', query: { lang: ' ' } },
            '/services': {page: '/services', query: { lang: ' ' } },
            '/location': {page: '/location', query: { lang: ' ' } },
            '/galleries': {page: '/galleries', query: { lang: ' ' } },
            '/reviews': {page: '/reviews', query: { lang: ' ' } },
            '/contact': {page: '/contact', query: { lang: ' ' } },
            '/el': {page: '/', query: { lang: 'el' } },
            '/el/accommodation': {page: '/accommodation', query: { lang: 'el' } },
            '/el/services': {page: '/services', query: { lang: 'el' } },
            '/el/location': {page: '/location', query: { lang: 'el' } },
            '/el/galleries': {page: '/galleries', query: { lang: 'el' } },
            '/el/reviews': {page: '/reviews', query: { lang: 'el' } },
            '/el/contact': {page: '/contact', query: { lang: 'el' } }

        })

    },
    cssModules: true,
    publicRuntimeConfig: {
        NEXT_URL: process.env.NEXT_SOURCE,
        WP_URL: process.env.WP_SOURCE,
        MAIL_USER: process.env.MAIL_USERNAME,
        MAIL_PASS:  process.env.MAIL_USERPASS,
        RECAPTCHA_KEY: process.env.RECAPTCHA
    },
    webpack: (config) => {

        // const originalEntry = config.entry;
        // config.entry = async () => {
        //     const entries = await originalEntry();
        //     if (entries['main.js']) {
        //         entries['main.js'].unshift('./static/polyfill.js');
        //     }
        //     return entries;
        // };

        config.plugins.push(
            new SWPrecacheWebpackPlugin({
                verbose: true,
                staticFileGlobsIgnorePatterns: [/\.next\//],
                cacheId: 'my-sw',
                filename: 'service-worker.js',
                runtimeCaching: [
                    {
                        handler: 'networkFirst',
                        urlPattern: /^https?.*/
                    }
                ],
                staticFileGlobs: [
                    'static/*.{js,html,css,png,json,jpg,gif,svg,eot,ttf,woff,ico}',
                    'static/fonts/*.{js,html,css,png,json,jpg,gif,svg,eot,ttf,woff,ico}',
                    'static/themeOne/*.{js,html,css,png,json,jpg,gif,svg,eot,ttf,woff,ico}',
                    // 'static/large/*.{js,html,css,png,json,jpg,gif,svg,eot,ttf,woff,ico}',
                    // 'static/rooms/**/*.{js,html,css,png,json,jpg,gif,svg,eot,ttf,woff,ico}'
                ],
                minify: true
            }),

            // new BundleAnalyzerPlugin()

        )

        // config.module.rules.push({
        //     test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
        //     use: {
        //         loader: 'url-loader',
        //         options: {
        //             limit: 100000,
        //             name: '[name].[ext]'
        //         }
        //     }
        // })

        return config
    }
})

