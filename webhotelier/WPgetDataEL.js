const fs =require('fs');
let request = require('request');
require('dotenv').config()

//WP
let WPusername = process.env.WP_USER,
    WPpassword = process.env.WP_PASS,
    WPurl = "https://" + process.env.WP_SOURCE +"/el/wp-json/wp/v2/rooms?_embed&filter[orderby]=date&order=asc",
    WPpages = "https://" + process.env.WP_SOURCE +"/el/wp-json/wp/v2/pages/",
    WPFieldsUrl = "https://" + process.env.WP_SOURCE + "/el/wp-json/acf/v3/rooms?_embed&filter[orderby]=date&order=asc",
    WPFieldsGalleries = "https://" + process.env.WP_SOURCE + "/el/wp-json/wp/v2/galleries?_embed&filter[orderby]=date&order=asc",
    WPservices = "https://" + process.env.WP_SOURCE + "/el/wp-json/wp/v2/services?_embed&filter[orderby]=date&order=asc",
    WPImages = "https://" + process.env.WP_SOURCE + "/el/wp-json/wp/v2/media/",
    WPmenu = "https://" + process.env.WP_SOURCE + "/el/wp-json/myroutes/menu",
    WPoptions = "https://" + process.env.WP_SOURCE + "/el/wp-json/acf/v3/options/options",
    WPreviews = "https://" + process.env.WP_SOURCE + "/el/wp-json/wp/v2/reviews?_embed&filter[orderby]=date&order=asc&per_page=100",
    WPtaxonomy = "https://" + process.env.WP_SOURCE + "/el/wp-json/wp/v2/sightseeings?_embed&per_page=100",
    WPmaps = "https://" + process.env.WP_SOURCE + "/el/wp-json/wp/v2/maps?_embed&per_page=100",
    WPauth = "Basic " + new Buffer(WPusername + ":" + WPpassword).toString("base64");

let homepage = {
    rooms : [],
    galleriesData: [],
    lat: "",
    lng: "",
    locationText: ""
}

function REQUESTpages(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Create
            if (!error && response.statusCode === 201) {

                //Gallery Media
                if( typeof body !== 'object'){

                    let info = JSON.parse(body)
                    let uroom =  url.formData.file[0].path.split('/')
                    WPHotelImagesId.push(info.id)

                }else{
                    //CREATE ROOM
                    console.log("kanamme create page")

                }

            }else{
                console.log(error)
            }

            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){
                    let info = JSON.parse(body)

                    console.log(body)

                    info.map(function(item){

                        switch(item.slug) {
                            case 'services':

                                let facilities = {
                                    "select_pop_up": item.acf.select_pop_up,
                                    "background_color": item.acf.background_color,
                                    "overview_text": item.acf.overview_text,
                                    "overview_background_color": item.acf.overview_background_color,
                                    "overview_color": item.acf.overview_color,
                                    "id": item.id,
                                    "SEO": {
                                        "title": item.acf.title,
                                        "description": item.acf.description,
                                        "keywords": item.acf.keywords
                                    }
                                }

                                fs.writeFile('./static/pages/services-el.json', JSON.stringify(facilities), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });

                                break;
                            case 'reviews':

                                let reviews = {
                                    "SEO": {
                                        "title": item.acf.title,
                                        "description": item.acf.description,
                                        "keywords": item.acf.keywords
                                    }
                                }

                                if( item.acf.facebook !== false){
                                    reviews.facebook = {
                                        "people": item.acf.facebook[0].people,
                                        "score" : item.acf.facebook[0].scores
                                    }
                                }

                                if( item.acf.booking !== false){
                                    reviews.booking = {
                                        "people": item.acf.booking[0].people,
                                        "score" : item.acf.booking[0].scores
                                    }
                                }

                                if( item.acf.tripadvisor !== false){

                                    reviews.tripadvisor = {
                                        "people": item.acf.tripadvisor[0].people,
                                        "score" : item.acf.tripadvisor[0].scores
                                    }

                                }

                                if( item.acf.hotels !== false){

                                    reviews.hotels = {
                                        "people": item.acf.hotels[0].people,
                                        "score" : item.acf.hotels[0].scores
                                    }

                                }

                                fs.writeFile('./static/pages/reviews-el.json', JSON.stringify(reviews), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            case 'galleries':

                                let gallery = {
                                    "SEO": {
                                        "title": item.acf.title,
                                        "description": item.acf.description,
                                        "keywords": item.acf.keywords
                                    }
                                }

                                fs.writeFile('./static/pages/gallery-el.json', JSON.stringify(gallery), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            case 'homepage':

                                let overviewArr = [];
                                let gallerieArr = [];
                                let whyChooseArr = [];
                                let slogansArr = [];
                                let reviewsArr = [];

                                if( item.acf.why_choose_us_content.length >= 0 ){
                                    whyChooseArr = item.acf.why_choose_us_content.map(function(overview){
                                        return overview.text
                                    })
                                }

                                if( item.acf.overview_content.length >= 0 ){
                                    overviewArr = item.acf.overview_content.map(function(facilitie){
                                        return facilitie.text
                                    })
                                }

                                if( item.acf.main_slider_images.length >= 0 ){
                                    gallerieArr = item.acf.main_slider_images.map(function(gallerie){
                                        return gallerie.image.url
                                    })
                                }

                                if( item.acf.rooms_and_layouts.length >= 0 ){
                                    slogansArr = item.acf.rooms_and_layouts.map(function(overview){
                                        return {
                                            text : overview.text,
                                            acf_fc_layout :  overview.acf_fc_layout,
                                            background_color : overview.background_color,
                                            color : overview.color
                                        }
                                    })
                                }

                                if( item.acf.reviews.length >= 0 ){
                                    reviewsArr = item.acf.reviews.map(function(review){
                                        return {
                                            heading: review.heading,
                                            content:  review.content,
                                            link: review.link
                                        }
                                    })
                                }


                                if( item.acf.overview_content.length >= 0 ){
                                    overviewArr = item.acf.overview_content.map(function(facilitie){
                                        return facilitie.text
                                    })
                                }

                                homepage.home = {
                                    "main_slider_heading": item.acf.main_slider_heading,
                                    "main_slider_content": item.acf.main_slider_content,
                                    "main_slider_images": gallerieArr,
                                    "overview_color": item.acf.overview_color,
                                    "overview": item.acf.overview,
                                    "overview_content": overviewArr,
                                    "why_choose_us": item.acf.why_choose_us,
                                    "why_choose_us_content": whyChooseArr,
                                    "book_now": item.acf.book_now,
                                    "reviews": reviewsArr,
                                    "rooms_and_layouts": slogansArr,
                                    "reviews_bg_color": item.acf.reviews_bg_color,
                                    "SEO": {
                                        "title": item.acf.title,
                                        "description": item.acf.description,
                                        "keywords": item.acf.keywords
                                    }
                                }

                                break;
                            case 'location'://CHANGE WITH LOCATION

                                let overviewCArr = [];
                                let choose_usArr = [];
                                let select_sightseeingsArr = [];


                                if( item.acf.loverview_content.length >= 0 ){
                                    overviewCArr = item.acf.loverview_content.map(function(overview){
                                        return overview.text
                                    })
                                }

                                if( item.acf.why_choose_us_content.length >= 0 ){
                                    choose_usArr = item.acf.why_choose_us_content.map(function(choose){
                                        return choose.text
                                    })
                                }

                                if( item.acf.select_sightseeings.length >= 0 ){
                                    select_sightseeingsArr = item.acf.select_sightseeings.map(function(sight){
                                        return sight.sightseeing.ID
                                    })
                                }

                                homepage.lat = item.acf.google_map_lat;
                                homepage.lng = item.acf.google_map_lng;
                                homepage.locationText = overviewCArr[0];

                                let location = {
                                    "overview_color": item.acf.overview_color,
                                    "loverview": item.acf.loverview,
                                    "loverview_content": overviewCArr,
                                    "why_choose_us": item.acf.why_choose_us,
                                    "why_choose_us_content": choose_usArr,
                                    "book_now": item.acf.book_now,
                                    "select_sightseeings": select_sightseeingsArr,
                                    "google_map_lat": item.acf.google_map_lat,
                                    "google_map_lng": item.acf.google_map_lng,
                                    "icon": item.acf.icon,
                                    "location": item.acf.location,
                                    "other_icon_google_map": item.acf.other_icon_google_map,
                                    "select_pop_up": item.acf.select_pop_up,
                                    "SEO": {
                                        "title": item.acf.title,
                                        "description": item.acf.description,
                                        "keywords": item.acf.keywords
                                    }
                                }

                                fs.writeFile('./static/pages/location-el.json', JSON.stringify(location), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            case 'accommodation':
                                let accommodation = {
                                    "background_Hero": item.acf.background_Hero.url,
                                    "SEO": {
                                        "title": item.acf.title,
                                        "description": item.acf.description,
                                        "keywords": item.acf.keywords
                                    }
                                }

                                fs.writeFile('./static/pages/accommodation-el.json', JSON.stringify(accommodation), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            case 'contact':

                                let telsArr = [];

                                if( item.acf.telephone.length >= 0 ){
                                    telsArr  = item.acf.telephone.map(function(tel){
                                        return tel.phone_number
                                    })
                                }

                                let contact = {
                                    "location": item.acf.location,
                                    "telephone": telsArr,
                                    "fax_number": item.acf.fax_number,
                                    "mail":  item.acf.mail,
                                    "select_pop_up": item.acf.select_pop_up,
                                    "SEO": {
                                        "title": item.acf.title,
                                        "description": item.acf.description,
                                        "keywords": item.acf.keywords
                                    }
                                }

                                homepage.contactLocation = item.acf.location;
                                homepage.contactTelephone = item.acf.telephone[0].phone_number;
                                homepage.contactMail = item.acf.mail;

                                fs.writeFile('./static/pages/contact-el.json', JSON.stringify(contact), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            default:
                                console.log("PAGE NOT FOUND")
                        }

                    })

                    fs.writeFile('./static/WPdataPages-el.json', JSON.stringify(body), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

function REQUESTMenu(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)

            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){
                    let info = JSON.parse(body)

                    let menu = []


                    info.map(function(item){

                        let obj = {
                            ID : "",
                            title: "",
                            item_lang: "",
                            menu_item_parent: "",
                            menu_order: ""
                        }

                        switch(item.url.split('/').length) {
                            case 6:
                                obj.slug = ''
                                break;
                            case 7:
                                obj.slug = item.url.split('/')[5]
                                break;
                            default:
                                obj.slug = item.url.split('/')[6]
                        }

                        if( item.url.split('/').length > 0 ){

                            console.log(item.url.split('/'))
                            console.log(item.url.split('/').length)
                        }

                        obj.ID = item.ID;
                        obj.title = item.title;
                        obj.item_lang = item.item_lang;
                        obj.menu_item_parent = item.menu_item_parent;
                        obj.menu_order = item.menu_order;
                        menu.push(obj)

                    })

                    fs.writeFile('./static/menu-el.json', JSON.stringify(menu), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

function REQUESTrooms(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Create
            if (!error && response.statusCode === 201) {

                //Gallery Media
                if( typeof body !== 'object'){

                    let info = JSON.parse(body)
                    let uroom =  url.formData.file[0].path.split('/')
                    WPHotelImagesId.push(info.id)

                }else{
                    //CREATE ROOM
                    console.log("kanamme create page")

                }

            }else{
                console.log(error)
            }

            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){
                    let info = JSON.parse(body)

                    let rooms = []

                    info.map(function(item){

                        let urlJson = './static/wprooms/'+ item.slug +'.json'
                        let main_slider_imagesArr, hightligtsArr;

                        if( item.acf.main_slider_images.length >= 0 ){
                            main_slider_imagesArr = item.acf.main_slider_images.map(function(image){
                                return image.url
                            })
                        }

                        if( item.acf.hightligts.length >= 0 ){
                            hightligtsArr = item.acf.hightligts.map(function(hightligts){
                                console.log( hightligts)
                                console.log( hightligts['icon-hightligt'].url)
                                return {
                                    icon : hightligts['icon-hightligt'].url,
                                    text : hightligts['text-hightligt']
                                }
                            })
                        }

                        let FeatImg = ""
                        if( item.acf.featured_image !== undefined){
                            FeatImg = item.acf.featured_image.url
                        }

                        let page = {
                            "room_id": item.id,
                            "room_name" : item.title.rendered,
                            "main_slider_images": main_slider_imagesArr,
                            "overview": item.acf.overview,
                            "facilities_heading": item.acf.facilities_heading,
                            "facilities": item.acf.facilities,
                            "up_to": item.acf.up_to,
                            "book_now": item.acf.book_now,
                            "featured_image": FeatImg,
                            "excerpt": item.acf.excerpt,
                            "hightligts": hightligtsArr,
                            "select_pop_up": item.acf.select_pop_up,
                            "SEO": {
                                "title": item.acf.title,
                                "description": item.acf.description,
                                "keywords": item.acf.keywords
                            }
                        }

                        rooms.push(page)

                        homepage.rooms.push(page)

                        fs.writeFile(urlJson, JSON.stringify(page), 'utf8',(err)=> {
                            console.log("change data output")
                            return (err) ? console.log(err): console.log("The file pages was saved!")
                        });

                    })

                    fs.writeFile('./static/WPdataRooms-el.json', JSON.stringify(rooms), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });


                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

function REQUESTgallerie(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){

                    let info = JSON.parse(body)

                    let galleries = []

                    info.map(function(item){
                        console.log(item)
                        let urlJson = './static/wpgalleries/'+ item.slug +'-el.json'
                        let  gallerie;

                        if( item.acf.gallery.length >= 0 ){
                            gallerie = item.acf.gallery.map(function(gallerie){
                                return gallerie.url
                            })
                        }

                        let page = {
                            "post_id": item.id,
                            "post_name": item.title.rendered,
                            "post_slug": item.slug,
                            "gallery_image": item.acf.gallery_image.url,
                            "gallery": gallerie,
                            "SEO": {
                                "title": item.acf.title,
                                "description": item.acf.description,
                                "keywords": item.acf.keywords
                            }
                        }

                        homepage.galleriesData.push(page)

                        galleries.push(page)

                        fs.writeFile(urlJson, JSON.stringify(page), 'utf8',(err)=> {
                            console.log("change data output")
                            return (err) ? console.log(err): console.log("The file pages was saved!")
                        });

                    })

                    fs.writeFile('./static/WPdataGalleries-el.json', JSON.stringify(galleries), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

function REQUESTOptions(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)

            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){
                    let info = JSON.parse(body)

                    let menu = []
                    console.log(info)

                    console.log(info.acf.book_now)

                    let obj = {
                        bookNow: info.acf.book_now,
                        facebook: info.acf.facebook,
                        instagram: info.acf.instagram,
                        twitter: info.acf.twitter,
                        hotelName: info.acf.hotel_name_and_development,
                        logo: info.acf.logo.url,
                        mhte: info.acf.mhte,
                        telephone: info.acf.telephone
                    }


                    fs.writeFile('./static/options-el.json', JSON.stringify(obj), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}


function REQUESTServices(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){

                    let info = JSON.parse(body)

                    let Services = []

                    info.map(function(item){

                        let urlJson = './static/wpservices/'+ item.slug +'-el.json'
                        let featImage = '';

                        if( item._embedded !== undefined && item._embedded['wp:featuredmedia'] !== undefined){
                            featImage = item._embedded['wp:featuredmedia']['0'].source_url
                        }

                        let page = {
                            "post_id": item.id,
                            "post_name": item.title.rendered,
                            "post_slug": item.slug,
                            "content" : item.content.rendered,
                            "excerpt" : item.excerpt.rendered,
                            "image": featImage,
                            "SEO": {
                                "title": item.acf.title,
                                "description": item.acf.description,
                                "keywords": item.acf.keywords
                            }
                        }

                        Services.push(page)

                        fs.writeFile(urlJson, JSON.stringify(page), 'utf8',(err)=> {
                            console.log("change data output")
                            return (err) ? console.log(err): console.log("The file pages was saved!")
                        });

                    })

                    fs.writeFile('./static/WPdataServices-el.json', JSON.stringify(Services), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

function REQUESTReviews(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){

                    let info = JSON.parse(body)

                    let Reviews = []

                    info.map(function(item){

                        let review = {
                            "post_id": item.id,
                            "post_name": item.title.rendered,
                            "post_slug": item.slug,
                            "content" : item.content.rendered,
                            "excerpt" : item.excerpt.rendered,
                            "name": item.acf.name,
                            "lang": item.acf.lang,
                            "review_from" : item.acf.review_from,
                            "SEO": {
                                "title": item.acf.title,
                                "description": item.acf.description,
                                "keywords": item.acf.keywords
                            }
                        }

                        Reviews.push(review)

                    })

                    fs.writeFile('./static/WPdataReviews-el.json', JSON.stringify(Reviews), 'utf8',(err)=> {
                        console.log("change data output Reviews")
                        return (err) ? console.log(err): console.log("The file pages was saved!  Reviews");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

let taxonomys;
function REQUESTTaxonomy(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){

                    let info = JSON.parse(body)

                    let Taxonomies = []

                    info.map(function(item){

                        let featImage = '';
                        let description = '';

                        if( item.acf.image !== undefined){
                            featImage = item.acf.image.url
                        }


                        if( item.description !== undefined){

                            let req = /\[.*?\]/
                            let desc = item.description.split(req)[2]

                            if( item.description.split(req).length > 3){
                                description = desc
                            }else{
                                description = item.description
                            }

                        }

                        let page = {
                            "post_id": item.id,
                            "post_name": item.name,
                            "post_slug": item.slug,
                            "description": description,
                            "image": featImage,
                            "count": item.count,
                            "show": false,
                        }

                        Taxonomies.push(page)

                        taxonomys = Taxonomies
                    })

                    fs.writeFile('./static/WPdataTaxs-el.json', JSON.stringify(Taxonomies), 'utf8',(err)=> {
                        console.log("change data output Taxs")
                        return (err) ? console.log(err): console.log("The file pages was saved!  Maps");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}


function REQUESTMaps(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){

                    let info = JSON.parse(body)

                    let Maps = []

                    info.map(function(item){

                        let featImage = '';
                        // let gallerieArr = []
                        let taxs = []

                        if( item._embedded !== undefined && item._embedded['wp:featuredmedia'] !== undefined){
                            featImage = item._embedded['wp:featuredmedia']['0'].source_url
                        }


                        let TAXONOMY_MAP = ''

                        if( item.sightseeings.length > 0){
                            item.sightseeings.map(function(tax,i){
                                taxs.push(tax)
                            })
                            taxonomys.map(function(tax){
                                if( tax.post_id === item.sightseeings[0] ){
                                    TAXONOMY_MAP = tax.post_slug
                                }
                            })
                        }

                        // if( item.acf.gallerie.length > 0){
                        //     item.acf.gallerie.map(function(img){
                        //         gallerieArr.push(img.url)
                        //     })
                        // }

                        let page = {
                            "post_id": item.id,
                            "post_name": item.title.rendered,
                            "post_slug": item.slug,
                            "content" : item.content.rendered,
                            "lat": item.acf.lat,
                            "lng": item.acf.lng,
                            "image": featImage,
                            "terms": taxs,
                            "taxonomies": TAXONOMY_MAP,
                            "show": false,
                            "showInfoBox": false,
                            "gogo": 0,
                            "SEO": {
                                "title": item.acf.title,
                                "description": item.acf.description,
                                "keywords": item.acf.keywords
                            }
                            // "gallerie": gallerieArr
                        }

                        Maps.push(page)

                    })

                    fs.writeFile('./static/WPdataMaps-el.json', JSON.stringify(Maps), 'utf8',(err)=> {
                        console.log("change data output Maps")
                        return (err) ? console.log(err): console.log("The file pages was saved!  Maps");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}


async function GET_WORDPRESS_DATA() {


    const WPTaxs = {
        url : WPtaxonomy,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET"
    }

    const Wtaxs= await REQUESTTaxonomy(WPTaxs)
    console.log('SHOULD WORK: Taxs');

    const WPMaps = {
        url : WPmaps,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET"
    }

    const Wmaps= await REQUESTMaps(WPMaps)
    console.log('SHOULD WORK: Maps');


    const WPReviews = {
        url : WPreviews,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET"
    }

    const Wreviews= await REQUESTReviews(WPReviews)
    console.log('SHOULD WORK: Reviews');


    const WPServices = {
        url : WPservices,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET"
    }

    const Wservices = await REQUESTServices(WPServices)
    console.log('SHOULD WORK: Services');

    const WPOptions = {
        url : WPoptions,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET",
        // body: JsonBody,
        //json: true
    }

    const WOptions = await REQUESTOptions(WPOptions);
    console.log('SHOULD WORK: CREATE OPTIONS');

    const WPMenus = {
        url : WPmenu,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET"
    }


    const WPMENU = await REQUESTMenu(WPMenus)
    console.log('SHOULD WORK: MENUS');

    const WPGalleries = {
        url : WPFieldsGalleries,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET"
    }

    const Wgalleries = await REQUESTgallerie(WPGalleries)
    console.log('SHOULD WORK: Galleries');

    const WPRooms = {
        url : WPurl,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET",
        // body: JsonBody,
        //json: true
    }

    const Wrooms = await REQUESTrooms(WPRooms);
    console.log('SHOULD WORK: CREATE ROOMS');

    const WPPAGES = {
        url : WPpages,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET",
        // body: JsonBody,
        //json: true
    }

    const Wpages = await REQUESTpages(WPPAGES)
    console.log('SHOULD WORK: CREATE PAGES');

    fs.writeFile('./static/pages/homepage-el.json', JSON.stringify(homepage), 'utf8',(err)=> {
        console.log("change data output")
        return (err) ? console.log(err): console.log("The file pages was saved!");
    })

}

GET_WORDPRESS_DATA()
