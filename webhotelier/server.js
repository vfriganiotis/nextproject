const express = require('express')
const next = require('next')
// const LRUCache = require('lru-cache')
const { join } = require('path')
const compression = require('compression')
const bodyParser = require('body-parser')
const Recaptcha = require('recaptcha-v2').Recaptcha;
const cors = require('cors')

require('dotenv').config()

const port = parseInt(process.env.PORT, 10) || 3000

const app = next({ dev: true })

const handle = app.getRequestHandler();

const cron = require('node-cron');

//Import Env Variables
// const getConfigEnv = require('./next.config.js')

//"start": "SET NODE_ENV=production && pm2 start server.js --watch --ignore-watch 'node_modules'"

const transporter = require('./mailer')

const MAIL_USER  = process.env.MAIL_USERNAME

//OLD SITE REDIRECTS
const redirects = [
    { from: '/contact.php', to: '/contact' },
    { from: '/gr/contact.php', to: '/el/contact' },
]

app.prepare()
  .then(() => {

      const server = express()

      //Compression
      server.use(compression())

      //Cors
      server.use( cors() );

      //Body Parser
      server.use( bodyParser.json() );       // to support JSON-encoded bodies
      server.use(bodyParser.urlencoded({     // to support URL-encoded bodies
          extended: true
      }));

      //Redirects
      redirects.forEach(({ from, to, type = 301, method = 'get' }) => {
          server[method](from, (req, res) => {
              res.redirect(type, to)
          })
      })

      server.get('/gr', function(req, res) {

          res.redirect('/el');
      });

      server.get('/gr/:page/', function(req, res) {

          let newUrl = '/el/' +  req.params.page
          res.redirect(newUrl);
      });

      server.get('/gr/:page/:subpage', function(req, res) {

          let newUrl = '/el/' +  req.params.page + '/' + req.params.subpage
          res.redirect(newUrl);
      });

      server.get('/gr/:page/:subpage/:postpage', function(req, res) {

          let newUrl = '/el/' +  req.params.page + '/' + req.params.subpage + '/' + req.params.postpage
          res.redirect(newUrl);
      });

      server.post('/sendMail', (req, res, next) => {

          let name = req.body.name
          let email = req.body.email
          let message = req.body.message

          let recapth = req.body.recapthca

          console.log("eeeeeeeeeeeee")
          console.log(message)
          let content = `name: ${name} \n email: ${email} \n message: ${message} `

          let mail = {
              from: name,
              to: MAIL_USER,  //Change to email address that you want to receive messages on
              subject: 'New Message from Contact Form',
              text: content
          }

          const recaptchaData = {
              remoteip: req.connection.remoteAddress,
              response: recapth,
              secret: process.env.RECAPTCHA_SECRET,
          };

          const recaptcha = new Recaptcha(process.env.RECAPTCHA, process.env.RECAPTCHA_SECRET, recaptchaData);

          recaptcha.verify((success) => {

              if (success) {

                  transporter.sendMail(mail, (err, data) => {

                      if (err) {
                          res.json({
                              msg: 'fail'
                          })
                      } else {
                          res.json({
                              msg: 'success'
                          })
                      }
                  })

              }else{

                  res.json({
                      msg:  'fail'
                  })

              }
          })

      })


      server.use("/el", express.static(__dirname + "/el"));

      // server.use("/out", express.static(__dirname + "/out"));

      server.use("/location", express.static(__dirname + "/location"));

      server.use("/static", express.static(__dirname + "/static", {
          maxAge: "365d"
      }));


      // server.use("/out/static", express.static(__dirname + "/out/static", {
      //     maxAge: "365d"
      // }));

        // server.get('/gallery', (req, res) => {
        //     return app.render(req, res, '/gallery', req.query)
        // })

        // server.get('/galleries', (req, res) => {
        //   return app.render(req, res, '/photo-gallery', { id: req.params.slug , tt: req.query } )
        // })

        // server.get('/galleries/:slug', (req, res) => {
        //     return app.render(req, res, '/photos', { id: req.params.slug , tt: req.query })
        // })

        server.get('/service-worker.js', (req, res) => {

            const filePath = join(__dirname, '.next', '/service-worker.js')
            app.serveStatic(req, res, filePath)

        });

        // server.get('/wp-reset', (req, res) => {
        //
        //      // ssrCache.reset()
        //
        // });


        server.get('/wp-api', (req, res) => {
              //  console.log(ssrCache)
              // ssrCache.reset()
              console.log("meta to reset")
              // console.log(ssrCache)
              const shell = require('./child_helper');

              const commandList = [
                  "npm run refresh"
              ]

              shell.series(commandList , (err)=>{
                  if(err){
                      console.log(err)
                  }
                  console.log('cron run')
              });

        });


      // server.get('/el/room/:slug', (req, res) => {
        //
        //     console.log(req.params[0])
        //     console.log(1)
        //     console.log(req.params.slug)
        //     let ROOMS = require('./static/wprooms/' + req.params.slug + '.json');
        //     let GALLERIES = require('./static/wpgalleries/' + req.params.slug + '.json');
        //     let HOMEPAGE = require('./static/pages/homepage.json');
        //
        //    //return app.render(req, res, '/template-room', { id: req.params.slug , page: ROOMS , gallery: GALLERIES })
        //     return app.render(req, res, '/index', { id : "d" , homepage: HOMEPAGE })
        // })

        // server.get('/room(?:/:slug?)?', (req, res) => {
        //
        //       console.log(req.params[0])
        //       console.log(req.params.slug)
        //       let ROOMS = require('./static/wprooms/' + req.params.slug + '.json');
        //       let GALLERIES = require('./static/wpgalleries/' + req.params.slug + '.json');
        //
        //       return app.render(req, res, '/template-room', { id: req.params.slug , page: ROOMS , gallery: GALLERIES })
        // })

       // server.get('/location/:sight/:slug', function(req, res){
       //
       //  return app.render(req, res, '/template-map', { lang: req.params.lang , sight: req.params.sight , slug:  req.params.slug })
       //
       // })


      // server.get('/location/:slug', function(req, res){
      //
      //
      //     return app.render(req, res, '/taxonomies', { lang: req.params.lang  , slug:  req.params.slug })
      // })



      server.get('/privacy', function(req, res) {

          return app.render(req, res, '/privacy', { lang: req.params.lang })
      });

      server.get('((/:lang?)/location|/location)',  function(req, res) {

          return app.render(req, res, '/location', { lang: req.params.lang })
      });

      server.get('((/:lang?)/location(?:/:slug?)|/location(?:/:slug?))', function(req, res){

          return app.render(req, res, '/taxonomies', { lang: req.params.lang  , slug:  req.params.slug })
      });

      server.get('((/:lang?)/location(?:/:sight?)(?:/:slug?)|/location(?:/:sight?)(?:/:slug?))', function(req, res){

          return app.render(req, res, '/template-map', { lang: req.params.lang , sight: req.params.sight , slug:  req.params.slug })
      });

      server.get('((/:lang?)/room(?:/:slug?)|/room(?:/:slug?))', function(req, res){

         return app.render(req, res, '/room', { lang: req.params.lang  , roomslug:  req.params.slug })
      });

     server.get('((/:lang?)/photos(?:/:slug?)|/photos(?:/:slug?))', (req, res) => {

        return app.render(req, res, '/photos', { lang: req.params.lang  , slug:  req.params.slug })
     });

     server.get('((/:lang?)/accommodation|/accommodation)', function(req, res){

        return app.render(req, res, '/accommodation', { lang: req.params.lang })
     });

     server.get('((/:lang?)/services|/services)', function(req, res) {

        return app.render(req, res, '/services', { lang: req.params.lang })
     });

     server.get('((/:lang?)/contact|/contact)', (req, res) => {

        return app.render(req, res, '/contact', { lang: req.params.lang })
     });

     server.get('((/:lang?)/homepage|/homepage)',(req, res) => {

         return app.render(req, res, '/homepage', { lang: req.params.lang })
     });

     server.get('((/:lang?)/reviews|/reviews)', (req, res) => {

         return app.render(req, res, '/reviews', { lang: req.params.lang })
     });

     server.get('((/:lang?)/galleries|/galleries)', (req, res) => {

         return app.render(req, res, '/galleries', { lang: req.params.lang })
     });

     server.get('/el', function(req, res) {

         return app.render(req, res, '/index', { lang : 'el' })
     });


      server.get('/', function(req, res) {

         return app.render(req, res, '/index', { lang : '' })
     });

     server.get('*', (req, res) => handle(req, res))

     server.listen(port, (err) => {
        console.log( process.env.NODE_ENV )
        console.log(`> Ready on 1 http://localhost:${port}`)
     })

  })



//PRODUCTION PACKAGE
// "build": "next build",
// "reload": "pm2 reload all",
// "start": "pm2 start server.js --name kallisto"


/*
 * CRON JOBS
 */
//
// cron.schedule('59 1 * * *' ,()=>{
//     const shell = require('./child_helper');
//
//     const commandList = [
//         "node  cronjob.js",
//     ]
//
//     shell.series(commandList , (err)=>{
//         if(err){
//             console.log(err)
//         }
//         console.log('cron run')
//     });
// });
//

/*
 * LRU CACHE
 */
//
// const ssrCache = new LRUCache({
//     max: 100,
//     maxAge: 1000 * 60 * 60 // 1hour
// })
//
// function getCacheKey (req) {
//     return `${req.url}`
// }
//
// async function renderAndCache (req, res, pagePath, queryParams) {
//     const key = getCacheKey(req)
//
//     // If we have a page in the cache, let's serve it
//
//     if (ssrCache.has(key)) {
//         console.log("cache HIT")
//         res.setHeader('x-cache', 'HIT')
//         res.send(ssrCache.get(key))
//         return
//     }
//
//     console.log("nOT Cache")
//
//     try {
//         // If not let's render the page into HTML
//         const html = await app.renderToHTML(req, res, pagePath, queryParams)
//
//         // Something is wrong with the request, let's skip the cache
//         if (res.statusCode !== 200) {
//             res.send(html)
//             return
//         }
//
//         // Let's cache this page
//         ssrCache.set(key, html)
//
//         res.setHeader('x-cache', 'MISS')
//         res.send(html)
//     } catch (err) {
//         app.renderError(err, req, res, pagePath, queryParams)
//     }
// }
//